from __future__ import division
import scipy as sp
import numpy as np
import carla_ordered as cor
import altered_cdfs as alt 
import cdfs_likenormal as lk
from plotfunc import pdf_cdf_plot
import hetero_cdfs as hc 
from plotfunc import pdf_cdf_plot
from std_errs import *
from scipy.stats import norm, laplace
from time import time

from scipy.stats import skew, kurtosis, norm, laplace
import matplotlib.pyplot as plt
from scipy.optimize import fmin_bfgs, minimize
import statsmodels.api as sm

import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *
from sgttree_pdfs import *


data = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\HOROWTZ5.txt')

y_obs = data[:,0]
x = data[:,1:]
# data = data[0:300]
np.savetxt('HOROWTZ5.csv', data, delimiter = ",")


k = 2
options = ({'maxiter': 50000, 'maxfev' : 50000})


#.....................probit.........................
#checking out the standard errors I have coded. 

guess_normal = [1, 1, 0, 0, 1]
est_normal = lk.estm_ordered(x, y_obs, norm.cdf, guess_normal, k)
quad1, quad2, quad4, hess_exp, errs1 = std_errs(y_obs, x, [est_normal.x[0]], est_normal.x[1:], [0], norm.pdf, norm.cdf, method = 'optgradient')
# bets, xb, PDFeval, CDFeval, rho, Yvec, errs2 = std_errs_prob(y_obs, x, est_normal.x)



# #........................Laplace..................................
# guess_laplace = [1.4, 2.6, .05, 0, 1]
# guess_laplace = est_normal.x
# est_laplace1 =  lk.estm_ordered(x, y_obs, laplace.cdf, guess_laplace, k, method = 'Nelder-Mead')
# est_laplace2 = cor.estm_ged(x, y_obs, alt.laplace_cdf_scale, guess_laplace, k, method = 'Nelder-Mead')

## .......................Snormal.......................................
# guess_snormal = np.concatenate((est_normal.x, [0]))
# est_snormal = cor.estm_ged(x, y_obs, alt.snormal_cdf_scale, guess_snormal, k, method = 'Nelder-Mead')

# #.........................Slaplace.....................................
# guess_slaplace = np.concatenate((est_normal.x,[0]))
# est_slaplace = cor.estm_ged(x, y_obs, alt.slaplace_cdf_scale, guess_slaplace, k, method = 'Nelder-Mead')



#..........................GED..................................
# guess_ged = [1.4, 2.6, .05, 0, 1, 2]
# est_ged = cor.estm_ged(x, y_obs, alt.ged_cdf_scale, guess_ged, k, method = 'Nelder-Mead')

# # #...........................SGED................................
# # guess_sged = [1.4, 2.6, .05, 0, 1, 0, 2]
# guess_sged = np.zeros(len(guess_laplace) + 2)
# guess_sged[0:len(guess_laplace)] = est_normal.x
# guess_sged[len(guess_laplace):] = [0, 2]
# est_sged = cor.estm_ged(x, y_obs, alt.sged_cdf_scale, guess_sged, k, method = 'Nelder-Mead')






#................hetprob........................................

# guess1_normalhet = [.6007, 1.22, .03, .005, .95, 0, 0, 0, 0]
# begin = time()
# est_normalh = hc.estm_ordered(x, y_obs, hc.normal_cdf_het, guess1_normalhet, k, method = 'Nelder-Mead', options = options)
# end = time() - begin

# # #...............hetged.....................................
# guess1_gedhet = [1.4, 2.6, .05, 0, 1, 2, .3, 0, 0, -.6]
# begin_ged = time()
# est_gedh = hc.estm_ordered(x, y_obs, hc.ged_cdf_het, guess1_gedhet, k, method = 'Nelder-Mead')
# end_ged = time() - begin
# #doesn't give me very similar results to the normal, but I guess that's ok. 

# #....................hetsnormal..........................
#this didn't work with the simulated data, so I doubt it will work here
# guess1_snormalhet = [1.4, 2.6, .05, 0, 1, 0, .3, 0, 0, -.6]
# begin_snormal = time()
# est_snormalh = hc.estm_ordered(x, y_obs, hc.snormal_cdf_het, guess1_snormalhet, k, method = 'Nelder-Mead')
# end_snormal = time() - begin_snormal


#.................hetlaplace.......................
# guess1_laplacehet = [1.4, 2.6, .05, 0, 1, .3, 0, 0, -6]
# begin_laplace = time()
# est_laplaceh = hc.estm_ordered(x, y_obs, hc.laplace_cdf_het, guess1_laplacehet, k, method = 'Nelder-Mead')
# end_laplace = time() - begin_laplace

# #.................hetslaplace.....................
# guess1_slaplacehet = [1.4, 2.6, .05, 0, 1, 0, .3, 0, 0, -6]
# begin_slaplace = time()
# est_slaplace = hc.estm_ordered(x, y_obs, hc.slaplace_cdf_het, guess1_slaplacehet, k, method = 'Nelder-Mead')
# end_slaplace = time() - begin_slaplace

#so the laplace distributions work! Yay. Now onto the SGED. 

#estimating things without heteroskedasticity to see if they give the same results as GQRmain