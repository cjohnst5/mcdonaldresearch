\documentclass[letterpaper,12pt]{article}

\usepackage{threeparttable}
% \usepackage[showframe=true]{geometry}
\usepackage{geometry}
\geometry{letterpaper,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in}
\usepackage[format=hang,font=normalsize,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{array}
\usepackage{delarray}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{csvsimple}
% \usepackage{placeins} 
\usepackage{caption}
\usepackage{bibentry}
\numberwithin{figure}{section}

%\usepackage{pdflscape} % Use this package if you want to see the landscape pages
                        % but are not going to print them (this is for editing)
                        % Use lscape package for printing.
\usepackage{lscape}
\usepackage{natbib}
\usepackage{setspace}
\usepackage{float,color}
\restylefloat{table}
\usepackage[pdftex]{graphicx}
% \usepackage{hyperref}
\usepackage[table,xcdraw]{xcolor}
\usepackage{authblk}
% \hypersetup{colorlinks,linkcolor=red,urlcolor=blue,citecolor=red}
\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}{Definition} % Number definitions on their own
\newtheorem{derivation}{Derivation} % Number derivations on their own
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}{Proposition} % Number propositions on their own
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\bibliographystyle{plainnat}
\newcommand\ve{\varepsilon}
\newcommand{\squeezeup}{\vspace{-2.5mm}}
\renewcommand\theenumi{\roman{enumi}}
\providecommand{\norm}[1]{\lVert#1\rVert}
\setlength{\fboxrule}{0pt}
\setlength{\fboxsep}{0mm}
\newcommand{\yii}[2]{$y^*_i$}




\begin{document}

\begin{titlepage}

\title{A Generalized Ordered Response Model}
\author{
Carla Johnston\footnote{Brigham Young University, Department of Economics, Provo, Utah, carlajohnston@gmail.com} 
    \and
    James McDonald\footnote{Brigham Young University, Department of Economics, Provo, Utah, james\_mcdonald@byu.edu}
      }
\date{October 2014} 
\maketitle

      
      
% \maketitle
\begin{abstract} 
\small{The ordered probit and logit models, based on the normal and logistic distributions, respectively, can yield
biased and inconsistent estimators when the distributions are misspecified. A generalized ordered response model is
introduced which can reduce the impact of distributional misspecification. An empirical exploration of various 
determinants of life satisfaction demonstrates the benefits of allowing for diverse distributional characteristics.}
% TODO As we finalize the application, key findings should probably be mentioned. 


\vspace{0.3in}

\textit{keywords:} semiparametric, SGED, categorical data

\vspace{0.3in}

\textit{JEL classifications:} E21,E23
\end{abstract}
\thispagestyle{empty}
\end{titlepage}
 
\begin{spacing}{1.5}
\section{Introduction}\label{SecIntro}
This paper is concerned with the possible improvement of estimation methods handling categorical data in the dependent variable. Models with ordered dependent variables can be applied to several microeconomic topics, such as assessment of car accidents, self-evaluation surveys, educational achievement and life satisfaction. Potenetial estimation problems arise when a latent, continuous variable is observed as a countable, ordered response. Various approaches have been taken to estimate these types of models, the two most common being the ordered probit and ordered logit models. These models use maximum likelihood estimation and assume the errors to be distributed normally (in the case of the ordered probit) or logistically (in the case of the ordered logit). When the true error distribution follows either a normal or logistic density function, the ordered probit and the ordered logit will yield the most efficient estimators. However, when the errors are not normally or logistically distributed estimators may not just be inefficient, but inconsistent as well.\\ \\
% 
Ordered response models, specifically the ordered probit and the ordered logit, have been used in a variety of applications. Oftentimes, results from studies using the ordered probit or ordered logit have policy implications. For example, \citep{PudneyShields:2000} indicate that there exist non-negligible differences in lifetime earnings between different ethnic groups . Ordered response models are also used to determine marginal effects on the severity of truck and car crash injuries and how to mitigate those consequences \citep{LempKockelmanUnnikrishnan:2011}. The relationship between life satisfaction, income and demographic variables is studied in many papers involving ordered response models. An application of life satisfaction that is of particular interest to these authors appears in \citet{HodgeShankar:2014}.\\  \\
%
Many researchers have turned their attention to the methodology of ordered response models, in hopes to improve marginal effect estimates.  An extensive review of specification tests used with ordered response models is included in \citet{BellemareMelenbergVanSoest:2002}. \citet{Stewart:2004} has studied the use of a nested ordered probit in a hermite polynomial, therefore relaxing the normal distributional assumption for the errors. Alternatives to OLS models which incorporate flexible error distributions are addressed in \citep{ButlerMcDonaldNelsonWhite:1990}. 
This study will take a similar approach; instead of assuming the error distribution is normal or logistic, we will use more flexible error distributions, such as the skewed laplace and the skewed generalized error distribution (SGED), in an effort to obtain more efficient marginal effects. \\ \\
%
The rest of the paper procedes as follows: section 2 presents the model, section 3 discusses the empirical application and results and section 4 concludes. 


 

\section{The Model}\label{SecModel}
Consider the model
\begin{equation}\label{contvar}
    y^{*}_i = x_i\beta + \epsilon_i \mbox{\;\;\;\;\;\;\;\;\;\;} 1 \leq i \leq N
\end{equation}
%
where \yii is a continuous, latent variable, $x_i$ is a kx1 vector of independent variables, $\beta$ is a vector of unknown parameters and the $\epsilon_i$ are assumed to be independently and identically distributed with a pdf denoted $f()$ with distributional parameters, $\theta$. The number of observations is N. The above model could be consistently estimated using OLS if $y^{*}_i$ was observed. Often this is not the case. The true, continuous variable $y^*$ can be represented by  the variable $y$, which only spans a finite number of outcomes.  Due to the discrete-choice nature of the data, using OLS will result in heteroskedastic errors and questionable predicted probabilities of each outcome described in the vector y. 
To circumvent this problem, maximum likelihood estimation is often used to obtain the unknown parameters.  Consider the observed variable $y_i$:

\begin{equation}
y_i = \left\{ \begin{array}{ll}
        1 & \mbox{if  $ y^*_i<\alpha_1$}\\
        2 & \mbox{if $\alpha_1 \leq y^*_i < \alpha_2$}\\
        3 & \mbox{if $\alpha_2 \leq y^*_i < \alpha_3$}\\
        \vdots \\
        J & \mbox{if $\alpha_{j-1} \leq y^*_i$}
        \end{array} \right.
\end{equation}
%
where $J$ is the number of mutually exclusive categories of $y_i$. The probability of observing a particular outcome, for $1 \leq j \leq J$ is given by

\begin{equation}\label{prob}\begin{split}
Pr(y_i=j) &= Pr(\alpha_{j-1} \leq y^*_i < \alpha_j)\\
          &= Pr(\alpha_{j-1}-x_i\beta \leq \epsilon_i < \alpha_j - x_i\beta)\\
          &= F(\alpha_j - x_i\beta; \theta) - F(\alpha_{j-1}-x_i\beta; \theta)
\end{split}\end{equation}
%
where $F$ is the cumulative density function for $\epsilon_i$ and $\alpha_0=-\infty$  and $\alpha_J=\infty$. The presence of $F$ leads to a maximum likelihood estimation framework.  If we define 
\begin{equation}
    y_{ij} = \left\{ \begin{array}{ll}
            1 & \mbox{ if $y_i = j$} \\
            0 & \mbox{ else}
    \end{array} \right.
\end{equation}
%
then we can write the log-likelihood function as follows :
\begin{equation}\label{loglike}
log L = \sum^N_{i=1} \sum^J_{j=1} y_{ij}log [F(\alpha_j - x_i\beta;\theta) - F(\alpha_{j-1} - x_i\beta;\theta)]
\end{equation}
%
This log likelihood function appears to be very similar to that of the binary choice model, but allows for the possibility of more discrete choice options. In addition to $\beta$ and $\theta$, unknown parameters include $\alpha_1, \alpha_2, \hdots \alpha_{j-1}$. The above log likelihood function is maximized with respect to all these unknown parameters. \\ \\
%
The most common choices for the cumulative density function of the errors, $F$, are the cumulative normal and logistic distributions. Although a maximum likelihood model framework which assumes errors are either normal or logistically distributed provides a better alternative to OLS, misspecification of the error distribution can lead to inconsistent and biased estimators. \citet{Ruud:1983} proves that consistency can still be achieved under misspecification, but stringent conditions must be placed on the distribution of the independent variables. \\ \\
%
Allowing the errors to take on a more generalized distributional assumption has potential to reduce the problem of distributional misspecification and lead to more efficient estimators. Pictured below is the Skewed Generalized t (SGT) distribution tree, which shows the limiting and special cases of the SGT, including the normal and Laplace distributions. The distributions become increasingly more flexible as one ascends the tree.
\begin{figure}[H] 
\centerline{\includegraphics[width = 10 cm]{sgt.png}}
\caption{The SGT distribution tree (adapted from \citet{HansenMcDonaldNewey:2010})}
\end{figure}


The SGT was introduced by \citet{Theodossiou:2010}. Its cumulative distribution is given by
\begin{equation}\label{SGT cumulative}
SGT(y; m, \lambda, \sigma, p, q) = \frac{1-\lambda}{2} + \frac{(1 + \lambda sign(y-m))}{2}sign(y-m)B _z(1/p, q)
\end{equation}
where the incomplete beta function is represented by $B_z$ and $z$ is given as
\begin{equation}
z = \frac{|y-m|^p}{|y-m|^p + q\sigma ^p (1 + \lambda sign(y-m))^p}
\end{equation}
The parameter $m$ is a location parameter, $\lambda$ denotes skewness and $p$ and $q$ are shape parameters that determine peakedness and kurtosis. Letting the parameter $q$ in the SGT grow indefinitely large results in the SGED. The cumulative distribution for the SGED is listed below.
\begin{equation}\label{SGED}
SGED(y; m, \lambda, \alpha, p) = \frac{1-\lambda}{2} + \frac{1 + \lambda sign(y-m)}{2}
sign(y-m)\Gamma_z(1/p)
\end{equation}
%
where $\Gamma_z$ is the incomplete gamma function and
\begin{equation}\label{z}
z = \frac{|y-m|^p}{\alpha^p ( 1 + \lambda sign(y-m))^p}
\end{equation}
The distributions are symmetric if $\lambda$ = 0. For example, when $\lambda$ = 0 the SGT and the SGED yield the generalized t (GT) introduced by \citet*{McDonaldNewey:1988} and generalized error distribution (GED), respectively. Letting $p=2$ in the SGT yields the skewed t (ST) \citep{Hansen:1994}
The distributions featured in this paper are the SGED and the skewed Laplace distribution (SLaplace). To obtain the SLaplace, set $p=1$ in the SGED.


\section{Empirical Application}\label{Results}
Using data from the World Values Survey, waves 1-5 (the waves begin in 1981 and end in 2008) we examine the impact of income and religious activity on life satisfaction. Below table  \ref{table:stats} has summary statistics of the dependent and independent variables used in this paper's analysis. 

\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|}
\hline
Variable     & Mean    & Std. Dev. & Min & Max \\ \hline
satisfaction & 5.3156  & 2.608117  & 0   & 9   \\ \hline
religious    & 0.8419  & 0.364853  & 0   & 1   \\ \hline
male         & 0.4941  & 0.49999   & 0   & 1   \\ \hline
income       & 4.5223  & 2.427157  & 1   & 10  \\ \hline
age          & 39.7417 & 15.33493  & 15  & 93  \\ \hline
married      & 0.6582  & 0.474337  & 0   & 1   \\ \hline
unemployed   & 0.125   & 0.330736  & 0   & 1   \\ \hline
high school  & 0.5755  & 0.494292  & 0   & 1   \\ \hline
Observations & 10000   &           &     &     \\ \hline
\end{tabular}
\caption{Satisfaction is the dependent variable. Data is from the World Values Survey, waves 1-5.}
\label{table:stats}
\end{table} 
% 
This application is similar to the application used in Hodge and Shanker (2014). Out of a sample of 104,400 a random sample of 10000 observations was used, due the processing capacity of the in beta program written by the authors. The dependent variable was life satisfaction, as reported by the participants. A rating of 1 indicates a dissatisfied attitude towards life in general and a rating of 10 indicates a very satisfied outlook on life. The variable religious is a binary variable indicating whether the individual considers themselves a practicer of an established religion.  The variable high school denotes whether the respondent has completed secondary school. Male, married and unemployed are also binary variables. Age and age2 represent age in years and age squared. Income is a categorical variable partitioning income into ten levels. 
Below in tables \ref{table:results} and \ref{table:lrtest} are results from an empirical application of the generalized ordered response model. Table \ref{table:results} reports the estimated coefficients for the individual variables, the corresponding standard errors, the estimated distributional parameters and the log-likelihood values. The estimation was performed using Python and the standard errors were estimated using the outer product of the gradients. Table \ref{table:lrtest} reports the results of testing different hypotheses about the distribution. 



\begin{table}[H]
\begin{tabular}{|l|l|l|l|l|}
\hline
                   & Normal            & Laplace           & Slaplace          & SGED              \\ \hline
                   &                   &                   &                   &                   \\ \hline
religious          & 0.112676***       & 0.115157***       & 0.102173***       & 0.104261***       \\ \hline
\textit{Std. Errs} & \textit{0.028896} & \textit{0.020729} & \textit{0.019296} & \textit{0.019956} \\ \hline
male               & -0.03097          & -0.00944          & -0.01169          & -0.01226          \\ \hline
                   & \textit{0.020892} & \textit{0.015107} & \textit{0.014143} & \textit{0.01461}  \\ \hline
income             & 0.075817***       & 0.061958***       & 0.066263***       & 0.067919***       \\ \hline
                   & \textit{0.004488} & \textit{0.003486} & \textit{0.003211} & \textit{0.003293} \\ \hline
age                & -0.02104***       & -0.01559***       & -0.01464***       & -0.01506***       \\ \hline
                   & \textit{0.003852} & \textit{0.00286}  & \textit{0.002673} & \textit{0.002756} \\ \hline
age2               & 0.000225***       & 0.000167***       & 0.000147***       & 0.000152***       \\ \hline
                   & \textit{4.16E-05} & \textit{3.11E-05} & \textit{2.91E-05} & \textit{3.00E-05} \\ \hline
married            & 0.067688***       & 0.036844***       & 0.039893**        & 0.041038**        \\ \hline
                   & \textit{0.023875} & \textit{0.017549} & \textit{0.016339} & \textit{0.01687}  \\ \hline
unemployed         & -0.00478          & -0.00752          & -0.02134          & -0.02106          \\ \hline
                   & \textit{0.031676} & \textit{0.022941} & \textit{0.021459} & \textit{0.022186} \\ \hline
high school        & 0.024429          & 0.054494***       & 0.040117***       & 0.041299***       \\ \hline
                   & \textit{0.021836} & \textit{0.016084} & \textit{0.015155} & \textit{0.015641} \\ \hline
Loglikelihood      & -22094.8          & -22053.3          & -22013.7          & -22013.5          \\ \hline
                   &                   &                   &                   &                   \\ \hline
Dist. Param        &                   &                   &                   &                   \\ \hline
lambda             & 0                 & 0                 & 0.281032          & 0.280507          \\ \hline
p                  & 2                 & 1                 & 1                 & 1.033853          \\ \hline
q                  & inf               & inf               & inf               & inf               \\ \hline
\end{tabular}
\caption{Results using increasingly more flexible error distributional assumptions. ***, **, * means significant at a .01, .05 and .1 level.}
\label{table:results}
\end{table}
% }
\squeezeup
% 
Religious, income, age and marital status are significant on a .01 level across all regressions. Interestingly, the level of education is significant for every distribution but the normal, which is the least flexible distribution. Sex is not significant for any distribution. As expected, the effect of unemployment is negative and income is positive, but suprisingly unemployment does not have a significant effect on life satisfaction in any of the specifications. The results in table 2 suggest that happiness decreases with age until about 50, then increases. In the results above, loglikelihood values increase with more flexible distributions, yet the SGED's loglikehood hardly differs from the SLaplace. Loglikelihood tests between the SGED and the three other less flexible distributions are given in table \ref{table:lrtest}. The chi-square test statistic for the normal vs SGED and the Laplace vs SGED are significant at the .001 level. The normal and the Laplace distributions are rejected on comparison to the SGED. Based on the likelihood ratio test, the hypothesis that the SLaplace and SGED are observationally equivalent is not rejected. From these results, it appears that the most improvement of parameter estimation comes from allowing for skewness. Because the SGED differs so little from the SLaplace, it appears that the additional flexibility of the SGED's shape parameter is not significantly beneficial.
% 
\begin{table}[H]
\begin{tabular}{|l|l|l|}
\hline
Normal vs SGED & Laplace vs SGED & SLaplace vs SGED \\ \hline
162.4474       & 79.48023        & 0.383994               \\ \hline
\end{tabular}
\caption{LR tests between various distributions}
\label{table:lrtest}
\end{table}

To further illustrate the differences between the SGED and the normal and Laplace distributions, the ratio of the marginal effects between distributions is given in table \ref{table:margins_ratios}. The marginal distributions were found by taking a derivative of the likelihood function (equation \ref{prob}) with respect to the $kth$ element of the $x_i$ vector. 
\begin{equation}\label{margins}
    \frac{\delta(F(\alpha_j - x_i\beta; \theta)-F(\alpha_{j-1}-x_i\beta; \theta))}{\delta x_{ik}} = \beta_k[f(\alpha_{j-1}-x_i\beta; \theta)-f(\alpha_j-x_i\beta; \theta)]      
\end{equation}
%
where $f(x_i;\alpha,\beta;\theta)$ is the pdf of the specificed distribution. Relative marginal effects are found for each category of life satisfcation ($j=0, 1, ...9$) and are reported in table \ref{table:margins_ratios}.
\end{spacing}

\begin{table}[H]
\begin{spacing}{1}
\small
% \begin{adjustwidth}{-2cm}{}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
$\frac{Normal}{SGED}$      & religious & male     & income   & age & age2 & married  & unemployed & high school \\ \hline
j=0           & 1.054307  & 2.464688 & 1.089012 & 1.362916 & 1.447419  & 1.609085 & 0.221532   & 0.57707     \\ \hline
j=1           & 0.798737  & 1.867234 & 0.82503  & 1.032538 & 1.096557  & 1.219034 & 0.167832   & 0.437185    \\ \hline
j=2           & 0.648789  & 1.516696 & 0.670146 & 0.838699 & 0.890699  & 0.990184 & 0.136325   & 0.355112    \\ \hline
j=3           & 0.502787  & 1.175382 & 0.519338 & 0.64996  & 0.690258  & 0.767355 & 0.105646   & 0.275198    \\ \hline
j=4           & 0.370665  & 0.866515 & 0.382866 & 0.479163 & 0.508872  & 0.565709 & 0.077885   & 0.202882    \\ \hline
\rowcolor[HTML]{EFEFEF} 
j=5           & -0.15078  & -0.35249 & -0.15575 & -0.19492 & -0.207    & -0.23012 & -0.03168   & -0.08253    \\ \hline
j=6           & 0.147768  & 0.345442 & 0.152632 & 0.191022 & 0.202865  & 0.225524 & 0.031049   & 0.08088     \\ \hline
j=7           & 0.519218  & 1.213794 & 0.53631  & 0.671201 & 0.712816  & 0.792432 & 0.109099   & 0.284192    \\ \hline
j=8           & 0.88883   & 2.077848 & 0.918089 & 1.149003 & 1.220243  & 1.356534 & 0.186762   & 0.486497    \\ \hline
j=9           & 1.503076  & 3.513792 & 1.552555 & 1.943047 & 2.063519  & 2.293998 & 0.315829   & 0.822702    \\ \hline
              &           &          &          &          &           &          &            &             \\ \hline
$\frac{Laplace}{SGED}$      &           &          &          &          &           &          &            &             \\ \hline
j=0           & 0.803912  & 0.560818 & 0.663964 & 0.753646 & 0.800546  & 0.653458 & 0.259775   & 0.960389    \\ \hline
j=1           & 0.816162  & 0.569364 & 0.674082 & 0.765131 & 0.812745  & 0.663416 & 0.263733   & 0.975024    \\ \hline
j=2           & 0.825393  & 0.575803 & 0.681706 & 0.773784 & 0.821937  & 0.670919 & 0.266716   & 0.986052    \\ \hline
j=3           & 0.834194  & 0.581943 & 0.688975 & 0.782035 & 0.830701  & 0.678073 & 0.26956    & 0.996566    \\ \hline
j=4           & 1.041695  & 0.726698 & 0.860353 & 0.976561 & 1.037333  & 0.84674  & 0.336612   & 1.244455    \\ \hline
\rowcolor[HTML]{EFEFEF} 
j=5           & -1.48613  & -1.03674 & -1.22742 & -1.39321 & -1.47991  & -1.208   & -0.48023   & -1.7754     \\ \hline
j=6           & 1.544031  & 1.077133 & 1.27524  & 1.447488 & 1.537565  & 1.255062 & 0.498936   & 1.844568    \\ \hline
j=7           & 1.368174  & 0.954454 & 1.129998 & 1.282628 & 1.362445  & 1.112118 & 0.44211    & 1.634483    \\ \hline
j=8           & 1.337199  & 0.932845 & 1.104415 & 1.253589 & 1.3316    & 1.08694  & 0.4321     & 1.597478    \\ \hline
j=9           & 1.309782  & 0.913718 & 1.08177  & 1.227886 & 1.304297  & 1.064654 & 0.423241   & 1.564724    \\ \hline
              &           &          &          &          &           &          &            &             \\ \hline
$\frac{SLaplace}{SGED}$     &           &          &          &          &           &          &            &             \\ \hline
j=0           & 0.974516  & 0.948649 & 0.970185 & 0.966617 & 0.965355  & 0.966674 & 1.007618   & 0.965968    \\ \hline
j=1           & 0.98941   & 0.963148 & 0.985013 & 0.981391 & 0.98011   & 0.981449 & 1.023019   & 0.980732    \\ \hline
j=2           & 1.0007    & 0.974139 & 0.996253 & 0.99259  & 0.991293  & 0.992648 & 1.034692   & 0.991923    \\ \hline
j=3           & 1.014584  & 0.987654 & 1.010075 & 1.006361 & 1.005047  & 1.00642  & 1.049048   & 1.005685    \\ \hline
j=4           & 1.054823  & 1.026825 & 1.050135 & 1.046274 & 1.044907  & 1.046335 & 1.090653   & 1.04557     \\ \hline
\rowcolor[HTML]{EFEFEF} 
j=5           & 1.076866  & 1.048282 & 1.072079 & 1.068138 & 1.066743  & 1.0682   & 1.113445   & 1.06742     \\ \hline
j=6           & 1.033737  & 1.006298 & 1.029142 & 1.025358 & 1.024019  & 1.025418 & 1.068851   & 1.024669    \\ \hline
j=7           & 1.011278  & 0.984436 & 1.006784 & 1.003082 & 1.001772  & 1.003141 & 1.045629   & 1.002408    \\ \hline
j=8           & 0.995052  & 0.968641 & 0.99063  & 0.986987 & 0.985699  & 0.987045 & 1.028852   & 0.986324    \\ \hline
j=9           & 0.974578  & 0.94871  & 0.970247 & 0.966679 & 0.965417  & 0.966736 & 1.007683   & 0.96603     \\ \hline
\end{tabular}
\caption{Ratios of the marginal effects between the Normal and SGED, the Laplace and SGED and the SLaplace and SGED}
\label{table:margins_ratios}
\end{spacing}
\end{table}



%
\begin{spacing}{1.5}
Notice that all ratios are positive except for the j=5 level for the Normal/SGED and the Laplace/SGED. This is because when skewness is added to the model for the Slaplace and SGED, the distribution of the errors is skewed positively. This skewness shifts the peak of the distribution to the right, causing the second term from equation \ref{margins} to become positive when it was once negative for $j=5$. This accounts for the negative rations of Normal/SGED and Laplace/SGED when $j=5$. When the ratios are near to one, there is close agreement between the different specifications, such as the SLaplace and the SGED.  However, there are large differences in the marginal effects between the ordered probit and the more flexible distributions which allow for skewness. 


\section{Conclusion}\label{SecConcl}

In this specific empirical application, rather than risking inefficiencies associated with over specification, one might want to use the SLaplace distribution. The method of allowing for alternative error distributions allows the analyst to select the model which best fits the data. Comparing the marginal effects shows that statistical improvement yields nearly the same marginal effects and beta coefficients for the SLaplace and SGED, but differ significantly from those obtained using the ordered probit. 


\end{spacing}


\bibliography{references}
\end{document}

