\ifx\undefined\BySame
\newcommand{\BySame}{\leavevmode\rule[.5ex]{3em}{.5pt}\ }
\fi
\ifx\undefined\textsc
\newcommand{\textsc}[1]{{\sc #1}}
\newcommand{\emph}[1]{{\em #1\/}}
\let\tmpsmall\small
\renewcommand{\small}{\tmpsmall\sc}
\fi
\begin{thebibliography}{}

\harvarditem[Bellemare, Melenberg, and van Soest]{Bellemare, Melenberg, and van
  Soest}{2002}{BellemareMelenbergVanSoest:2002}
\textsc{Bellemare, C., B.~Melenberg,  {\small and} A.~van Soest}  (2002):
  ``Semi-parametric Models for Satisfaction with Income,'' \emph{Portuguese
  Economic Journal}, 1(2), 181--203.

\harvarditem[Butler, McDonald, Nelson, and White]{Butler, McDonald, Nelson, and
  White}{1990}{ButlerMcDonaldNelsonWhite:1990}
\textsc{Butler, R., J.~McDonald, R.~Nelson,  {\small and} S.~White}  (1990):
  ``Robust and Partially Adaptive Estimation of Regression Models,''
  \emph{Review of Economics and Statistics}, 72(2), 321--327.

\harvarditem[Hansen, McDonald, and Newey]{Hansen, McDonald, and
  Newey}{2010}{HansenMcDonaldNewey:2010}
\textsc{Hansen, C., J.~B. McDonald,  {\small and} W.~K. Newey}  (2010):
  ``Instrumental Variables Estimation with Flexible Distributions,''
  \emph{Journal of Business and Economic Statistics}, 28(1), 13--25.

\harvarditem[Hodge and Shankar]{Hodge and Shankar}{2014}{HodgeShankar:2014}
\textsc{Hodge, A.,  {\small and} S.~Shankar}  (2014): ``Partial Effects in
  Ordered Response Models with Factor Variables,'' \emph{Econometric Reviews},
  33(8), 854--868.

\harvarditem[Lemp, Kockelman, and Unnikrishnan]{Lemp, Kockelman, and
  Unnikrishnan}{2011}{LempKockelmanUnnikrishnan:2011}
\textsc{Lemp, J.~D., K.~M. Kockelman,  {\small and} A.~Unnikrishnan}  (2011):
  ``Analysis of Large Truck Crash Severity Using Heteroskedastic Ordered Probit
  Models,'' \emph{Accident and Analysis Prevention}, 43(1), 370--380.

\harvarditem[Pudney and Shields]{Pudney and Shields}{2000}{PudneyShields:2000}
\textsc{Pudney, S.,  {\small and} M.~Shields}  (2000): ``Gender, Race, Pay and
  Promotion in the British Nursing Professions: Estimation of a Generalized
  Ordered Probit Model,'' \emph{Journal of Applied Econometrics}, 15(4),
  367--399.

\harvarditem[Ruud]{Ruud}{1983}{Ruud:1983}
\textsc{Ruud, P.~A.}  (1983): ``Sufficient Conditions for the Consistency of
  Maximum Likelihood Estimation Despite Misspecification of Distribution in
  Multinomial Discrete Choice Models,'' \emph{Econometrica}, 51(1), pp.
  225--228.

\harvarditem[Stewart]{Stewart}{2004}{Stewart:2004}
\textsc{Stewart, M.~B.}  (2004): ``Semi-nonparametric Estimation of Extended
  Ordered Probit Models,'' \emph{The Stata Journal}, 4(1), 27--39.

\harvarditem[Theodossiou]{Theodossiou}{1998}{Theodossiou:2010}
\textsc{Theodossiou, P.}  (1998): ``Financial Data and the Skewed Generalized T
  Distribution,'' \emph{Management Science}, 44(12-1), 1650--1651.

\end{thebibliography}
