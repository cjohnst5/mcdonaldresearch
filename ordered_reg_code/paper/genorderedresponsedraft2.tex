\documentclass[letterpaper,12pt]{article}

\usepackage{threeparttable}
\usepackage{geometry}
\geometry{letterpaper,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in}
\usepackage[format=hang,font=normalsize,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{array}
\usepackage{delarray}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{csvsimple}
\usepackage{placeins}
% \usepackage[demo]{graphicx}   
\usepackage{caption}
\usepackage{bibentry}
\numberwithin{figure}{section}

%\usepackage{pdflscape} % Use this package if you want to see the landscape pages
                        % but are not going to print them (this is for editing)
                        % Use lscape package for printing.
\usepackage{lscape}
\usepackage{natbib}
\usepackage{setspace}
\usepackage{float,color}
\restylefloat{table}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage[table,xcdraw]{xcolor}
\usepackage{placeins}
% \usepackage{authblk}
\hypersetup{colorlinks,linkcolor=red,urlcolor=blue,citecolor=red}
\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}{Definition} % Number definitions on their own
\newtheorem{derivation}{Derivation} % Number derivations on their own
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}{Proposition} % Number propositions on their own
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\bibliographystyle{econometrica}
\newcommand\ve{\varepsilon}
\newcommand{\squeezeup}{\vspace{-2.5mm}}
\renewcommand\theenumi{\roman{enumi}}
\providecommand{\norm}[1]{\lVert#1\rVert}
\setlength{\fboxrule}{0pt}
\setlength{\fboxsep}{0mm}
\newcommand{\yii}[2]{$y^*_i$}




\begin{document}

\begin{titlepage}
\title{A Generalized Ordered Response Model}
\author{
Carla Johnston\footnote{Brigham Young University, Department of Economics, Provo, Utah 84602, \href{mailto:carlajohnston@gmail.com}{carlajohnston@gmail.com}.}  \\[-2pt]
    \and
    James McDonald\footnote{Brigham Young University, Department of Economics, Provo, UTah 84602, \href{mailto:james\_mcdonald@byu.edu}{james\_mcdonald@byu.edu}.}\\[-2pt]
      }
\date{May 2014} 
      
      
\maketitle
\begin{abstract}
\small{ The ordered probit and logit models are the standard model used to analyze categorical data. The use of a normal or logistic distribution to estimate parameters removes nonsensical parameter results and mitigates heteroskedasticity. Yet, the use of a normal or logistic distribution poses a threat of misspecification. When using MLE to estimate marginal effects, misspecification of the distribution of the error terms can lead to biased parameter estimates. To avoid this danger, the generalized ordered response model allows the error terms to take on more flexible distributions and reduce the consequences of misspecification. In particular, the skewed generalized error distribution (SGED) allows for varying skewness and kurtosis in the error distribution. An empirical application examining the effect of income on life satisfaction exhibits the improvement in estimate results when increasing the flexibility of error distribution assumptions. Most notable, the log-likelihood values increased with each step in increasing distribution flexibility. 

\vspace{0.3in}

\textit{keywords:} semiparametric estimation, ordered response, heteroskedasticity

\vspace{0.3in}

\textit{JEL classifications:} E21,E23}
\end{abstract}
\thispagestyle{empty}
\end{titlepage}
 
\begin{spacing}{1.5}
\section{Introduction}\label{SecIntro}
This paper is concerned with the possible improvement of estimation methods handling categorical data in the dependent variable. Models with ordered dependent variables can be applied to several microeconomic topics, such as assessment of car accidents, self-evaluation surveys, educational achievement, and job and life satisfaction. Estimation problems arise when a latent, continuous variable is observed as a countable, ordered response. Several approaches have been taken to estimate these types of models, the two most common being the ordered probit and ordered logit models. These models use maximum likelihood estimation and assume the errors to be distributed normally (in the case of the ordered probit) or logistically (in the case of the ordered logit). When the true error distribution follows either a normal or logistic density function, the ordered probit and the ordered logit will yield the most efficient estimators. However, when the errors are not normally or logistically distributed estimators may not just be inefficient, but inconsistent as well.\\

Ordered response models, specifically the ordered probit and the ordered logit, have been used in a variety of applications. Oftentimes, results from studies using the ordered probit or ordered logit have policy implications. For example, in their paper, ''Race, Pay and Promotion in the British Nursing Profession,'' Stephen Pudney and Michael Shields indicate that there exist non-negligible differences in lifetime earnings between different ethnic groups \citep{PudneyShields:2000}. Ordered response models are also used to determine marginal effects on the severity of truck and car crash injuries and how to mitigate those consequences \citep{LempKockelmanUnnikrishnan:2011}. The relationship between life satisfaction, income and demographic variables is studied in many papers involving ordered response models. An application of life satisfaction that is of particular interest to these authors appears in \citet{HodgeShankar:2014}.  

Many researchers have turned their attention to the methodology of ordered response models, in hopes to improve marginal effect estimates.  Charles Bellemare, Bertrand Melenberg and Arthur van Soest have written an extensive review of specification tests used with ordered response models \citep{BellemareMelenbergVanSoest:2002}. \citet{Stewart:2004} has studied the use of a nested ordered probit in a hermite polynomial, therefore relaxing the normal distributional assumption for the errors. In the application of OLS and GARCH models, other studies have simply assumed a more flexible error distribution, rather than using a hermite polynomial \citep{HansenMcDonaldTheodossiou:2007}
 This study will take a similar approach; instead of assuming the error distribution is normal or logistic, we will use more flexible error distributions, such as the skewed normal and the skewed generalized error distribution, in an effort to obtain more efficient marginal effects. \\

The rest of the paper procedes as follows: section 2 presents the model, section 3 discusses the empirical application and results, and section 4 concludes. 

%URL =      {http://www.economics-ejournal.org/economics/journalarticles/2007-7},
 

\section{The Model}\label{SecModel}
Consider the model
\begin{equation}\label{contvar}
    y^{*}_i = x_i\beta + \epsilon_i \mbox{\;\;\;\;\;\;\;\;\;\;} 1 \leq i \leq N
\end{equation}

where \yii is a continuous, latent variable, $x_i$ is a kx1 vector of independent variables, $\beta$ is a vector of unknown parameters and the $\epsilon_i$ are assumed to be independently distributed with a pdf denoted $f()$ with distributional parameters, $\theta$. The number of observations is N. The above model could be consistently estimated using OLS if $y^{*}_i$ was observed. Often this is not the case. The true, continuous variable $y^*$ can be represented by  the variable $y$, which only spans a finite number of outcomes.  Due to the discrete-choice nature of the data, using OLS will result in heteroscedastic errors and nonsensical predicted probabilities of each outcome described in the vector y. 
To circumvent this problem, maximum likelihood estimation is often used to obtain the unknown parameters.  Consider the observed variable $y_i$:

\begin{equation}
y_i = \left\{ \begin{array}{ll}
        1 & \mbox{if  $ y^*_i<\alpha_1$}\\
        2 & \mbox{if $\alpha_1 \leq y^*_i < \alpha_2$}\\
        3 & \mbox{if $\alpha_2 \leq y^*_i < \alpha_3$}\\
        \vdots \\
        J & \mbox{if $\alpha_{j-1} \leq y^*_i$}
        \end{array} \right.
\end{equation}

where $J$ is the number of mutually exclusive categories of $y_i$. The probability of observing a particular outcome, for $1 \leq j \leq J$ is given by

\begin{equation}\begin{split}
Pr(y_i=j) &= Pr(\alpha_{j-1} \leq y^*_i \leq \alpha_j)\\
          &= Pr(\alpha_{j-1}-x_i\beta \leq \epsilon_i < \alpha_i - x_i\beta)\\
          &= F(\alpha_j - x_i\beta) - F(\alpha_{j-1}-x_i\beta)
\end{split}\end{equation}

where $F$ is the cumulative density function for $\epsilon_i$ and $\alpha_0=-\infty$  and $\alpha_J=\infty$. The presence of $F$ leads to a maximum likelihood estimation framework.  If we define 
\begin{equation}
    y_{ij} = \left\{ \begin{array}{ll}
            1 & \mbox{ if $y_i = j$} \\
            0 & \mbox{ else}
    \end{array} \right.
\end{equation}

then we can write the log-likelihood function as follows :
\begin{equation}
log L = \sum^N_{i=1} \sum^J_{j=1} y_{ij}log [F(\alpha_j - x_i\beta) - F(\alpha_{j-1} - x_i\beta)]
\end{equation}

This log likelihood function appears to be very similar to that of the binary choice model, but allows for the possibility of more discrete choice options. In addition to $\beta$, unknown parameters include $\alpha_1, \alpha_2, \hdots \alpha_j$. The above log likelihood function is maximized with respect to all these unknown parameters. \\

The most common choices for the cumulative density function of the errors, $F$, are the cumulative normal and logistic distributions. Although a maximum likelihood model framework which assumes errors are either normal or logistically distributed provides a better alternative to OLS, misspecification of the error distribution can lead to inconsistent and biased estimators. \citet{Ruud:1983} proves that consistency can still be achieved under misspecification, but stringent conditions must be placed on the distribution of the independent variables. \\

Allowing the errors to take on a more generalized distributional assumption has potential to reduce the problem of distributional misspecification and lead to more efficient estimators. Pictured below is the Skewed Generalized t (SGT) distribution tree, which pictures the limiting and special cases of the SGT, including the normal and laplace distributions. The distributions become increasingly more flexible as one ascends the tree.
\begin{figure}[H] 
\centerline{\includegraphics[width = 10 cm]{sgt.png}}
% \caption{The SGT distribution tree}
% \bibentry{KermanMcDonald}
% \squeezeup
\end{figure}
\citep{KermanMcDonald:2013}
% \footnotemark
% \footnotetext{}

The cumulative distribution for the SGT is
\begin{equation}\label{SGT cumulative}
SGT(y; m, \lambda, \sigma, p, q) = \frac{1-\lambda}{2} + \frac{(1 + \lambda sign(y-m))}{2}sign(y-m)B _z(1/p, q)
\end{equation}
where the incomplete beta function is represented by $B_z$ and $z$ is given as
\begin{equation}
z = \frac{|y-m|^p}{|y-m|^p + q\sigma ^p (1 + \lambda sign(y-m))^p}
\end{equation}
and $m$ is a location parameter, $\lambda$ denotes skewness and $p$ and $q$ are shape parameters that determine peakedness and kurtosis. \\ \\

The distributions featured in this piece are the the skewed generalized error distribution (SGED), skewed normal(Snormal), and the skewed laplace distribution (Slaplace). The cumulative distribution for the SGED is listed below. : 



\begin{equation}\label{SGED}
SGED(y; m, \lambda, \alpha, p) = \frac{1-\lambda}{2} + \frac{1 + \lambda sign(y-m)}{2}
sign(y-m)\Gamma_z(1/p)
\end{equation}

where $\Gamma_z$ is the incomplete gamma function and
\begin{equation}\label{z}
z = \frac{|y-m|^p}{\alpha^p ( 1 + \lambda sign(y-m))^p}
\end{equation}

To obtain the skewed normal, set $p=2$ and to obtain the skewed laplace, set $p=1$. 
% To motivate Compare the SGED cumulative distribution to the normal cumulative distribution: 

% \begin{equation}\label{normal}
% normal(y; \lambda, ) = put equation here
% \end{equation}




\section{Empirical Application}\label{Results}
Using data from the World Values Survey, waves 1-5 (the waves began in 1981 and ends in 2008) we examine the impact of income and religious activity on life satisfaction. Below \ref{table:stats} has summary statistics of the dependent and independent variables used in this paper's analysis.

\begin{table}[h]
\begin{tabular}{|l|l|l|l|l|}
\hline
\rowcolor[HTML]{C0C0C0} 
Variable      & Mean    & Std. Dev  & Min & Max \\ \hline
satisfaction  & 5.5847  & 2.52477   & 0   & 9   \\ \hline
religious     & 0.939   & 0.2393423 & 0   & 1   \\ \hline
male          & 0.5101  & 0.499923  & 0   & 1   \\ \hline
income        & 4.0425  & 2.311066  & 1   & 10  \\ \hline
yearsold      & 37.3889 & 14.40392  & 16  & 88  \\ \hline
married       & 0.637   & 0.4808889 & 0   & 1   \\ \hline
unemployed    & 0.1308  & 0.3371984 & 0   & 1   \\ \hline
high school   & 0.595   & 0.4909166 & 0   & 1   \\ \hline
              &         &           &     &     \\ \hline
Observations: & 10000   &           &     &     \\ \hline
\end{tabular}
\caption{Satisfaction is the dependent variable. Data is from the World Values Survey, waves 1-5.}
\label{table:stats}
\end{table}





This application is similar to the application used in Hodge and Shanker (2014). Out of a sample of 104,400 a random sample of 1044 observations was used, due the processing capacity of the in beta program written by the authors. The dependent variable was life satisfaction, as reported by the participants. A rating of 1 indicates dissatisfied attitude towards life in general and a rating of 10 indicates very satisfied outlook. Independent variables are whether the individual views themselves as "religious", sex, income, age, whether they are married, whether they are  unemployed and whether they have graduated primary education levels. 
Below in tables \ref{table:results} and \ref{table:lrtest} are results from an empirical application of the generalized ordered response model using increasingly flexible models from left to right and likelihood ratio tests between different distributional assumptions. 


 % Our new results..............................
 % \FloatBarrier
 % TODO Get this table to appear in the right place. 


\begin{table}[H]
\begin{tabular}{|l|l|l|l|l|}
\hline
\begin{tabular}[c]{@{}l@{}}Assumed\\   Distribution:\end{tabular} & Normal               & Laplace              & Skewed Laplace       & SGED                 \\ \hline
                                                                  &                      &                      &                      &                      \\ \hline
religion                                                          & 0.094793283          & 0.043842721          & 0.051242081          & 0.052706401          \\ \hline
\textit{Standard errors}                                          & \textit{0.041135676} & \textit{0.031033175} & \textit{0.027550679} & \textit{0.027904122} \\ \hline
male                                                              & 0.026911861          & 0.0039426            & 0.004012522          & 0.00414428           \\ \hline
                                                                  & \textit{0.020883622} & \textit{0.015053798} & \textit{0.013409071} & \textit{0.013601635} \\ \hline
income                                                            & 0.076516533          & 0.064907205          & 0.069717282          & 0.070480561          \\ \hline
                                                                  & \textit{0.004704178} & \textit{0.003629638} & \textit{0.003249102} & \textit{0.003285109} \\ \hline
yearsold                                                          & -0.024066164         & -0.018754731         & -0.019515566         & -0.019772909         \\ \hline
                                                                  & \textit{0.004209051} & \textit{0.003179019} & \textit{0.002864607} & \textit{0.002901343} \\ \hline
yearsold2                                                         & 0.000305321          & 0.000230737          & 0.00022356           & 0.000226678          \\ \hline
                                                                  & \textit{4.74E-05}    & \textit{3.61E-05}    & \textit{3.24E-05}    & \textit{3.28E-05}    \\ \hline
married                                                           & 0.155235181          & 0.088107399          & 0.104899348          & 0.107006243          \\ \hline
                                                                  & \textit{0.024528707} & \textit{0.018322812} & \textit{0.016466289} & \textit{0.016692136} \\ \hline
unemployed                                                        & -0.087595602         & -0.073209383         & -0.065942561         & -0.066452419         \\ \hline
                                                                  & \textit{0.031656322} & \textit{0.022349551} & \textit{0.020071003} & \textit{0.020370112} \\ \hline
high\_school                                                      & 0.010599759          & 0.018712283          & 0.063363343          & 0.064239337          \\ \hline
                                                                  & \textit{0.021473418} & \textit{0.015812061} & \textit{0.014291164} & \textit{0.014484203} \\ \hline
                                                                  &                      &                      &                      &                      \\ \hline
Loglikelihood                                                     & -21618.35186         & -21587.43332         & -21509.81203         & -21509.7712          \\ \hline
                                                                  &                      &                      &                      &                      \\ \hline
Distributional Parameters                                         &                      &                      &                      &                      \\ \hline
lambda                                                            & 0                    & 0                    & 0.365946215          & 0.366957231          \\ \hline
p                                                                 & 2                    & 1                    & 1                    & 1.015428309          \\ \hline
q                                                                 & inf                  & inf                  & inf                  & inf                  \\ \hline
\end{tabular}
\caption{Results using increasingly more flexible error distributio assumptions}
\label{table:results}
\end{table}
\squeezeup

In the results above, loglikelihood values increase with more flexible distributions, yet the SGED's loglikehood hardly differs from the skewed laplace. The standard errors improve from left to right except in the case of going from the skewed laplace to the SGED. This is interesting and calls for further examination. 
% Old results.............................
% \begin{figure}[!ht]
% \begin{center}
%   \begin{tabular}{ | l | c | }
%     \hline
%     Ordered Probit vs Skewed normal & 8.76 \\ \hline
%     Ordered Probit vs SGED & 29.127192 \\
%     \hline
%   \end{tabular}  
% \end{center}
% \caption{LR test comparing each flexible distribution to the ordered probit}
% \end{figure}
% \squeezeup
% ........................................
% New results.............................

\begin{table}[!ht]
\begin{tabular}{|l|l|l|l|}
\hline
Normal vs Laplace & Normal vs Skewed Laplace & Normal vs. SGED & Skewed Lapce vs SGED \\ \hline
61.837083         & 217.0796496              & 217.1613242     & 0.0816746            \\ \hline
\end{tabular}
\caption{LR tests between various distributions}
\label{table:lrtest}
\end{table}

As depicted in table \ref{table:lrtest}, the likelihood ratio test comparing normal and laplace distribution assumptions indicates improvement, but it is not significant on the .01 level. Allowing the laplace distribution to take on more flexibility in the skewness parameter does result in a statistically significant improvement over using a normal distribution. Using a SGED distribution over a normal also results in statistically significant improvement yet the more flexible SGED distribution hardly leads to an improvement over the skewed laplace. 

\section{Conclusion}\label{SecConcl}

In this specific empirical application, skewed laplace outperforms the normal in terms of loglikelihood. Because the SGED distribution provides minor improvement over the skewed laplace, it appears that most of the improvement of the loglikelihood value came from allowing skewness in the laplace distribution and not necessarily altering tail thickness. 


\end{spacing}


\bibliography{references}
\end{document}

