\documentclass[letterpaper,12pt]{article}

\usepackage{threeparttable}
\usepackage{geometry}
\geometry{letterpaper,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in}
\usepackage[format=hang,font=normalsize,labelfont=bf]{caption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{array}
\usepackage{delarray}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{csvsimple}
% \usepackage[demo]{graphicx}   
\usepackage{caption}
\numberwithin{figure}{section}

%\usepackage{pdflscape} % Use this package if you want to see the landscape pages
                        % but are not going to print them (this is for editing)
                        % Use lscape package for printing.
\usepackage{lscape}
\usepackage{natbib}
\usepackage{setspace}
\usepackage{float,color}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage{placeins}
\usepackage{authblk}
\hypersetup{colorlinks,linkcolor=red,urlcolor=blue,citecolor=red}
\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}{Definition} % Number definitions on their own
\newtheorem{derivation}{Derivation} % Number derivations on their own
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}{Proposition} % Number propositions on their own
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\bibliographystyle{econometrica}
\newcommand\ve{\varepsilon}
\newcommand{\squeezeup}{\vspace{-2.5mm}}
\renewcommand\theenumi{\roman{enumi}}
\providecommand{\norm}[1]{\lVert#1\rVert}
\setlength{\fboxrule}{0pt}
\setlength{\fboxsep}{0mm}
\newcommand{\yii}[2]{$y^*_i$}




\begin{document}

\begin{titlepage}
\title{A Generalized Ordered Response Model}
% \author{Carla Johnston\footnote{Brigham Young University, Department of Economics, Provo, Utah 84602, \href{mailto:carlajohnston@gmail.com}{carlajohnston@gmail.com}.}  \\[-2pt]
%       }
\author[1]{Carla Johnston \thanks{carlajohnston@byu.edu}}
\author[1]{James McDonald \thanks {james\_mcdonald@byu.edu}}
\affil[1]{Department of Economics, Brigham Young University}
% \author{James McDonald\footnote{Brigham Young University, Department of Economics, Provo, Utah 84602, \href{mailto:james}} } %TODO Figure out how to put second author here. 
\date{November 2013} 
      
      
\maketitle
\begin{abstract}
\small{ The ordered probit and logit models are the standard model used to analyze categorical data. The use of a normal or logistic distribution to estimate parameters removes nonsensical parameter results and mitigates heteroskedasticity. Yet, the normal or logistic distributional assumptions for the errors pose a threat of misspecification. When using MLE to estimate marginal effects, misspecification of the error terms can lead to biased parameter estimates. To avoid this danger of misspecification, the generalized ordered response model allows the error terms to take on more flexible distributions, such as the skewed normal, the SGED and the ST. In particular, the SGED and ST allow for varying skewness and kurtosis factors in the error distribution. An empirical application examining the effect of income on life satisfaction exhibits the improvement in estimate results when increasing the flexibility of error distribution assumptions. Standard errors on parameters decreased and the log-likelihood functions increased with each step in increasing flexibility. Estimation methods accounting for heteroskedasticity and more flexible error distributions are also applied to the data.

\vspace{0.3in}

\textit{keywords:} semiparametric estimation, ordered response, heteroskedasticity

\vspace{0.3in}

\textit{JEL classifications:} E21,E23}
\end{abstract}
\thispagestyle{empty}
\end{titlepage}
 
\begin{spacing}{1.5}
\section{Introduction}\label{SecIntro}
This paper is concerned with the improvement of estimation methods handling categorical data in the dependent variable. Models with ordered dependent variables can be applied to several microeconomic topics, such as assessment of car accidents, self-evaluation surveys, educational achievement, and job satisfaction. Estimation problems arise when a latent, continuous variable is observed as a countable, ordered response. Several approaches have been taken to estimate these types of models, the two most common being the ordered probit and ordered logit models. These models use maximum likelihood estimation and assume the errors to be distributed normally (in the case of the ordered probit) or logistically (in the case of the ordered logit). When the true error distribution follows either a normal or logistic density function, the ordered probit and the ordered logit will yield the most efficient estimators. Yet these estimators depart very quickly from their efficiency properties if the error distribution is misspecified. \\

Semi-parametric literature concerning ordered dependent models is fairly recent and include papers by DeLucca and Perotti (2010) and Stewart (2004,2005). 

List of other papers I would like to mention: 
McDonald, Hansen and Theodossiou 2007 (use of more flexible distributions)\\
Van Soest 2002 (This paper goes over a few of the semi-parametric and non-parametric ways to estimate ordered response models )\\
A Review of the Ordered Probit model Daykin Moffatt 2002 \\
Stephen Pudney and Michael Shields (Wage gap in the nursing profession in the UK)\\
Hodge and Shanker 2013 (Partial Effects in ordered response models with factor variables.)
The paper will go as follows: Section 1 is a brief literature review; Section 2 describes the derivation of an ordered probit, ordered logit, and Stewart’s semi parametric approach. Section 3 describes the Monte Carlo study and presents the results. These methods, along with Stewart’s semi-parametric approach are applied to an empirical dataset in Section 4. Section 5 concludes. 



\section{The Model}\label{SecModel}
Consider the model
\begin{equation}\label{contvar}
    y^{*}_i = x_i\beta + \epsilon_i
\end{equation}

where \yii is a continuous, latent variable, $x_i$ is a matrix of independent variables, $\beta$ is a vector of unknown parameters and $\epsilon_i$ is a random, independent error term from some distribution, call it $F$. The above model could be consistently estimated using OLS if  was observed. Often this is not the case. The true, continuous variable $y^*$ can be represented by  the variable $y$, which only spans a finite number of outcomes.  Due to the discrete-choice nature of the data, using OLS will result in heteroscedastic errors and nonsensical predicted probabilities of each outcome described in the vector y. 
To circumvent this problem, maximum likelihood estimation is often used to obtain the unknown parameters.  Consider the observed variable $y_i$:

\begin{equation}
y_i = \left\{ \begin{array}{ll}
        1 & \mbox{if  $ y^*_i<\alpha_1$}\\
        2 & \mbox{if $\alpha_1 \leq y^*_i < \alpha_2$}\\
        3 & \mbox{if $\alpha_2 \leq y^*_i < \alpha_3$}\\
        \vdots \\
        J & \mbox{if $\alpha_{j-1} \leq y^*_i$}
        \end{array} \right.
\end{equation}

where $J$ is the number of mutually exclusive categories of $y_i$. The probability of observing a particular outcome, for $1 \leq j \leq J$ is given by

\begin{equation}\begin{split}
Pr(y_i=j) &= Pr(\alpha_{j-1} \leq y^*_i \leq \alpha_j)\\
          &= Pr(\alpha_{j-1}-x_i\beta \leq \epsilon_i < \alpha_i + x_i\beta)\\
          &= F(\alpha_j - x_i\beta) - F(\alpha_{j-1}-x_i\beta)
\end{split}\end{equation}

where $F$ is the cumulative density function for $\epsilon_i$ and $\alpha_0=-\infty$  and $\alpha_j=\infty$ . The presence of $F$ leads to a maximum likelihood estimation framework.  If we define 
\begin{equation}
    y_{ij} = \left\{ \begin{array}{ll}
            1 & \mbox{ if $y_i = j$} \\
            0 & \mbox{ else}
    \end{array} \right.
\end{equation}

This log likelihood function appears to be very similar to that of the binary choice model, but allows for the possibility of more discrete choice options. In addition to $\beta$, unknown parameters include $\alpha_1, \alpha_2, \hdots \alpha_j$. The above log likelihood function is maximized with respect to all these unknown parameters. \\

The most common choices for the cumulative density function of the errors, $F$, are the cumulative normal and logistic distributions. Although a maximum likelihood model framework which assumes errors are either normal or logistically distributed provides a better alternative to OLS, misspecification of the error distribution can lead to inconsistent and biased estimators. Rudd (1983) proves that consistency can still be achieved under misspecification, but stringent conditions must be placed on the distribution of the independent variables. \\

Allowing the errors to take on a more generalized distributional assumption can prevent misspecification and lead to more consistent estimators. Pictured below is the Skewed Generalized T (SGT) distribution tree, where the distributions become increasingly more flexible as one ascends the tree. 
% \includegraphics[width = 10 cm]{} %TODO insert image

The distributions featured in this piece are the the skewed generalized error distribution, skewed normal, and the skewed laplace distribution. Their cumulative distributions are listed below: 



\begin{equation}\label{SGED}
SGED(y; m, \lambda, \alpha, p) = \frac{1-\lambda}{2} + \frac{1 + \lambda sign(y-m)}{2}
sign(y-m)\Gamma_z(1/p)
\end{equation}

where $\Gamma_z$ is the incomplete gamma function and
\begin{equation}\label{z}
z = \frac{|y-m|^p}{\alpha^p ( 1 + \lambda sign(y-m))^p}
\end{equation}

where $m$ is a location parameter, $\lambda$ indicates skewness, and $p, q$ are shape parameters that indicate tail thickness. \\

To obtain the skewed normal, set $p=2$ and to obtain the skewed laplace, set $p=1$. Compare the SGED cumulative distribution to the normal cumulative distribution: 

\begin{equation}\label{normal}
normal(y; \lambda, ) = put equation here
\end{equation}




\section{Empirical Application}\label{Results}
Using data from the World Values Survey, waves 1-5 (the waves began in 1981 and ends in 2008). This application is similar to the application used in Hodge and Shanker (2013). Out of a sample of 104,400, a random sample of 1044 observations was used, due the processing capacity of the inbeta program. The independent variable was life satisfaction, as reported by the participants. A rating of 1 indicates dissatisfied and a rating of 10 indicates very satisfied. Dependent variables are whether the individual views themselves as "religious", sex, income, age, whether they are married, whether they are  unemployed and whether they have graduated primary education levels. 
Below are results from an empirical application of the generalized ordered response model.
 \begin{figure}[!ht]
    \includegraphics[width = 15 cm]{results.png}
    \caption{Results using increasingly more flexible error distribution assumptions}
    \end{figure}
    \squeezeup
\begin{figure}[!ht]
    \includegraphics[width = 15 cm] {lrtest.png}
    \caption{LR test comparing each flexible distribution to the ordered probit}
    \end{figure}
    \squeezeup
Heteroskedasticity results are forthcoming. 
\section{Conclusion}\label{SecConcl}

Forthcoming. 
\end{spacing}
\end{document}

%Results: Snormal, SGED, Slaplace 
%Results: Hetero with all of them. 