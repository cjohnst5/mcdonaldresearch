from __future__ import division
import scipy as sp
import numpy as np
import carla_ordered as cor
import altered_cdfs as alt 
import cdfs_likenormal as lk
from plotfunc import pdf_cdf_plot

from scipy.stats import skew, kurtosis, norm, laplace
import matplotlib.pyplot as plt
from scipy.optimize import fmin_bfgs, minimize
import statsmodels.api as sm

import sys

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\mle')
from normal_MLE import normal_MLE
from laplace_MLE import laplace_MLE
from slaplace_MLE import slaplace_MLE
from ged_MLE import ged_MLE
from snormal_MLE import snormal_MLE
from t_MLE import t_MLE
from sged_MLE import sged_MLE
from gt_MLE import gt_MLE
from st_MLE import st_MLE
from sgt_MLE import sgt_MLE

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *
from sgttree_pdfs import *

happy_income = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\gss_12.csv', delimiter = ",")
happy = happy_income[0:500, 1]
income = happy_income[0:500, 0]

#my ordered reg MLE requires the categories to go from 0 to n-1, hence the subtraction. 
happy = happy - 1
# happy = np.asarray([1 if x==2 else x for x in happy])
data = np.vstack((happy, income)).T
np.savetxt('happy_income.csv', data, delimiter = ",")
k = 3

guess_oprobit = [-1.5,  .3, -.07]
est_oprobit = lk.estm_ordered(income, happy, norm.cdf, guess = guess_oprobit, k = k, method = 'Nelder-Mead')

# #plotting my errors. 


guess_ged = [-1.5, .3,  -.07, 2]
est_ged = cor.estm_ged(income, happy, alt.ged_cdf_scale, guess_ged, k, method = 'Nelder-Mead')

# fig = plt.figure()
# pdf_cdf_plot('GED', ged_pdf, ged_cdf, est_ged.x[-1], alt.phi)




# guess_snormal = [-1.5, .3, -.07, .5]
# est_snormal = cor.estm_ged(income, happy, alt.snormal_cdf_scale, guess_snormal, k, method = 'Nelder-Mead')

# fig = plt.figure()
# pdf_cdf_plot('Snormal', snormal_pdf, snormal_cdf, est_snormal.x[-1], alt.phi)



# guess_slaplace = [-1.5, .3, -.07, 0]
# est_slaplace = cor.estm_ged(income, happy, alt.slaplace_cdf_scale, guess_snormal, k, method = 'Nelder-Mead')

# fig = plt.figure()
# pdf_cdf_plot('Slaplace', slaplace_pdf, slaplace_cdf, est_slaplace.x[-1], alt.phi)



guess_sged = [-1.5, .3,  -.07, 0, 2]
est_sged = cor.estm_ged(income, happy, alt.sged_cdf_scale, guess_sged, k, method = 'Nelder-Mead')

# fig = plt.figure()
# pdf_cdf_plot('SGED', sged_pdf, sged_cdf, est_sged.x[-2:], alt.phi)
# plt.show()


