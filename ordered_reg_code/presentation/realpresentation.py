from __future__ import division
import numpy as np
from scipy.special import beta, gamma
from matplotlib import pyplot as plt
from scipy.stats import norm


import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\shinycode')
import sgtpdfs 
from opt_main import *
from ordered_main import *
from std_errs1 import *

from sigma_calc import *
from sgtcdfs import *
from sgtpdfs import *
from loglike import *

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code')
from simul_func import *

def st_pdf(y, paravec):
    m, lamb, phi, p, q = paravec
    pdf = p/ ( 2*phi * q**(1/p) * beta(1/p, q)*\
        (1 + (abs(y-m)**p / ((1 + lamb*np.sign(y-m))**p * q *phi**p))**(q+1/p)))
    return pdf

def sged_pdf(paravec, y):
    m, lamb, sig, p = paravec 

    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    # print phi, "Phi sged"
    # print gamma(1/p), "Gamma(1/p)" 

    pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * phi) ** p) \
          / (2 * phi * gamma(1/p))
    return pdf

x_space = np.linspace(-5,5, 500)
#plotting all the different pdfs and cdfs

#location and phi
paravec_mp1 = [0, 0, 1, 2]
plt.plot(x_space, sged_pdf(paravec_mp1, x_space), label = 'm = 0, phi = 0')

paravec_mp2 = [2, 0, .5, 2]
plt.plot(x_space, sged_pdf(paravec_mp2, x_space), label = 'm=2, phi=.5')
ax = plt.gca()
ax.spines['right'].set_color('none') #Making a spine invisible
ax.spines['top'].set_color('none') #making a spine invisible
ax.spines['bottom'].set_position(('data', 0)) #we want to center this spine as the x-axis
ax.spines['left'].set_position(('data', 0)) #we want to center this spine as the y-axis
plt.xticks([-4, -2, 0, 2, 4], ['-4', '-2', '0', '2', '4'])
plt.yticks([0.2, .4, .6, .8, 1],['0.2','0.4','0.6','0.8','1'])
plt.legend(loc = 'upper left')
# plt.title('Sged')
plt.savefig('mean_scale', dpi = 500)
plt.show()

#lambda
paravec_l2 = [0, -.9, 1, 2]
plt.plot(x_space, sged_pdf(paravec_mp1, x_space), label = 'lamb = 0')
plt.plot(x_space, sged_pdf(paravec_l2, x_space), label = 'lamb = -0.9')
ax = plt.gca()
ax.spines['right'].set_color('none') #Making a spine invisible
ax.spines['top'].set_color('none') #making a spine invisible
ax.spines['bottom'].set_position(('data', 0)) #we want to center this spine as the x-axis
ax.spines['left'].set_position(('data', 0)) #we want to center this spine as the y-axis
plt.xticks([-4, -2, 0, 2, 4], ['-4', '-2', '0', '2', '4'])
plt.yticks([0.2, .4, .6, .8, 1],['0.2','0.4','0.6','0.8','1'])
plt.legend(loc = 'upper left')
# plt.title('Sged')
plt.savefig('lambchange', dpi = 500)
plt.show()

#p
paravec_p = [0, 0, 1, 4]
plt.plot(x_space, sged_pdf(paravec_mp1, x_space), label = 'p = 2')
plt.plot(x_space, sged_pdf(paravec_p, x_space), label = 'p = 4')
ax = plt.gca()
ax.spines['right'].set_color('none') #Making a spine invisible
ax.spines['top'].set_color('none') #making a spine invisible
ax.spines['bottom'].set_position(('data', 0)) #we want to center this spine as the x-axis
ax.spines['left'].set_position(('data', 0)) #we want to center this spine as the y-axis
plt.xticks([-4, -2, 0, 2, 4], ['-4', '-2', '0', '2', '4'])
plt.yticks([0.2, .4, .6, .8, 1],['0.2','0.4','0.6','0.8','1'])
plt.legend(loc = 'upper left')
# plt.title('Sged')
plt.savefig('pchange', dpi = 500)
plt.show()


# #simulating laplace data
#-------------------------------------------------------------------------------
# k = 5
# Y_lap, X_lap, alphas_lap = simul_data(100, [-3, 2], k, distr = 'laplace')
# data_lap = np.concatenate((Y_lap.reshape(len(Y_lap),1), X_lap), axis = 1)
# np.savetxt('laplace.csv', data_lap, delimiter = ',')
# Y_norm, X_norm, alphas_norm = simul_data(500, [-3,2], k, distr = 'normal')
# data_norm = np.concatenate((Y_norm.reshape(len(Y_norm),1), X_norm), axis = 1)
# np.savetxt('normal.csv', data_norm, delimiter = ',')

# laplace_test = output_function(2, Y_lap, X_lap, None, method_opt = 'Nelder-Mead', options = ({'maxiter': 2000, 'maxfev' : 20000}), method_std = 'invneghess')
# normal_test = output_function(1, Y_lap, X_lap, None, method_opt = 'Nelder-Mead', options =({'maxiter': 2000, 'maxfev' : 20000}), method_std = 'invneghess')
# ged_test = output_function(4, Y_lap, X_lap, None, method_opt = 'Nelder-Mead', options = ({'maxiter': 2000, 'maxfev' : 20000}), method_std = 'invneghess')

##these results are rather discouraging and boring, so I'll leave them out for now

#making pdfs of normal and GED
#-------------------------------------------------------------------------------
x_space = np.linspace(-5,5, 200)
plt.plot(x_space, norm.pdf(x_space), label = 'normal errors')
plt.plot(x_space, sged_pdf([0, 0, 3, 1], x_space), label = 'GED errors')
plt.legend()
plt.savefig('normalged', dpi = 500)
plt.show()