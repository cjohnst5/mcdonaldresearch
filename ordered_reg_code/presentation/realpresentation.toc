\beamer@endinputifotherversion {3.30pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Happiness Example}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{How does the oprobit work?}{6}{0}{1}
\beamer@sectionintoc {2}{Generalized Ordered Response Model}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{SGT tree (588 anyone?)}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Optimization methods}{17}{0}{2}
\beamer@subsectionintoc {2}{3}{Test statistics}{18}{0}{2}
\beamer@subsectionintoc {2}{4}{Heteroskedasticity}{19}{0}{2}
\beamer@sectionintoc {3}{So what?}{20}{0}{3}
\beamer@sectionintoc {4}{Preliminary results}{24}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{28}{0}{5}
\beamer@subsectionintoc {5}{1}{Future Research}{29}{0}{5}
