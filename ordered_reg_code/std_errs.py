from __future__ import division
import numpy as np
import scipy as sp
import scipy.linalg as la
from scipy.stats import norm, laplace
from simul_func import simul_data
import scipy.sparse as spar


def std_errs(Y, X, alphas, betas, dist_param, pdf, cdf, deltas = None, sigma_form = None, method = 'optgradient'):
    """
    Calculates the standard errors of estimated slope parameters and cutoffs. 
    Currently only calculates by using the inverse gradient and doesn't
    incorporate heteroskedasticity

    Parameters:
    ----------------------------------------------------------------
    Y:
        categorical variable. numpy VECTOR of INTEGERS
        Assuming the categories go from 0 to 1
    X:
        Independent variables. No intercept (column of ones). Numpy array/vector
    alphas:
        Cutoff values. List or numpy vector
    betas:
        Slope parameters. List or numpy vector
    deltas:
        Parameters for the sigma_form of heteroskedasticity
    dist_param:
        Distributional parameters for whatever distribution we are using
    pdf:
        Pdf function for our specified distribution. Hopefully someday this will
        become a global variable
    cdf:
        CDF function for our specified distribution. Hopefully will become a global
        variable. 
    sigma_form:
        Not sure what this will be yet. Something to incorporate different types of heteroskedasticity

    Returns:
    ---------------------------------------------------------------
    Standard deviations for betas and alphas 
    """
    #let's get started!
    #......................................GRADIENT MATRIX......................................
    
    k = len(alphas) + 1 #number of categories our y variable has. 
    m = len(Y)
    try:
        n = X.shape[1]
    except:
        n = 1
    xb = np.dot(X, betas)
    alpha_mat = np.zeros((m, len(alphas)))    
    beta_grad = np.zeros((m,1))

    #creating the alpha knot column that we will use in our gradient calculation
    #and updating our first derivative beta_grad 
    a01 = Y == 0
    z0 = alphas[0] - xb
    
    
    alpha_mat[:,0][a01] = pdf(z0[a01])/cdf(z0[a01])
    beta_grad[:,0][a01] = pdf(z0[a01])/cdf(z0[a01]) 
    print alpha_mat[:,0].mean(), "Alpha mat mean, old code"
    print beta_grad[:,0].mean(), "Beta mat mean, old code"         
    print pdf(z0[a01])[0:10], "Pdf"       
    print cdf(z0[a01])[0:10], "Cdf"


    #creating the last alpha column that we will use in our gradient calculation
    #and updating our first derivative beta_grad 
    alast1 = Y  == len(alphas) #since our categories go from 0,...n-1, this will give us the correct category number
    
    z4 = alphas[-1] - xb
    alpha_mat[:,-1][alast1] = -pdf(z4[alast1])/(1-cdf(z4[alast1]))
    beta_grad[:,0][alast1] = -pdf(z4[alast1])/(1-cdf(z4[alast1])) 

    if len(alphas) > 1:
        a02 = Y == 1
        z1 = alphas[1] - xb    
        alpha_mat[:,0][a02] = -pdf(z0[a02])/(cdf(z1[a02]) - cdf(z0[a02]))
        
        z3 = alphas[-2] - xb
        alast2 = Y == len(alphas) - 1    
        alpha_mat[:,-1][alast2] = pdf(z4[alast2])/(cdf(z4[alast2]) - cdf(z3[alast2]))

 
    #filling in the middle of our alpha columns, if we have more than two cutoff values
    if len(alphas) > 2:
        for j in xrange(1, k-2):            
            zj0 = alphas[j-1] - xb
            zj = alphas[j] - xb
            zj1 = alphas[j+1] - xb
            a1 = Y == j
            alpha_mat[:, j][a1] = pdf(zj[a1])/(cdf(zj[a1]) - cdf(zj0[a1]))
            a2 = Y == j + 1
            alpha_mat[:,j][a2] = -pdf(zj[a2])/(cdf(zj1[a2]) - cdf(zj[a2]))

        #completing our beta vector
        beta_bool = np.nonzero((Y!=0) & (Y!=len(alphas)))
        zj = alphas[Y[beta_bool]] - xb[beta_bool]
        zj0 = alphas[Y[beta_bool]-1] - xb[beta_bool]
        beta_grad[:,0][beta_bool] = (pdf(zj) - pdf(zj0)) / (cdf(zj) - cdf(zj0))
    

    
    gradrep = np.concatenate((alpha_mat, beta_grad * -1*X), axis = 1)
    gradrep[np.isnan(gradrep)] = 0
    gradrep[np.isinf(gradrep)] = 0
    grad_exp = np.dot(gradrep.T, gradrep) #* (1/m) #expected value of the outer product of the gradient
    
    
    #...........................................HESSIAN MATRIX............................................
    #first we will get our double derivatives for the alphas. 
    alpha_dd_mat = np.zeros((m, len(alphas)))
    alpha_cross_mat = np.zeros((m,len(alphas) -1))
    ab_cross_mat =  np.zeros((m, len(alphas)))
    beta_hess = np.zeros((m,1))

    #first column of alpha_dd and special case 1 for beta_hess
    rho1 = getrhoGQR(xb, alphas[0], 1)
    z0 = alphas[0] - xb

    alpha_dd_mat[:,0][a01] = -rho1[a01] * pdf(z0[a01]) / cdf(z0[a01]) - \
                                    (pdf(z0[a01]) / cdf(z0[a01])) **2

    ab_cross_mat[:,0][a01] = rho1[a01] * pdf(z0[a01]) / cdf(z0[a01]) + \
                            (pdf(z0[a01]) / cdf(z0[a01])) **2
    beta_hess[:,0][a01] = -rho1[a01] * pdf(z0[a01]) / cdf(z0[a01]) - \
                                    (pdf(z0[a01]) / cdf(z0[a01])) **2

    #last alpha_dd
    rho2 = getrhoGQR(xb, alphas[-1], 1)
    z4 = alphas[-1] - xb
    # print rho2.mean(), z4.mean(), "Rho2 mean, z4 mean"
    alpha_dd_mat[:,-1][alast1] = rho2[alast1] * pdf(z4[alast1]) / (1-cdf(z4[alast1])) \
                                    - (pdf(z4[alast1]) / (1-cdf(z4[alast1]))) ** 2
    ab_cross_mat[:,-1][alast1] = -rho2[alast1] * pdf(z4[alast1]) / (1-cdf(z4[alast1])) \
                                + (pdf(z4[alast1]) / (1-cdf(z4[alast1]))) ** 2

    beta_hess[:,0][alast1] = rho2[alast1] * pdf(z4[alast1]) / (1-cdf(z4[alast1]))\
                                    - (pdf(z4[alast1]) / (1-cdf(z4[alast1]))) ** 2

    #double derivatives and crossproducts for alphas
    if len(alphas) > 1:
        alpha_dd_mat[:,0][a02] = rho1[a02] * pdf(z0[a02])/(cdf(z1[a02]) - cdf(z0[a02])) \
                                    - (pdf(z0[a02])/(cdf(z1[a02]) - cdf(z0[a02]))) ** 2
        ab_cross_mat[:,0][a02] = -rho1[a02] * pdf(z0[a02]) /(cdf(z1[a02]) - cdf(z0[a02])) \
                                - (pdf(z0[a02]) * (pdf(z1[a02]) - pdf(z0[a02]))) / (cdf(z1[a02]) - cdf(z0[a02])) ** 2
        

        alpha_dd_mat[:,-1][alast2] = -rho2[alast2] * pdf(z4[alast2]) / (cdf(z4[alast2]) - cdf(z3[alast2])) \
                                    - (pdf(z4[alast2]) / (cdf(z4[alast2]) - cdf(z3[alast2]))) ** 2
        ab_cross_mat[:,-1][alast2] = rho2[alast2] * pdf(z4[alast2]) / (cdf(z4[alast2]) - cdf(z3[alast2])) \
                                + pdf(z4[alast2]) * (pdf(z4[alast2]) - pdf(z3[alast2])) / (cdf(z4[alast2]) - cdf(z3[alast2])) ** 2
        

        for j in xrange(0, k - 2):
            z2, z1 = alphas[j+1] - xb, alphas[j] -xb
            a = Y == j
            alpha_cross_mat[:,j][a] = pdf(z2[a]) * pdf(z1[a]) / (cdf(z2[a]) - cdf(z1[a])) ** 2
    

    #now getting the middle alpha and beta 2nd derivatives:
    if len(alphas) > 2:

        for j in xrange(1, k -2):
            zj0, zj, zj1 = alphas[j-1] - xb, alphas[j] - xb, alphas[j+1] - xb            
            rho = getrhoGQR(xb, alphas[j], 1)
            a1 = Y == j            
            a2 = Y == j + 1            
            alpha_dd_mat[:,j][a1] = -rho[a1] * pdf(zj[a1]) / (cdf(zj[a1]) - cdf(zj0[a1])) \
                                    - (pdf(zj[a1]) / (cdf(zj[a1]) - cdf(zj0[a1]))) ** 2
            alpha_dd_mat[:,j][a2] = rho[a2] * pdf(zj[a2]) / (cdf(zj1[a2]) - cdf(zj[a2])) \
                                    - (pdf(zj[a2]) / (cdf(zj1[a2]) - cdf(zj[a2]))) ** 2

            ab_cross_mat[:,j][a1] = rho[a1] * pdf(zj[a1]) / (cdf(zj[a1]) - cdf(zj0[a1])) \
                                    + pdf(zj[a1]) * (pdf(zj[a1]) - pdf(zj0[a1])) / (cdf(zj[a1]) - cdf(zj0[a1])) ** 2
            ab_cross_mat[:,j][a2] = -rho[a2] * pdf(zj[a2]) / (cdf(zj1[a2]) - cdf(zj[a2])) \
                                    - pdf(zj[a2]) * (pdf(zj1[a2]) - pdf(zj[a2])) / (cdf(zj1[a2]) - cdf(zj[a2])) ** 2

        beta_bool = np.nonzero((Y!=0) & (Y!=len(alphas)))
        z1 = alphas[Y[beta_bool]] - xb[beta_bool]
        z0 = alphas[Y[beta_bool]-1] - xb[beta_bool]
        rho1 = getrhoGQR(xb[beta_bool], alphas[Y[beta_bool]], 1)
        rho0 = getrhoGQR(xb[beta_bool], alphas[Y[beta_bool] -1], 1)
        beta_hess[:,0][beta_bool] = (-rho1 * pdf(z1) + rho0 * pdf(z0)) / (cdf(z1) - cdf(z0)) \
                                  - ((pdf(z1) - pdf(z0))/ (cdf(z1) - cdf(z0))) ** 2

    #quadrants of the hessian
    quad1 = alpha_dd_mat.sum(axis = 0)
    quad2 = np.zeros((len(alphas), n))
    quad4 = np.dot(X.T, X * beta_hess)
    for i in xrange(len(alphas)):
        ab_cross_vec = ab_cross_mat[:,i].reshape((m,1)) * X
        quad2[i,:] = ab_cross_vec.sum(axis = 0)



    if len(alphas) > 1:
        diag2 = alpha_cross_mat.sum(axis = 0)
        quad1 = spar.diags([quad1, diag2, diag2], [0, 1, -1]).todense()


    hess_exp = np.zeros((len(alphas) + n, len(alphas) + n))
    hess_exp[0: len(alphas), 0:len(alphas)] = quad1
    hess_exp[0:len(alphas), len(alphas):] = quad2
    hess_exp[len(alphas):, 0:len(alphas)] = quad2.T
    hess_exp[len(alphas):, len(alphas):] = quad4
    hess_exp[np.isnan(hess_exp)] = 0
    hess_exp[np.isinf(hess_exp)] = 0


    if method == 'optgradient':
        V = la.inv(grad_exp)
    if method == 'invneghess':
        V = la.inv(-hess_exp)
        # V = la.inv(hess_exp)
    if method == 'sandwich':
        V = np.dot(np.dot(la.inv(hess_exp), grad_exp), la.inv(hess_exp))
    
    std_errs = np.sqrt(np.diag(V))



    return quad1, quad2, quad4, hess_exp, std_errs

def getrhoGQR(xb, alpha, sigma):
    """
    This will become more complicated as we add more distributions. 
    """

    rho = (alpha - xb)/sigma**2
    return rho 

def std_errs_prob(Y, X, betas1, method = 'invneghess'):
    """
    I'm just seeing if i can get the same standard errors the simple way
    I'm pretty sure that the X includes a column of ones in it and the betas
    include a constant, so make sure to flip the sign and stuff like that
    """
    m = len(Y)
    betas = betas1.copy()
    print betas, "before negative"
    betas[0] = -1 *betas[0]
    print betas, "after negative"
    X = np.concatenate((np.ones((m,1)), X), axis = 1)
    xb = np.dot(X, betas)
    PDFeval = norm.pdf(xb)
    CDFeval = norm.cdf(xb)
    rho = xb 
    Yvec = np.zeros((m, 1))
    Y_bool = Y == 1
    print rho.shape, "Rho shape"
    print PDFeval.shape, "PDF shape"
    print CDFeval.shape, "CDF shape"    
    Yvec[:,0][Y_bool] = -rho[Y_bool]*PDFeval[Y_bool] / CDFeval[Y_bool] - (PDFeval[Y_bool]/CDFeval[Y_bool]) **2 
    Yvec[:,0][~Y_bool] = rho[~Y_bool] * PDFeval[~Y_bool] / (1 - CDFeval[~Y_bool]) - (PDFeval[~Y_bool]/(1-CDFeval[~Y_bool])) **2
    Yvec[:,0][np.isnan(Yvec[:,0])] = 0
    Yvec[:,0][np.isinf(Yvec[:,0])] = 0
    hess = np.dot(X.T, X * Yvec)
    
    V = la.inv(-hess)
    stderrors = np.sqrt(np.diag(V))

    return betas, xb, PDFeval, CDFeval, rho, Yvec, stderrors
    #this gives me the same results and GQR. I still have to figure out what's wrong
    #with the more general, ordered probit model. Hmmm...




#this function gives me different results than STATA. I'm not sure about dividing by # of obs
#but it seems like in the GQR code they didn't do that it. 
