from __future__ import division
import adorio_ologit as ao
import scipy as sp
import numpy as np
import adorio_ologit as ao
from scipy.optimize import fmin_bfgs, minimize

data = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\ologit_data2.csv', delimiter = ",")

# X = np.concatenate((np.ones((len(data),1)), data[:,1:]), axis = 1)
X = data[:,1:]
Y =  data[:, 0]
Y = Y.astype(int)

beta_guess = [100, 100, 100, 100, 100]
beta = ao.estmlogit(beta_guess, X, Y)

#writing my own estimation function 
m = 3
cri_log = lambda x: ao.negologlik(x, X, Y, m)
#output = minimize(cri_log, beta_guess, method = 'Nelder-Mead', tol = 1e-8)

#figuring out the negologlik command
X = np.concatenate((np.ones((5, 1)), np.random.rand(5, 1)), axis = 1)
Y = np.random.randint(0, 3, (5,1))
L = 0
enum = list(enumerate(zip(X,Y)))
m = 3

beta = [-2, -4, 1, 0.05, .6]
for row, (xrow, ylevel) in enumerate(zip(X,Y)):
    d = [1.0] * m
    xdotb = sum([(x * b) for (x,b) in zip(xrow[1:], beta[m:])])
    pgk = [1.0] * m

    print xdotb, "Xdotb"

    for k in range(1, m):
        v = beta[k] + xdotb
        print v, "alpha + xdotb"
        try:
            if v < -30:
                v = -30
                pgk[k] =1.0 /(1 + exp(-v))
                print pgk[k], "Something called pgk."
        except:
            print " v=", v
    if 0 <= ylevel < m-1 :
       prob = pgk[ylevel] - pgk[ylevel+1]
       print prob, "Probability of that outcome happening"
    elif ylevel == m-1:
       prob = pgk[ylevel]
       print prob, "What happens when ylevel == m-1"
    print pgk, "pgk"