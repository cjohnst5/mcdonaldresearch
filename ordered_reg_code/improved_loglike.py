from __future__ import division
import adorio_ologit as ao
import scipy as sp
import numpy as np
from scipy.optimize import fmin_bfgs, minimize
from scipy.stats import norm

import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *

def llv((paravec_ab_cdf, X, Y, cdf, k, sign = 1):
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function. 

    Parameters:
    .................................
    paravec_ab_cdf:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers and start at 0)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)
    paravec_cdf:
        parameters for the specified cdf. These are usually found using MLE 
        fitting of the pdf to the data. See sgttree/mle folder for MLE functions
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    L:
        The probability of the random variable Y occurring 
    """
    L = 0
    m = len(Y)
    try:
        n = X.shape[1]
    except:
        n = 1
    alpha = paravec_ab_cdf[0:k-1]
    betas = paravec_ab_cdf[k-1:n+k-1]
    paravec_cdf = paravec_ab_cdf[n+k-1:]

    xb = np.dot(X, betas.T)
    LLV = np.zeros_like(Y) 

    d_start = [i for i,x in enumerate(X) if x == 0]
    LLV
    return d_start
    #to do. Finish writing this function. I don't know how helpful it will be. 