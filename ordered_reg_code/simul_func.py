from __future__ import division
import numpy as np
import scipy as sp


def simul_data(numobs, betas, numcat, distr = 'normal'):
    """
    Simulates ordered category data    
    Returns the y, x and alphas
    """
    if distr == 'normal':
        err = sp.random.normal(size = numobs)
    elif distr == 'laplace':
        err = sp.random.laplace(size = numobs)
    numx = len(betas)
    x = sp.random.uniform(0, 1,size = (numobs, numx))
    
    y = np.dot(x, betas) + err
    # y_obs = np.zeros((len(y), 1))
    y_obs = np.zeros_like(y)

    #creating the cutoffs: 
    max_min = y.max() - y.min()   
    alphas = []
    cat = range(numcat)
    for i in xrange(1, numcat):
       alphas.append(max_min * (i/numcat) + y.min())
    alphas = np.asarray(alphas)

    #filling our y_obs vector
    alpha_bool = y < alphas[0]    
    y_obs[alpha_bool] = 0

    alpha_bool = y >= alphas[-1]
    y_obs[alpha_bool] = numcat -1

    for i in xrange(1, numcat - 1):
        alpha_index = np.nonzero((alphas[i-1] < y) & (y <= alphas[i]))
        y_obs[alpha_index] = cat[i] 
    y_obs = y_obs.astype(int)
    return y_obs, x, alphas

#I think this is working quite nicely. 
#trying out the function
Y, X, alphas = simul_data(500, [.5, 2], 5)
data =  np.concatenate((Y.reshape(len(Y), 1), X), axis = 1)
np.savetxt('simul_func.csv', data, delimiter = ",")