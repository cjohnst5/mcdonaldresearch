from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc 
from matplotlib import pyplot as plt
from scipy.stats import norm

#writing my cdfs so they have the scale parameter included in them. 
def sged_cdf_scale(paravec, y):
    lamb, p = paravec 
    #our two scale parameters  
    m = 0                                                           #we are not estimating the location or scale parameter. 
    phi = (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) #we get this from calculating variance
                                                                    #of the SGED and solving for phi. 
    
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf

def ged_cdf_scale(paravec, y):
    p = paravec

    #our two scale parameters
    m = 0
    phi = (gamma(1/p) / gamma(3/p))**(1/2)
  
    z = abs(y-m) ** p / (phi ** p)
    cdf = (1) / 2 + ((1) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def snormal_cdf_scale(paravec, y):
    lamb = paravec

    #our two scale parameters
    m = 0 
    phi = (gamma(1/2)/ (gamma(3/2) + lamb**2 * (3 * gamma(3/2) - 4 / gamma(1/2)))) ** (1/2)
    
    z = abs(y-m) ** 2 / (phi ** 2 * (1 + lamb * np.sign(y-m)) ** 2 )
    cdf = cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf


def slaplace_cdf_scale(paravec, y):
    lamb = paravec
    #our two scale parameters. Phi is a function of sigma^2, but we standardize 
    #so variance = 1. 
    m = 0
    phi = 1 / (gamma(3) + lamb ** 2 * \
         (3 * gamma(3) - 4 * gamma(2)**2))**(1/2)  

    z = abs(y-m) / (phi * (1 + lamb * np.sign(y-m)))
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1, z)
    return cdf

def laplace_cdf_scale(paravec, y):
    #our phi is calculated whe sigma^2 = 1. 
    m, lamb, phi, p = 0, 0, 1/2**(1/2), 1  
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf




#writing a phi function just so cdf's are easy to plot
def phi (paravec):
    """
    Given that sigma = 1 (due to standardizing), this the calculation of the scale parameter, phi
    that we need for our sged pdfs and cdfs. 
    """
    lamb, p = paravec
    phi = (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    return phi


