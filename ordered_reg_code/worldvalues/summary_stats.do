//Sept 18, 2014
//This is for the random 10000 observations starting at observation 20000 in the 
//randomized worldvalues data set (worldvalues_random.csv)

cd "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\shinycode\10000 obs, sept_16"
import delimited "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues_random.csv", clear
//ssc install outreg2

gen num = _n
sum if n > 20000 & num <=30000 
outreg2 using sum_stats.doc, replace 

//oprobit satisfaction religious male income yearsold yearsold2 married unemployed high_school 
//predict yhat 
 
