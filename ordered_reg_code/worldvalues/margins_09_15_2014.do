clear all 
import delimited "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues_random.csv"
gen n = _n
oprobit satisfaction religious male income yearsold yearsold2 married unemployed high_school if n>20000 & n <= 30000
mfx compute, predict(outcome(5))
