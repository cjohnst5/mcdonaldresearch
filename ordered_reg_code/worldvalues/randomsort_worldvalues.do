//Radomizing the pared down world values data set
//Sept 13, 2014

import delimited "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues.csv", clear

tempvar sortorder
gen `sortorder' = runiform()
sort `sortorder'
cd "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues"
export delimited satisfaction religious male income yearsold yearsold2 married unemployed high_school using ".\worldvalues_random.csv", replace
