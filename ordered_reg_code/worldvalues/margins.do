clear all
import delimited "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues.csv"
gen n = _n
oprobit satisfaction religious male income yearsold yearsold2 married unemployed high_school if n<=10000

*margins, predict(outcome(0)) atmeans
margins, atmeans

*Trying it with binary data
import delimited "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\horowitz.csv", clear 
oprobit v1 v2 v3 v4 v5
