set more off 
use "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\datasets\wvs1981_2008_v20090914_stata (1)\wvs1981_2008_v20090914_stata.dta", clear
keep A170 X001 X047 F024 F025 G007_64 A165 A173 X003 X007 X028 X025 A009 S003 S002
//Variable I wonder about are trust, income and religion. Sorta important. Haha. 

****Generating my age variables
gen age24=.
replace age24=0 if !missing(X003)
replace age24=1 if X003<25

gen age25_34 = .
replace age25_34=0 if !missing(X003)
replace age25_34=1 if X003>=25 & X003<=34 

gen age35_44 = .
replace age35_44 = 0 if !missing(X003)
replace age35_44 = 1 if X003>=35 & X003<=44

gen age45_54 = .
replace age45_54 = 0 if !missing(X003)
replace age45_54 = 1 if X003>=45 & X003<=54

gen age55_64 = .
replace age55_64 = 0 if !missing(X003)
replace age55_64 = 1 if X003>=55 & X003<=64

gen age65=.
replace age65 = 0 if !missing(X003)
replace age65 = 1 if X003>=65

***Generating marital status
gen married = .
replace married = 0 if !missing(X007)
replace married = 1 if X007 == 1 | X007 == 2

gen div_sep_wid = .
replace div_sep_wid = 0 if !missing(X007)
replace div_sep_wid = 1 if X007 == 3 | X007 == 4 | X007 ==5 | X007 == 7

gen single = .
replace single = 0 if !missing(X007)
replace single = 1 if X007 == 6 | X007 ==8

***Generating work status
gen fulltime = .
replace fulltime = 0 if !missing(X028)
replace fulltime = 1 if X028 == 1

gen partime = .
replace partime = 0 if !missing(X028)
replace partime = 1 if X028 == 2

gen retired = .
replace retired = 0 if !missing(X028)
replace retired = 1 if X028 == 4

gen housewife_student = .
replace housewife_student = 0 if !missing(X028)
replace housewife_student = 1 if X028 == 5 | X028 ==6 

gen unemployed = .
replace unemployed = 0 if !missing(X028)
replace unemployed = 1 if X028 == 7

gen other_emp = .
replace unemployed = 0 if !missing(X028)
replace unemployed = 1 if X028 == 8 | X028 == 3

***Generation education levels
gen noprimary = .
replace noprimary = 0 if !missing(X025)
replace noprimary = 1 if X025 == 1

gen primary = .
replace primary = 0 if !missing(X025)
replace primary = 1 if X025 == 2

gen inc_sec_tech = .
replace inc_sec_tech = 0 if !missing(X025)
replace inc_sec_tech = 1 if X025 == 3

gen sec_tech = .
replace sec_tech = 0 if !missing(X025)
replace sec_tech = 1 if X025 == 4

gen inc_sec_uni = .
replace inc_sec_uni = 0 if !missing(X025)
replace inc_sec_uni = 1 if X025 == 5

gen sec_uni = .
replace sec_uni = 0 if !missing(X025)
replace sec_uni = 1 if X025 == 6

gen lower_ter = .
replace lower_ter = 0 if !missing(X025)
replace lower_ter = 1 if X025 == 7

gen upper_ter = . 
replace upper_ter = 0 if !missing(X025)
replace upper_ter = 1 if X025 == 8

***Generating health variables
gen poor_health = .
replace poor_health = 0 if !missing(A009)
replace poor_health = 1 if A009 == 4 | A009 == 5 

gen average_health = .
replace average_health = 0 if !missing(A009)
replace average_health = 1 if A009 == 3

gen good_health = .
replace good_health = 0 if !missing(A009)
replace good_health = 1 if A009 == 2

gen verygood_health = .
replace verygood_health = 0 if !missing(A009)
replace good_health = 1 if A009 == 1

*** Country and wave controls
tabulate S003, generate(country)
tabulate S002, generate(wave)

************Dropping all the variables that will not be used in ordered main**********
drop S002 S003 A009 F025 X007 X025 X028 /*X003 is age, could be dropped*/

**not sure which trust and religion variable to keep, so we'll just keep the binary
drop G007_64
**dropping age, marriage, education, health variables to avoid multicollinearity
//drop age24 single other_emp noprimary 

/*Relabeling the variable names so i know what they are and adjusting binary variables*/
rename A170 satisfaction
replace satisfaction = satisfaction -1
rename A165 trust
replace trust = 0 if trust == 2
rename A173 freedom
rename F024 religious
rename X001 male
replace male = 0 if male == 2
rename X047 income 
rename X003 yearsold
gen yearsold2 = yearsold*yearsold

//cd "C:\Users\cjohnst5\Desktop\mcdonaldresearch\ordered_reg_code"
//save worldvalues_simpled, replace 

//keeping the variables I used in ordered probit
keep satisfaction trust freedom religious male income yearsold yearsold2 married ///
div_sep_wid fulltime partime retired housewife_student unemployed primary inc_sec_tech ///
sec_tech inc_sec_uni sec_uni lower_ter upper_ter wave2 wave3 

//oprobit regression that I want to mimic in python 
oprobit satisfaction trust freedom religious male income yearsold yearsold2 married div_sep_wid fulltime partime retired housewife_student unemployed primary inc_sec_tech sec_tech inc_sec_uni sec_uni lower_ter upper_ter  wave2 wave3

//getting missing observations out of there. 
drop if missing(satisfaction) | missing(trust) | missing(freedom) | missing(religious) | missing(male) | missing(income) | missing(yearsold) | missing(yearsold2) | missing(married) | ///
missing(div_sep_wid) | missing(fulltime) | missing(partime) | missing(retired) | missing(housewife_student) | missing(unemployed) | missing(primary) | missing(inc_sec_tech) | ///
missing(sec_tech) | missing(inc_sec_uni) | missing(sec_uni) | missing(lower_ter) | missing(upper_ter) | missing(wave2) | missing(wave3) 

//getting a sample of 10000 and then running the ordered probit to see if 
//it's different
//sample 10


/**** Commands from here on are me fiddling with the number 
of observations so I can try and get the program to work in python*/
//sample 10
oprobit satisfaction trust freedom religious male income yearsold yearsold2 married div_sep_wid fulltime partime retired housewife_student unemployed primary inc_sec_tech sec_tech inc_sec_uni sec_uni lower_ter upper_ter wave2 wave3  //running a simpler oprobit
//the below regression is in the order that the variables are exported to a csv. 
oprobit satisfaction religious male income yearsold yearsold2 married unemployed primary 


***Creating an excel spreadsheet
export delimited satisfaction religious male income yearsold yearsold2 married unemployed primary ///
using "C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues.csv", nolabel replace
//inc_sec_tech sec_tech inc_sec_uni sec_uni lower_ter upper_ter 
//All the variables. I just want to run it with a few variables to see. 
