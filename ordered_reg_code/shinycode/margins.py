from __future__ import division
import numpy as np
import sgtpdfs as sgtp
import pandas as pd
import os


def get_margins(pdf, pdf_param, x, y_outcome, betas, cutoffs, moment):
	"""
	This function calculates the margins at a specified moment of the independent variables (means, median, etc)

	Parameters
	------------------------------------------------------------------------------------------------------------
	pdf: Designated which distribution was used to obtain the betas with MLE
	x: A nxk numpy matrix of the x's 
	y_outcome = either a integer indicating what y category you are interested in finding the margins at, or "all" indicating
	you would like to find the marginal effects at all outcomes of y. Categories of y go from 0 to J-1, J being the 
	number of all outcomes. 
	betas = a kx1 numpy array of beta values, estimated using MLE
	cutoffs = The J-1x1 numpy array of estimated cutoffs for partitioning the y variable (J categories in y, one less cutoff)
	moment: either "mean" or "median"  

	Returns
	------------------------------------------------------------------------------------------------------------
	The marginal effects as a 1XK numpy vector (if only one y outcome is specified) or a JXK numpy matrix if "all"
	outcomes is specified. 

	"""

	#Condensing our x variables to the correct moments
	J = cutoffs.size + 1
	K = betas.size
	margins = np.zeros((J,K))
	x_condensed = 0
	pdf_vec = [1,pdf_param] 	

	if moment == "mean":
		x_condensed = x.mean(axis = 0)
	elif moment == "median":
		x_condensed = x.median(axis = 0)


	#Finding the margins given the y_outcome specified
	xb = np.dot(x_condensed, betas)
	if y_outcome == "all":		

		margins[0,0:] = -betas*pdf(pdf_vec, cutoffs[0] - xb)		
		for i in xrange(1,J-1):
			margins[i,0:] = (betas*(pdf(pdf_vec, cutoffs[i-1] - xb) - pdf(pdf_vec, cutoffs[i] - xb)))
		margins[J-1,0:] = (betas*pdf(pdf_vec, cutoffs[J-2] - xb))
	else: 
		if y_outcome == 0:
			margins = -betas*pdf(pdf_vec, cutoffs[0] - xb)
		elif y_outcome == J-1:
			margins = betas*pdf(pdf_vec, cutoffs[J-2] - xb)
		else:
			margins = betas*(pdf(pdf_vec, cutoffs[y_outcome-1] - xb) - pdf(pdf_vec, cutoffs[y_outcome] - xb))

	return margins


def write_to_excel_margins(data_matrix, dist, obs):
	"""
	This function takes the marginal effects for all categories of y for a given distribution and writes 
	them to a csv file. 

	Parameters
	-----------------------------------------------------------------------------------------------------
	data_matrix: JXK matrix of the marginal effects for each outcome of y
	dist: String that specifies the distribution of the error terms
	obs: integer that indicates how many observations were used in the MLE


	"""
	#Creating the row lables (each row corresponds to a y category)
	i = ["y=0", "y=1", "y=2", "y=3", "y=4", "y=5", "y=6", "y=7", "y=8", "y=9",]

	#Creating labels for the columns (the different betas)
	col =["religious",	"male",	"income",	"yearsold",	"yearsold2",	"married",	"unemployed",	"high_school"]

	df = pd.DataFrame(data = data_matrix, index = i, columns = col)

	filename = 'margins_for_' + dist + '_with_' + str(obs)+'_obs.csv'
	df.to_csv(filename)



# -------------------Getting the marginal effects for world values data---------------------------------
#X data
data = np.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues_random.csv', delimiter = ',', skiprows = 1)
obs = 10000
start_index = 20000
x = data[start_index:start_index + obs,1:]
# y_outcome = "all"
y_outcome = 5

#MLE results
myDirname = os.path.normpath('C:/Users/Daniel and Carla/Documents/carlascode/mcdonaldresearch/ordered_reg_code/shinycode/10000 obs, sept_16/betas_cutoffs_formargins.csv')
betas_num = 8
cutoffs_num = 9
betas_alphas = np.loadtxt(myDirname, delimiter = ',', usecols = np.arange(1,betas_num+cutoffs_num+1), skiprows = 1)
pdf_param_all = [[0], [0], [0.281032115], [0.280507106, 1.033853416]]
pdf_vec = [sgtp.normal_pdf, sgtp.laplace_pdf, sgtp.slaplace_pdf, sgtp.sged_pdf]
pdf_vec_names = ['normal', 'laplace', 'slaplace', 'sged']


for i in xrange(3,4):
	betas = betas_alphas[i,0:betas_num]
	cutoffs = betas_alphas[i, betas_num:]
	pdf_values = pdf_param_all[i]
	pdf = pdf_vec[i]
	print "pdf", pdf_values
	margins_all = get_margins(pdf, pdf_values, x, y_outcome, betas, cutoffs, moment = "mean")
	print "Margins for", y_outcome, margins_all
	# write_to_excel_margins(margins_all, pdf_vec_names[i], obs)

# print "param", param, param.shape