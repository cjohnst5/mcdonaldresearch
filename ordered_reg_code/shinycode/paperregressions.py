# Working on my results for my paper
# July 15, 2014
from __future__ import division
import scipy as sp
import numpy as np
from time import time
import csv
import pandas as pd
from matplotlib import pyplot as plt
from sgtpdfs import *

from opt_main2 import *
from ordered_main import *
from std_errs1 import *

# # cdf_case: 
# #         1: Normal
# #         2: Laplace
# #         3: Snormal
# #         4: GED
# #         5: Slaplace
# #         6: SGED
# #         7: ST. This is still pretty slow on optimization

def write_to_excel(data, dist,obs):
    alphas, betas, thetas, betas_steds, alphas_steds, ll_restricted, ll_unrestricted, lr_test, est_param = data
    est_param_success = [est_param.success]
    ll_restricted = [ll_restricted]
    ll_unrestricted = [ll_unrestricted]
    lr_test = [lr_test]
    i = ['success', 'alphas', 'alphas_steds', 'betas', 'betas_steds', 'thetas', 'll_restricted', 'll_unrestricted', 'lr_test']

    df_a = pd.DataFrame(data=alphas).transpose()
    df_b = pd.DataFrame(data=alphas_steds).transpose()
    df_t = pd.DataFrame(data=betas).transpose()
    df_bs = pd.DataFrame(data=betas_steds).transpose()
    df_as = pd.DataFrame(data=thetas).transpose()
    df_lr = pd.DataFrame(data=ll_restricted).transpose()
    df_ur = pd.DataFrame(data=ll_unrestricted).transpose()
    df_lt = pd.DataFrame(data=lr_test).transpose()
    df_s = pd.DataFrame(data=est_param_success).transpose()

    df_a = df_s.append(df_a)
    df_a = df_a.append(df_b)
    df_a = df_a.append(df_t)
    df_a = df_a.append(df_bs)
    df_a = df_a.append(df_as)
    df_a = df_a.append(df_lr)
    df_a = df_a.append(df_ur)
    df_a = df_a.append(df_lt)

    df_a = pd.DataFrame(data=df_a.values,index=i)

    filename = dist+ '_with_' + str(obs)+'_obs_old.csv'
    df_a.to_csv(filename)
    #----------------Creating an excel file that is easier to copy and paste over---------------------------------------
    param_vec = np.concatenate((alphas, betas, ll_unrestricted, ll_restricted, lr_test, thetas))
    std_vec = np.concatenate((alphas_steds, betas_steds))
    df_param = pd.DataFrame(data=param_vec).transpose()
    df_std = pd.DataFrame(data=std_vec).transpose()
    df_all = df_param.append(df_std)
    filename = dist+ '_with_' + str(obs)+'_obs.csv'
    df_all.to_csv(filename)

data = np.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues_random.csv', delimiter = ',', skiprows = 1)
obs = 10000
het_form = None
start_index = 20000
Y = data[start_index:start_index+obs,0]
Y = Y.astype(int)
X = data[start_index:start_index + obs,1:]
print data[start_index,1:], "first obs"
print data[start_index+obs,1:], "last obs"
dist_list = ['normal', 'laplace', 'snormal', 'ged', 'slaplace', 'sged', 'st']

# Obtaining our regression results using distributions specified in the range -------------------------------------------
for cdf_case in range(1,8):
    print "Working on ", dist_list[cdf_case-1],"..."
    begin = time()

    distribution_results = output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 100000, 'maxfev' : 100000}), method_std = 'optgradient')

    end = time() - begin
    print "Total time", end
    number = np.size(Y)
    print "Number of observations", number

    write_to_excel(distribution_results, dist_list[cdf_case-1],obs)




