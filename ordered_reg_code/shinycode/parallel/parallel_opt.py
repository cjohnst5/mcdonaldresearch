# ##practicing sending messages between functions in different proccesses
# ##July 4, 2014
# from __future__ import division 
# import mpi4py
# from mpi4py import MPI
# from mpi4py.MPI import ANY_SOURCE
# import numpy as np
# import scipy as sp
# from scipy.optimize import minimize
# import sys
# import math

# sys.path.insert(0, '../')

# import improved_loglike as ll

# comm = MPI.COMM_WORLD
# rank = comm.Get_rank()
# size = comm.Get_size()
# n = 10
# print "size",size
# quotient = int(math.floor(n/(size-1)))
# remainder = n % (size-1)

# print quotient,remainder


# #initializing a receiving array
# x_local_sum = np.zeros(1)
# parameter = np.zeros(1)
# stop_signal = np.array([float('NaN')])
# stop = False
# hundred = np.arange(1,n+1).astype(float)
# #length = hundred.size/(size-1)

# data_vec = np.array([])

# if (rank != 0) and (rank < remainder+1):    
#     data_vec = hundred[(quotient+1)*(rank-1):(quotient+1)*(rank)]
#    # print rank,(quotient+1)*(rank-1),(quotient+1)*(rank)-1
# elif (rank > remainder):
#     data_vec = hundred[remainder+quotient*rank-quotient:remainder+quotient*rank]
#    # print rank,remainder+quotient*rank-quotient,remainder+quotient*rank-1

# #print data_vec

# print "process rank ",rank, "is working on " ,data_vec

# #.....................Functions..............................
# # function that sends out the parameter and receives local sums
# def farm_function(parameter):   


#     print "Parameter fed to farm", parameter
#     for i in xrange(size-1):
#         comm.Send(parameter, dest = i+1)
#         print "sent to rank", i+1    

#     total = np.zeros(1)    
#     for i in xrange(size-1):
#         comm.Recv(x_local_sum, ANY_SOURCE) #receiving the chunks of summed loglikes from all the worker functions
#         total[0] += x_local_sum[0] #summing the chunks together 

#     print "Rank", rank, "has received all local sums:", total

#     return abs(total - parameter) #eventually all we will need to return is the total
    

# #function that takes the data and works with it. 
# def worker_func(): 
#     # if stop != 0:
#     #     return None
#     comm.Recv(parameter, source = 0)
#     if math.isnan(parameter[0]):
#         print "Stop signal received for rank ",rank
#         stop = True
#         return None
#     else:
#         print "Parameter received:", parameter, "for rank", rank

#         x_local_sum = data_vec.sum() #Where we need to feed it the log likelihood function 
#         comm.Send(x_local_sum, dest = 0)
#         print "Rank", rank, "sent back it's local sum", x_local_sum 
#         return x_local_sum

# #...............................................................


# if rank == 0:
#     stop = True
#     guess = 0 
#     final_parameter = minimize(farm_function, guess, method = 'Nelder-Mead')
#     print "minimize done"
#     for i in xrange(size-1):
#         comm.Send(stop_signal, dest = i+1)
#         print "Sent stop signal to rank", i+1

#     print "Parameter",final_parameter.x

# else: 
#     while stop == False:
#         print "while stop was ",stop, " for rank ",rank
#         x_local_sum = worker_func()
#         print "Local x sum for rank", rank, ":", x_local_sum
#         if x_local_sum == None:
#             break

##practicing sending messages between functions in different proccesses
##July 4, 2014
from __future__ import division 
import mpi4py
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
import numpy as np
import scipy as sp
from scipy.optimize import minimize
import sys
import math
import time
import csv

sys.path.insert(0, '../')
import improved_loglike as ll
#import paperregressions as pr
from opt_main2 import *
from ordered_main import *
from std_errs1 import *



def showtime(msg="elapsed time"):
    global lasttime
    newtime = time.time()
    print "{}: {:.2f}".format(msg, 1000*(newtime-lasttime))
    lasttime = newtime

data = np.array([])
test = np.array([])
init_done = False
global o_probit_test
global alphas
global betas
global thetas
global betas_steds
global alphas_steds
global ll_restricted
global ll_unrestricted
global lr_test

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    test = np.array([3])
    data = np.loadtxt('worldvalues.csv', delimiter = ',', skiprows = 1)
    global init_done
    init_done = True

while init_done == False:
    pass

cdf_case = 6
het_form = None
obs = 1000
Y = data[0:obs,0]
Y = Y.astype(int)
X = data[0:obs,1:]
print "rank ",rank," got here1 ", test[0]

if rank == 0:
    o_probit_test = output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 100000, 'maxfev' : 100000}), method_std = 'optgradient')
    alphas, betas, thetas, betas_steds, alphas_steds, ll_restricted, ll_unrestricted, lr_test = o_probit_test
    print "rank ",rank," got his crap done"

number = np.size(Y)
print "Number of observations", number
print "rank ",rank," got here2"
lasttime = None

if rank == 0:
  lasttime = time.time()

#print "size",size
quotient = int(math.floor(obs/(size-1)))
remainder = obs % (size-1)
print "rank ",rank," got here3"

#print quotient,remainder


#initializing a receiving array
x_local_sum = np.zeros(1)
parameter = np.zeros(1)
stop_signal = np.array([float('NaN')])
stop = False
hundred = np.arange(1,obs+1).astype(float)

data_vec = np.array([])

if (rank != 0) and (rank < remainder+1):    
    data_vec = hundred[(quotient+1)*(rank-1):(quotient+1)*(rank)]
elif (rank > remainder):
    data_vec = hundred[remainder+quotient*rank-quotient:remainder+quotient*rank]


#print "process rank ",rank, "is working on " ,data_vec

#.....................Functions..............................
# function that sends out the parameter and receives local sums
def farm_function(parameter):   


  #  print "Parameter fed to farm", parameter
    for i in xrange(size-1):
        comm.Send(parameter, dest = i+1)
   #     print "sent to rank", i+1    

    total = np.zeros(1)    
    for i in xrange(size-1):
        comm.Recv(x_local_sum, ANY_SOURCE) #receiving the chunks of summed loglikes from all the worker functions
        total[0] += x_local_sum[0] #summing the chunks together 

   # print "Rank", rank, "has received all local sums:", total

    return abs(total - parameter) #eventually all we will need to return is the total
    

#function that takes the data and works with it. 
def worker_func(): 
    # if stop != 0:
    #     return None
    comm.Recv(parameter, source = 0)
    if math.isnan(parameter[0]):
      #  print "Stop signal received for rank ",rank
        stop = True
        return None
    else:
      #  print "Parameter received:", parameter, "for rank", rank

        x_local_sum = data_vec.sum() #Where we need to feed it the log likelihood function 
        comm.Send(x_local_sum, dest = 0)
      #  print "Rank", rank, "sent back it's local sum", x_local_sum 
        return x_local_sum

#...............................................................


if rank == 0:
    stop = True
    guess = 0 
    final_parameter = minimize(farm_function, guess, method = 'Nelder-Mead')
    print "minimize done"
    for i in xrange(size-1):
        comm.Send(stop_signal, dest = i+1)
      #  print "Sent stop signal to rank", i+1

    print "Parameter",final_parameter.x
    showtime("end")

else: 
    while stop == False:
       # print "while stop was ",stop, " for rank ",rank
        x_local_sum = worker_func()
      # print "Local x sum for rank", rank, ":", x_local_sum
        if x_local_sum == None:
            break

