##practicing sending messages between functions in different proccesses
##May 8, 2014
from __future__ import division 
import mpi4py
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
import numpy as np
import scipy as sp
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
n = 10

#initializing a receiving array
x_local = np.zeros(n/(size-1))
x_local_sum = np.zeros(1)
parameter = np.zeros(1)
hundred = np.arange(1,n+1).astype(float)
length = hundred.size/(size-1)
# print "hundred vector", hundred
#.....................Functions..............................
# function that divides up the data and farms it out
def farm_function(parameter):
    # length = hundred.size/(size)
    # # print "length", length
    # divided_vector = []
    # for i in xrange(size-1):
    #     x_local = hundred[0+length*i:length*(i+1)]
    #     # comm.Send(x_local, dest = i+1)  
    #     divided_vector.append(x_local)
    # divided_vector = np.array(divided_vector)

    for i in xrange(size-1):
        comm.Send(parameter, dest = i+1) 

    # # some sort of scatter command
    # comm.Scatter(divided_vector, x_local, root=0)
    # print "Scatter is done"
    # temp = comm.gather(sum_local, root = 0)
    # return temp

    total = np.zeros(1)
    # print "reduce is running in process ", rank
    # comm.Reduce(x_local_sum, total, op = MPI.SUM)  
    # print "reduce done"      
    for i in xrange(size-1):
        comm.Recv(x_local_sum, ANY_SOURCE)
        total[0] += x_local_sum[0]

   


    return abs(total - parameter)
    # return divided_vector

#function that takes the data and works with it. 
def worker_func():
    # comm.Recv(x_local, source = 0)
    # print "local data for rank", rank, "before sum:", x_local
    comm.Recv(parameter, source = 0)
    print "Parameter received", parameter, "for rank", rank
    x_local_sum = hundred[0+length*(rank-1):length*(rank)].sum()
   comm.Send(x_local_sum, dest = 0)
    return x_local_sum

#...............................................................

if rank == 0: 
#initializing the array we want to send out
    
    # temp = farm_function(size)
    # print "Results", temp   
    # print "Type", type(temp)

    

    # length = hundred.size/(size)
    # divided_vector = []
    # for i in xrange(size):
    #     x_local = hundred[0+length*i:length*(i+1)]
    #     # comm.Send(x_local, dest = i+1)  
    #     divided_vector.append(x_local)

    # divided_vector = np.array(divided_vector)
    # print "Divided vector", divided_vector
    # # comm.Scatter(divided_vector, x_local)
    # print "Scatter is done"

else: 
    x_local_sum = worker_func()
    # print "Local x sum for rank", rank, ":", x_local_sum
   
print "Local x for rank", rank, ":", x_local

# total = np.zeros(1)
# comm.Reduce(x_local_sum, total, op = MPI.SUM) 
# print "Total after reduce", total