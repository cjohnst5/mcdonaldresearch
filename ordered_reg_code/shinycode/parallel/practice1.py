# from __future__ import division 
import mpi4py
from mpi4py import MPI
import numpy as np
# import scipy as sp
# import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

x_local = np.zeros(1)

if rank == 0:
    x_local = np.random.random_sample(1)
    x_local[0] = 5
    print x_local
    comm.Send(x_local, dest = 1)
if rank == 1:
    comm.Recv(x_local, source = 0)
    print "Rank", rank, "has", x_local[0]



# import numpy
# from mpi4py import MPI
# comm = MPI.COMM_WORLD
# rank = comm.Get_rank()

# randNum = numpy.zeros(1)

# if rank == 0:
#     print "1"
#     randNum = numpy.random.random_sample(1)
#     print "Process", rank, "drew the number", randNum
#     comm.Send(randNum, dest=1)

# if rank == 1:
#     print "0"
#     print "Process", rank, "before receiving has the number", randNum[0]
#     comm.Recv(randNum, source=0)
#     print "Process", rank, "received the number", randNum[0]