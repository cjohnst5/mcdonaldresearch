import mpi4py
from mpi4py import MPI
import numpy as np
import scipy as sp
import sys

#For correct performance, run with three processes
#Collective Homework 5
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size


A = sp.array([[1.,2.,3.],[4.,5.,6.],[7.,8.,9.]]) if comm.rank == 0 else None
x = sp.ones(3) #why would we need to bcast this vector? 

#initializing things I will need
b = sp.ones(3) if comm.rank == 0 else None
dot = sp.zeros(1)
local_a = sp.zeros(len(x))

#Scattering my A vector to all the world. 
# if rank == 1:
# 	comm.Scatter(A, local_a, root = 0)
comm.Scatter(A, local_a)

#Now let's dot product this thing. 
local_dot = sp.array([sp.dot(local_a, x)])

if rank == 0:
    print "zero", local_a, local_dot
if rank == 1: 
    print "one", local_a, local_dot
if rank == 2:
    print "two", local_a, local_dot

#ok. Gather all my things. 
temp = comm.gather(local_dot, root = 0)
if rank ==0:
    for i in xrange(len(x)):
        b[i] = temp[i]
    print 'result', b
    print 'Temp', temp
    print "TEmp[0]", temp[0]

#this is done. Comments and all. 