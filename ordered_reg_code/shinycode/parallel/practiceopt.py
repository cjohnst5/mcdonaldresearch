##practicing sending messages between functions in different proccesses
##July 4, 2014
from __future__ import division 
import mpi4py
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
import numpy as np
import scipy as sp
from scipy.optimize import minimize
import sys
import math

sys.path.insert(0, '../')

import improved_loglike as ll

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
n = 10


#initializing a receiving array
x_local_sum = np.zeros(1)
parameter = np.zeros(1)
stop_signal = np.array([float('NaN')])
stop = False
hundred = np.arange(1,n+1).astype(float)
length = hundred.size/(size-1)
#.....................Functions..............................
# function that sends out the parameter and receives local sums
def farm_function(parameter):   


    print "Parameter fed to farm", parameter
    for i in xrange(size-1):
        comm.Send(parameter, dest = i+1)
        print "sent to rank", i+1    

    total = np.zeros(1)    
    for i in xrange(size-1):
        comm.Recv(x_local_sum, ANY_SOURCE) #receiving the chunks of summed loglikes from all the worker functions
        total[0] += x_local_sum[0] #summing the chunks together 

    print "Rank", rank, "has received all local sums:", total

    return abs(total - parameter) #eventually all we will need to return is the total
    

#function that takes the data and works with it. 
def worker_func(): 
    # if stop != 0:
    #     return None
    comm.Recv(parameter, source = 0)
    if math.isnan(parameter[0]):
        print "Stop signal received for rank ",rank
        stop = True
        print "stop changed to ",stop
        return None
    else:
        print "Parameter received:", parameter, "for rank", rank
        x_local_sum = hundred[0+length*(rank-1):length*(rank)].sum() #Where we need to feed it the log likelihood function 
        comm.Send(x_local_sum, dest = 0)
        print "Rank", rank, "sent back it's local sum", x_local_sum 
        return x_local_sum

#...............................................................


if rank == 0:
    stop = True
    guess = 0 
    final_parameter = minimize(farm_function, guess, method = 'Nelder-Mead')
    print "minimize done"
    for i in xrange(size-1):
        comm.Send(stop_signal, dest = i+1)
        print "Sent stop signal to rank", i+1

    print "Parameter",final_parameter.x

else: 
    while stop == False:
        print "while stop was ",stop, " for rank ",rank
        x_local_sum = worker_func()
        print "Local x sum for rank", rank, ":", x_local_sum
        if x_local_sum == None:
            break

   


