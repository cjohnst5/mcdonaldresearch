from __future__ import division
import scipy as sp
import numpy as np
import time
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm, laplace, t 
from scipy.optimize import fmin_bfgs, minimize


from opt_main import *
from ordered_main import *
from std_errs1 import *
from sigma_calc import *
from sgtcdfs import *
from sgtpdfs import *
from loglike import *
from improved_loglike import *

import sys
sys.path.insert(0, 'C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code')
from simul_func import *
# data = sp.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\HOROWTZ5.txt')

# data = np.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\gss2_12.csv', delimiter = ',')
# # """
# # cdf_case: 
# #         1: Normal
# #         2: Laplace
# #         3: Snormal
# #         4: GED
# #         5: Slaplace
# #         6: SGED
# #         7: ST. This is still pretty slow on optimization
# # """
# Y = data[:,0]
# X = data[:,1:]
# k = 3
# loglike_test = loglike(betas, [-.05133, .536751], X, Y, sged_cdf, k, None)
#so my sigma and loglikelihood is working with the normal. Let's try ged

#Testing out my start values function. 
# initial_guess = int_guess_calc(Y, X, 1, het_form = X)
# so that works. 

#testing to see if my optimization functino works: 
#first we'll try probit with no heteroskedasticity and then try out the 
#rest of the distributions. 
#Then, with simulated data, we'll try out probit and the rest of the 
#distributions. 


# est_param_prob = calling_opt(1, None, Y, X)
# print "Estimated Parameters", est_param_prob


#so this works for all the distributions. Let's try using multiple categories
# k = 10
# Y, X, alphas = simul_data(500, [-3, 2, 5, 100, 12, 21], k)
# data = np.concatenate((Y.reshape(len(Y),1), X), axis = 1)
# np.savetxt('maincheck.csv', data, delimiter = ',')
# #so this also works for multiple categories. ;) :)



# #checking to see whether my new standard errors work the way the way they should. 
#it does for two categories. Let's see what happens with multiple categories. 


# est_param1 = calling_opt(cdf_case, Y, X, het_form)[4].x
# if k == 2:
#     alphas = [est_param1[0]]
#     if het_form == None:        
#         if cdf_case < 3:
#             betas = est_param1[1:]
#             dist_param = []
#         if cdf_case >= 3 and cdf_case <6:
#             betas = est_param1[1:-1]
#             dist_param = [est_param1[-1]]
#         if cdf_case == 6 or cdf_case == 7:
#             betas = est_param1[1:X.shape[1]+1]
#             dist_param = est_param1[-2:]
#     else: 
#         numdelt = het_form.shape[1]
#         if cdf_case < 3:
#             betas = est_param1[k-1:-numdelt]
#             deltas = est_param1[-numdelt:]
#             dist_param = []
#         if cdf_case >= 3 and cdf_case < 6:
#             betas = est_param1[k-1:-(numdelt+1)]
#             dist_param = [est_param1[-(numdelt+1)]]
#             deltas = est_param1[-numdelt:]
#         if cdf_case == 6 or cdf_case ==7: 
#             betas = est_param1[k-1:-(numdelt+2)]
#             dist_param = est_param1[-(numdelt + 2):-numdelt]
#             deltas = est_param1[-numdelt]
        

# else:
#     alphas = est_param1[0:k-1]
#     if het_form == None:        
#         if cdf_case < 3:
#             betas = est_param1[k-1:]
#             dist_param = []
#         if cdf_case >= 3 and cdf_case <6:
#             betas = est_param1[k-1:-1]
#             dist_param = [est_param1[-1]]
#         if cdf_case == 6 or cdf_case == 7:
#             betas = est_param1[k-1:-2]
#             dist_param = est_param1[-2:]
#     else:
#         numdelt = het_form.shape[1]
#         if cdf_case < 3:
#             betas = est_param1[k-1:-numdelt]
#             deltas = est_param1[-numdelt:]
#             dist_param = []
#         if cdf_case >= 3 and cdf_case < 6:
#             betas = est_param1[k-1:-(numdelt+1)]
#             dist_param = [est_param1[-(numdelt+1)]]
#             deltas = est_param1[-numdelt:]
#         if cdf_case == 6 or cdf_case == 7: 
#             betas = est_param1[k-1:-(numdelt+2)]
#             dist_param = est_param1[-(numdelt + 2):-numdelt]
#             deltas = est_param1[-numdelt]
# print alphas, betas, dist_param, "Alphas, betas, dist_param"


# pdf, cdf = choose_pdfcdf(cdf_case)
# if het_form != None:
#     print deltas, "Deltas"
#     sig = sigma_calc(het_form, len(Y), deltas)
#     std_errors = std_errs(Y, X, alphas, betas, dist_param, pdf, cdf, sig, cdf_case, deltas, method = 'sandwich')
# else: 
#     sig = sigma_calc(het_form, len(Y))
#     std_errors = std_errs(Y, X, alphas, betas, dist_param, pdf, cdf, sig, cdf_case, method = 'sandwich')

# print std_errors, "Standard Errors"


# xb = np.dot(X, betas)
# xb_a = alphas - xb
# rho = getrhoGQR(xb, alphas, [sig, dist_param], cdf_case)
#standard errors(all three types) work for 2+categories for all distributions (not including st)
#assuming homoskedasticity

# #testing out my closed form laplace 
# x_space = np.linspace(-2,2,300)
# sig_test = np.ones(300)
# paravec_st = [sig_test, [0, 100]]
# # phi = phi_sgt(1, 0, 100, 2)
# laplace_points = laplace_cdf(paravec_st, x_space)
# laplace_points_closed = laplace_cdf_closed(paravec_st, x_space)
# plt.plot(x_space, laplace_points, label = 'laplace')
# plt.plot(x_space, laplace_points_closed, 'r--', label = 'laplace closed')
# plt.legend()
# plt.title('Laplace cdf')
# plt.show()
# carla is the best
#yay. The cdf looks nice, but the pdf looks like crap




# # pdf looks crappy, but let's see if optimization works. 
# est_param_st2 = calling_opt(cdf_case, Y, X, het_form)
# output_test = output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000}), method_std = 'invneghess')

#checking my closed form laplace:

#......................................................................................................................................................

#Trying this out with the world values survey data

# data = np.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\worldvalues\worldvalues.csv', delimiter = ',')
# data = np.loadtxt('C:\Users\Daniel and Carla\Documents\carlascode\mcdonaldresearch\ordered_reg_code\shinycode\maincheck.csv', delimiter = ',')

cdf_case = 1
het_form = None
# Y = data[:,0]
# Y = Y.astype(int)
# X = data[:,1:]
k = 10
# o_probit_test = output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 100000, 'maxfev' : 100000}), method_std = 'optgradient')
# print "Done"


#--------------Trying to write a faster log-likelihood by using booleans

#simulating our data
numobs = 10000
# numobs = Y.size
betas = np.array([-0.43313270406393045, 0.14084244102201782, -0.032758825099192194, 0.047816596549008342, \
    -0.00064878078346358931, -0.22890795234735992, -0.54772066133002906, -0.32030451865557652])

Y, X, alphas = simul_data(numobs, betas, k, distr = 'normal')
Y = Y.astype(float)
L = 0
m = len(Y)
try:
    n = X.shape[1]
except:
    n = 1
# alpha = o_probit_test[0]
# alphas = np.array([-2.1119228284548726, -1.4248821427760665, -0.92276018316829278,-0.55410096909478934, \
    # -0.16101733702720122, 0.15968844298477772, 0.55503885733416691, 0.95987911755883415, 1.603153333262755])

# betas = o_probit_test[1]

# paravec_cdf = paravec_ab_cdf[n+k-1:]
paravec_cdf = [1]



#-------------------------------------------First procedure-----------------------------------
# xb = np.dot(X, betas.T)
begin = time.time()


LLV = np.zeros_like(Y).astype(float) 


#trying to create our endpoints matrix
endpoints_two = Y.astype(float)
endpoints_one = Y.astype(float)
alphas_two = np.concatenate((alphas,[0.0]))
alphas_one = np.concatenate(([0.0], alphas))

for i in xrange(numobs):
	endpoints_two[i] = alphas_two[Y[i]]
	endpoints_one[i] = alphas_one[Y[i]]

#making the whole xb array
xb = np.dot(X, betas)
# print "XB", xb

#calculating the log-likelihood for the two end bins and the middle bins
#Calculating the log-likelihood for the first cutoff: 
first_cutoff = Y == 0
z = alphas[0] - xb[first_cutoff]
prob = normal_cdf(paravec_cdf, z)
LLV[first_cutoff] = np.log(prob)

#last cutoff 
last_cutoff = Y == k-1
z = alphas[k-2] - xb[last_cutoff]
prob = 1 - normal_cdf(paravec_cdf, z)
LLV[last_cutoff] = np.log(prob)

z2 = np.zeros_like(Y)
z1 = np.zeros_like(Y)
prob1 = np.zeros_like(Y)
prob2 = np.zeros_like(Y)
#middle bins 
middle_cutoff = (Y != 0) & (Y != k-1)
z2[middle_cutoff] = endpoints_two[middle_cutoff] - xb[middle_cutoff]
z1[middle_cutoff] = endpoints_one[middle_cutoff] - xb[middle_cutoff]
prob2[middle_cutoff], prob1[middle_cutoff] = normal_cdf(paravec_cdf, z2[middle_cutoff]), normal_cdf(paravec_cdf, z1[middle_cutoff])
LLV[middle_cutoff] = np.log(prob2[middle_cutoff] - prob1[middle_cutoff])
LLV_sum = LLV.sum()
end = time.time() - begin
# print LLV, "Log-likelihood"
# print ""
#--------------------------------------------------------------------------------------------

#------------------------------------------------checking it against the original log-likelihood
paravec_ab = np.concatenate((alphas, betas))
begin1 = time.time()
value, LLV_2, zzz, zz, endpointzz = loglike(paravec_ab, paravec_cdf, X, Y, normal_cdf, k, het_form = None, deltas = None, sign = 1)
end1 = time.time() - begin1
# print LLV_2, "Log-likelihood function"
#---------------------------------------------------------------------------------------------
#-----------------------------------------------Making sure our function works----------------
begin2 = time.time()
value2 = llv(paravec_ab, paravec_cdf, X, Y, normal_cdf, k, het_form = None, deltas = None)
end2 = time.time() - begin2
print "Boolean time", end
print "For loop time", end1 
print "Boolean Function", end2
