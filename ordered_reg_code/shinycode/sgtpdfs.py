from __future__ import division
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 

#TODO explain the inputs to the pdfs 

def st_pdf(paravec, y):
    sig = paravec[0]
    lamb, q = paravec[1][0:]
    m = 0
    p = 2

    phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
        *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)
    # phi = 1    
    pdf = np.exp((np.log(p)-np.log(2)-np.log(phi)-1/p*np.log(q)-betaln(q,1/p))+(-(q+1/p)\
                        *np.log(1+abs(y-m)**p/q/(phi**p)/(1+lamb*np.sign(y-m))**p)));
    return pdf

def sged_pdf(paravec, y):
    # print paravec, "paravec"
    sig = paravec[0]
    # print "Paravec[0]", paravec[0]
    m = 0
    # print paravec[1], "paravec in sged"
    
    lamb, p = paravec[1]


    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    # print phi, "Phi sged"
    # print gamma(1/p), "Gamma(1/p)" 

    pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * phi) ** p) \
          / (2 * phi * gamma(1/p))
    return pdf

def slaplace_pdf(paravec, y):    
    m = 0
    sig = paravec[0]
    # print "Sig", sig
    lamb = paravec[1][0]
    # print "Lamb", lamb

    phi = sig * (gamma(1) / (gamma(3) + lamb ** 2 * \
         (3 * gamma(3) - 4 * gamma(2)**2 / gamma(1))))**(1/2)
    # print "Phi", phi
    pdf = np.exp(- abs(y-m) / (( 1 + lamb * np.sign(y-m)) * phi)) \
          / (2 * phi * gamma(1))
    return pdf

def ged_pdf(paravec, y):
    m = 0
    lamb = 0
    sig = paravec[0]
    p = paravec[1][0]

    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)

    pdf = p * np.exp(-abs(y-m) ** p / phi ** p ) \
          / (2 * phi * gamma(1/p))
    return pdf

def snormal_pdf(paravec, y):
    m = 0
    p = 2
    sig = paravec[0]
    lamb = paravec[1][0]

    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) 

    pdf = np.exp(- abs(y-m) ** 2 / (( 1 + lamb * np.sign(y-m)) * phi) ** 2) \
          / (phi * gamma(1/2))
    return pdf

def normal_pdf(paravec, y):
    m = 0    
    lamb = 0

    sig = paravec[0]
    phi = sig * (gamma(1/2) / gamma(3/2)) **(1/2) 
    # print phi, "phi normal"
    pdf = np.exp(- abs(y-m) ** 2 /  phi ** 2) \
          / (phi * gamma(1/2))

    return pdf

def laplace_pdf(paravec, y):
    m, lamb, p = 0, 0, 1
    sig = paravec[0]
    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2)
    pdf = pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * phi) ** p) \
          / (2 * phi * gamma(1/p))
    return pdf

