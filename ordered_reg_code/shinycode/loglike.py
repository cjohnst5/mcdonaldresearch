from __future__ import division
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from sigma_calc import *

def loglike(paravec_ab, thetas, X, Y, cdf, k, het_form, deltas = None, sign = 1):
    
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function 

    Parameters:
    .................................
    paravec_ab:
        Includes all regression parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(k-1), b_0, b1, ... b_(j-1) where a_i are the cutoff values for the
        categorical variables with k categories. b_i are the slope parameters.
        An intercept/constant is not included. 
    thetas: 
        distributional parameters. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers and start at 0)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)
    
    k:
        Number of categories the Y variable has
    het:
        Integer. 0 indicates homoskedasticity, 1 indicates heteroskedasticiy
    deltas:
        if heteroskedasticity is assumed, then these are coefficients on the heteroskedastic x's when finding variance. 
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    L:
        The probability of the random variable vector Y occurring 
    """
    L = 0    
    m = len(Y)
    LLV = np.zeros(m)
    z1 = np.zeros(m)
    z0 = np.zeros(m)
    endpoints_one = np.zeros(m)
    try:
        n = X.shape[1]
    except:
        n = 1
    alpha = paravec_ab[0:k-1]
    betas = paravec_ab[k-1:]

    sigmas = sigma_calc(het_form, m, deltas)
    
    count = 0   
    for j in xrange(k):
        for i in xrange(m):
            cdf_param = [sigmas[i], thetas]            
            if Y[i] == j: 
                count += 1                
                xb = np.dot(X[i], betas)   
                                       
                if j == 0:
                    z = alpha[j] - xb
                    prob = cdf(cdf_param, z)
                    
                elif j == k-1:
                    z = alpha[j-1] - xb
                    prob = 1 - cdf(cdf_param, z)
                    
                else: 
                    z1[i] = alpha[j] - xb
                    z0[i] = alpha[j-1] - xb
                    endpoints_one[i] = alpha[j-1]
                    cdf1 = cdf(cdf_param, z1[i])
                    cdf0 = cdf(cdf_param, z0[i])                     
                    prob = cdf1 - cdf0
                    
                L += sign * np.log(prob)
                LLV[i] = np.log(prob)
    return L, LLV, z1, z0, endpoints_one
