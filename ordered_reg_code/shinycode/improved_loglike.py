#May 28 2014
from __future__ import division
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from sigma_calc import *


def llv(paravec_ab, thetas, X, Y, cdf, k, het_form, deltas = None): #I used to include sign =1
                                                                    #in parameter, but it prevented autoreloading
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function. 

    Parameters:
    .................................
    paravec_ab:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    thetas: 
        distributional parameters. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers and start at 0)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)    
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    LLV_sum:
        The probability of the random variable Y occurring 
    """
    # L = 0
    m = len(Y)
    try:
        n = X.shape[1]
    except:
        n = 1

    #putting things in order    
    alphas = paravec_ab[0:k-1]
    # print alphas, "alphas"
    betas = paravec_ab[k-1:]
    sigmas = sigma_calc(het_form, m, deltas)   
    LLV = np.zeros(m)

    #trying to create our endpoints matrix
    endpoints_two = Y.astype(float)
    endpoints_one = Y.astype(float)
    alphas_two = np.concatenate((alphas,[0.0]))
    alphas_one = np.concatenate(([0.0], alphas))

    for i in xrange(m):
        endpoints_two[i] = alphas_two[Y[i]]
        endpoints_one[i] = alphas_one[Y[i]]

    #making the whole xb array
    xb = np.dot(X, betas)  

    #Calculating the log-likelihood for the first cutoff: 
    first_cutoff = Y == 0
    cdf_param = [sigmas[first_cutoff], thetas]
    z = alphas[0] - xb[first_cutoff]
    prob = cdf(cdf_param, z)
    LLV[first_cutoff] = np.log(prob)

    #last cutoff 
    last_cutoff = Y == k-1
    cdf_param = [sigmas[last_cutoff], thetas]
    z = alphas[k-2] - xb[last_cutoff]
    prob = 1 - cdf(cdf_param, z)
    LLV[last_cutoff] = np.log(prob)

    z2 = np.zeros(m)
    z1 = np.zeros(m)
    prob1 = np.zeros(m)
    prob2 = np.zeros(m)
    #middle bins 
    middle_cutoff = (Y != 0) & (Y != k-1)
    cdf_param = [sigmas[middle_cutoff], thetas]
    z2[middle_cutoff] = endpoints_two[middle_cutoff] - xb[middle_cutoff]
    z1[middle_cutoff] = endpoints_one[middle_cutoff] - xb[middle_cutoff]
    prob2[middle_cutoff], prob1[middle_cutoff] = cdf(cdf_param, z2[middle_cutoff]), cdf(cdf_param, z1[middle_cutoff])
    LLV[middle_cutoff] = np.log(prob2[middle_cutoff] - prob1[middle_cutoff])
    LLV_sum = LLV.sum()
    return -LLV_sum