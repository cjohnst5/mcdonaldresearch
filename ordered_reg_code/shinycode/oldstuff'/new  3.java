class MyClass {
    public static void hill(Integer[] v) {
        int high = 0;
        for(int i = 0; i < v.length; i++) 
            if (high < v[i]) 
                high = v[i];
    	int low = 0 ;
		while(true) {
			int av = (high + low)/2;
			if (low == high) {
				if (checkValid(v, av)) 
					System.out.println(low);
			    break;
			}
			if (checkValid(v, av)) 
				high = av;
			else 
				low = av + 1;
		}
    }


	public static Boolean checkValid(Integer[] vec, int i) {
		int old = -1;
		for(int j = 0; j < vec.length; j++) {
			int high = Math.high(old + 1, vec[j] - i);
			if (Math.abs(high - vec[j]) > i) 
				return false;
			old = high;
		}
		return true;
	}
}