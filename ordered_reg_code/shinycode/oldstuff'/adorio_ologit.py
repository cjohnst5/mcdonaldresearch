"""
file    ologit.py
author  Ernesto Adorio, Ph.D.
        UPDEPP at Clarkfield, Pampanga
email   ernesto.adorio@gmail.com
version 0.0.1- 2012.03.17   initial version
        0.0.2- 2012.03.25   working version without gradient.
references
        Kleinbaum and Klein, pp 314-317, "Logistic Regression",
          A Self-Learning Text, Springer , 2e.
license
        Educational use only. Attribution requested if used in scholarly works.
"""
 
from __future__ import division
from math import exp, log
from scipy.optimize import fmin_bfgs
 
import numpy as np
 
def readcsv(filename, header=True, withintercept=True):
    try:
        data = open(filename, "r").read()
        data = data.replace(',', " ")
        data = data.strip()
        X = []
        Y = []
        colnames = None
        unnamed = True
        for i , line in enumerate(data.split("\n")):
            #print i + 1, line
            fields = line.split()
            if header and unnamed:
                unnamed = False
                colnames = fields
                continue
            Y.append(int(fields[0]))
            x =  [float(f) for f in fields[1:]]
            if withintercept:
                x.insert(0, 1.0)
            X.append(x)
        miny = min(Y)
        if miny != 0:
            for i in range(len(Y)):
                Y[i] -= miny
        return X, Y, colnames
 
    except:
        return None, None, None
 
def itable(Y):
    """
    Returns dictionary of unique elements.
    """
    D={}
    for i, y in enumerate(Y):
        if y in D:
            D[y] += 1
        else:
            D[y] = 1  # changed mar.17, 2012 originally zero!
    return (D)
 
 
def negologlik(Betas, X, Y,  m):
    """
    log likelihood for ordered logistic regression (ologit).
    Betas - estimated coefficients, as a SINGLE array!
    Y values are coded from 0 to ncategories - 1.
 
    Betas - array of coefficients. Think of this with the following layout:
 
       alpha_0, alpha_1,..., alpha_(m-1), Betas[m], Betas[m+1, ..., Betas[m+n-1]
 
       The alpha coefficients are stored in the array with indices
          0, ..., m-1. The reference level always has value of zero.
       Stored in one array! The beta  coefficients for each level
       are stored with indices in range(m , m + n-1)
    X,Y   data X matrix and integer response Y vector with values
            from 0 to maxlevel=ncategories-1. Y vector values must be integers
    m - number of categories in Y vector. each value of  ylevel in Y must be in the
            interval [0, ncategories) or 0 <= ylevel < m
    reflevel - reference level, fixed default code: 0
    """
    L  = 0
    for row, (xrow, ylevel) in enumerate(zip(X,Y)):
        d   = [1.0] * m
        xdotb = sum([(x * b) for (x,b) in zip(xrow[1:], Betas[m:])])
        pgk = [1.0] * m
 
        for k in range(1, m):
            v = Betas[k] + xdotb # alpha + x dot Betas
            try:
                if v < -30:
                    v = -30
                    pgk[k] =1.0 /(1 + exp(-v))
            except:
                print " v=", v
 
        #ignore the reference level (it has probability of 1!)
        if 0 <= ylevel < m-1 :
           prob = pgk[ylevel] - pgk[ylevel+1]
        elif ylevel == m-1:
           prob = pgk[ylevel]
        #print "P(Y==", ylevel,"\")", prob  ,
        #Those numerical inaccuracies and domain errors!
        if prob > 1.0:
            prob = 1.0
        elif prob <= 0:
            prob = 1.0e-8
        L += log(prob)
    #print "loglik=", -2*L
    return -2*L
 
 
def estmlogit(Betas, X,Y,  m=None, algo="bfgs", maxiter=50, epsilon = 1.0e-8, full_output=True, disp=False):
    """
    Returns estimates of the unknown parameters of the multivalued logistic regression
        initial estimates, the response Y vector and predictors X matrix. X must have
    a column of all 1s if a constant is in the model!
    The estimates are determined by fmin_bfgs.
    """
 
    if m is None:
       m = len(itable(Y))
 
    def floglik(betas):
        return negologlik(betas, X, Y, m = m)
 
    if algo=="bfgs":
       output = fmin_bfgs(floglik , Betas, fprime=None, maxiter=maxiter, epsilon = epsilon, full_output = True, retall=False, disp=False)
    return output
 
 
 
 
 
# if __name__=="__main__":
#     X,Y, colnames = readcsv("ologit_data.csv")
#     print "colnames=", colnames
#     ydict          = itable(Y)
#     for key in ydict: print key, ydict[key]
#     m              = len(ydict)
#     n              = len(X[0])
#     # raw_input("press ENTER KEY to continue:")
#     Betas = [1] * (m+n)
 
    """
R results:
       Coef  S.E.   Wald Z P
y>=1   -2.20332 0.7795 -2.83  0.0047
y>=2   -4.29877 0.8043 -5.34  0.0000
pared   1.04766 0.2658  3.94  0.0001
public -0.05868 0.2979 -0.20  0.8438
gpa  0.61575 0.2606  2.36  0.0182
"""
    # Betas =[ 1.0, -2.20332, -4.29877, 1.04766, -0.05868, 0.61575]
    # print "negologlik() function value with optimal values:", negologlik(Betas, X,Y, m), Betas
    # Betas = np.array([0.0] * (m +n))
    # print "negologlik() function value with  zero  values:", negologlik(Betas, X,Y, m), Betas
 
    # print "Running bfgs algorithm on negologlik function:"
    # output = estmlogit(Betas, X,Y,  m=None, algo="bfgs", maxiter=10,epsilon = 1.0e-5, full_output=True, disp=False)
    # output = estmlogit(output[0], X,Y,  m=None, algo="bfgs", maxiter=30,epsilon = 1.0e-8, full_output=True, disp=False)
    # for field in output:
    #     print field
