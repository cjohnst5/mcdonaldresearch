from __future__ import division
import scipy as sp
import numpy as np
import scipy.linalg as la
import carla_oprobit as co
import carla_oged as cged
# import ged

from scipy.stats import skew, kurtosis, norm, laplace
import matplotlib.pyplot as plt

# import sys
# sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\mle')
# from scipy.optimize import fmin_bfgs, minimize
# from normal_MLE import normal_MLE
# from laplace_MLE import laplace_MLE
# from slaplace_MLE import slaplace_MLE
# from ged_MLE import ged_MLE
# from snormal_MLE import snormal_MLE
# from t_MLE import t_MLE
# from sged_MLE import sged_MLE
# from gt_MLE import gt_MLE
# from st_MLE import st_MLE
# from sgt_MLE import sgt_MLE

# sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
# from sgttree_cdfs import *
# from sgttree_pdfs import *
#
# sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\graph_func')
# from graph_func import single_graph

X = np.random.rand(500)
Y = np.random.randint(0, 3, 500)
data = np.vstack((X, Y)).T
np.savetxt('practice_data.csv', data, delimiter = ",")
paravec_ab = [.2863603, 2.178006, 2.04]


data = sp.loadtxt('worldvalues.csv',skiprows=1,delimiter=',')
obs = 100
het_form = None

Y = data[0:obs,0]
Y = Y.astype(int)
X = data[0:obs,1]
X = X.reshape((obs,1))

k = 10
startbetas = la.lstsq(X, Y)[0]
alphas = []
#getting alpha estimates using predicted y's from OLS.
unique_cat = set(Y)
numcat = len(unique_cat)
y_pred = np.dot(X, startbetas)
max_min = y_pred.max() - y_pred.min()

cat = range(numcat)
for i in xrange(1, numcat):
   alphas.append(max_min * (i/numcat) + y_pred.min())
startalphas = np.asarray(alphas)

#using some OLS and divying to start us off
start_ab = np.zeros(len(startalphas) + len(startbetas))
start_ab[0:len(startalphas)] = startalphas
start_ab[len(startalphas):] = startbetas

guess = start_ab

#assuming a standard normal distribution, which is odd to me. 
# k=3
# sign = -1
# guess = [.2, 5, 5]


#my function for estimation oprobit parameters, which works now
est_normal = co.estm_ordered(X, Y, guess, k)

print est_normal

#Powell method sucks. Stick to Nelder-Mead. Possible exploration: Use BFGS and gradients.
#yesss. This is exactly what STATA gives me. I'm not sure why I get runtime errors. Hmm..

"""
#Now I'm going to try out my GED method. What will be tricky is getting intial guesses for it. 
ged_opts = ({'maxiter': 5000, 'maxfev' : 5000})
guess_GED = [-.01, .22, .08, 0, .4, 8]
est_GED = cged.estm_ged(X, Y, ged_cdf, guess_GED, k, method = 'Nelder-Mead', options = ged_opts)

#plotting the pdf that I get
x_range = np.linspace(-1,1,500)
ged_pdfpoints = ged_pdf([est_GED.x[-3], est_GED.x[-2], 7], x_range)
ged_cdfpoints = ged_cdf(est_GED.x[-3:], x_range)

plt.subplot(2,1,1)
plt.plot(x_range, ged_pdfpoints)
plt.title('Estimated GED pdf')
plt.subplot(2,1,2)
plt.plot(x_range, ged_cdfpoints)
plt.title('Estimated GED cdf')

#GED results are really strange. The pdf that I get back goes way about 1 and isn't smooth. 

#I'm setting m = 0, s=1, in teh GED cdf, so that only p needs to estimated. So it's sorta
#standard GED....
guess2_GED = [-.01, .22, .08, 2]
est2_GED = cged.estm_ged(X, Y, ged.carla_ged_cdf, guess2_GED, k)

#plotting this new distribution. 
ged2_pdfpoints = ged_pdf([0, 1, est2_GED.x[-1]], x_range)
ged2_cdfpoints = ged_cdf([0, 1, est2_GED.x[-1]], x_range)
plt.subplot(2, 1, 1)
plt.plot(x_range, ged2_pdfpoints)
plt.subplot(2, 1, 2)
plt.plot(x_range, ged2_cdfpoints)


#so my pdf is below 1, at least, but both the cdf and pdf look ridiculous. 

#assuming a snormal distribution and seeing  how that goes
guess_snormal = [-.244, .53, .06, 0, 1, 10]
est_snormal = cged.estm_ged(X, Y, snormal_cdf, guess_snormal, k, method = 'Powell')

#plotting snormal results
snormal_pdfpoints = snormal_pdf(est_snormal.x[-3:], x_range)
snormal_cdfpoints = snormal_cdf(est_snormal.x[-3:], x_range)
plt.subplot(2, 1, 1)
plt.plot(x_range, snormal_pdfpoints)
plt.subplot(2, 1, 2)
plt.plot(x_range, snormal_cdfpoints)
plt.show() """