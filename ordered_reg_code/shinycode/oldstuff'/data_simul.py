#data_simul.py
#Last updated July 17, 2013

#Using data with normally distributed errors to try and get my GED to work. 

from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import carla_oprobit as co
import carla_oged as cged
# import ged

import sys
# sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
# from sgttree_cdfs import *
# from sgttree_pdfs import *


x = sp.random.normal(size = 500)
err = sp.random.normal(size = 500)
y_star = .5 * x + err
y_obs = sp.ones_like(y_star)*50

for i in xrange(len(y_star)):
    if y_star[i] <=-1:
        y_obs[i] = 1
    elif -1<y_star[i]<=1:
        y_obs[i] = 2
    elif y_star[i]>1:
        y_obs[i] = 3

guess_probit = [-3, 3, 1]
est_param = co.estm_ordered(x, y_obs, guess_probit)


k = 3
guess_GED = [-7, -1, .5, 0, 1, 2]
est_ged =  cged.estm_ged(x, y_obs, ged_cdf, guess_GED, k)

#plotting the GED to see what it looks like
x_range = np.linspace(-1,1,500)
ged_pdfpoints = ged_pdf(est_GED.x[-3:], x_range)
ged_cdfpoints = ged_cdf(est_GED.x[-3:], x_range)

plt.subplot(2,1,1)
plt.plot(x_range, ged_pdfpoints)
plt.title('Estimated GED pdf')
plt.subplot(2,1,2)
plt.plot(x_range, ged_cdfpoints)
plt.title('Estimated GED cdf')

#So the GED still looks like crap