from __future__ import division
import adorio_ologit as ao
import scipy as sp
import numpy as np
from scipy.optimize import fmin_bfgs, minimize
from scipy.stats import norm

# import sys
# sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
# from sgttree_cdfs import *


#I'm going to try and code by own ordered probit. The one from online doesn't work

# Note: I didn't worry about an intercept. Steward 2004 does not use one and neither does the STATA
#procedure. 

def loglike_ged(paravec_ab_cdf, X, Y, cdf, k, sign = 1):
    
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function (TODO: allow for different cdfs)

    Parameters:
    .................................
    paravec_ab_cdf:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)
    paravec_cdf:
        parameters for the specified cdf. These are usually found using MLE 
        fitting of the pdf to the data. See sgttree/mle folder for MLE functions
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    L:
        The probability of the random variable Y occurring 
    """
    L = 0
    m = len(Y)
    try:
        n = X.shape[1]
    except:
        n = 1
    alpha = paravec_ab_cdf[0:k-1]
    betas = paravec_ab_cdf[k-1:n+k-1]
    paravec_cdf = paravec_ab_cdf[n+k-1:]
    
    count = 0
    for j in xrange(k):
        for i in xrange(m):            
            if Y[i] == j: 
                # print '\n x', X[i]
                xb = np.dot(X[i].T, betas)    
                # print  'xb', xb
                count += 1             
                if j == 0:
                    z = alpha[j] - xb
                    prob = cdf(paravec_cdf,z)
                    # print z, "alpha - xb"
                    # print prob, "prob when j = 0"
                elif j == k-1:
                    z = alpha[j-1] - xb
                    prob = 1 - cdf(paravec_cdf,z)
                    # print z, "alpha- xb"
                    # print prob, "prob when j=k-1"
                else: 
                    z1 = alpha[j] - xb
                    z0 = alpha[j-1] - xb
                    cdf1 = cdf(paravec_cdf,z1)
                    cdf0 = cdf(paravec_cdf, z0)                     
                    prob = cdf1 - cdf0
                    # print z1, z0, "z1, z0"
                    # print cdf1, "J"
                    # print cdf0, "J-1"
                    # print prob, "prob when 0<j<k-1"
                L += sign * np.log(prob)
    return L


def estm_ged(X, Y, cdf, guess, k = None, method = 'Nelder-Mead', options = ({'maxiter': 5000, 'maxfev' : 5000})
):
    
    """
    Using MLE, estimates the alpha, betas and distributional parameters for 
    and ordered regression model. Right now paravec_cdf is included just so we
    can specify a standard normal and see how that goes. 
    
    Parameters:
    ------------------------
    Look at loglike_ordered docstring for parameters (X, Y, cdf, paravec_cdf, k)
   

    guess:
        Initial guess for all the parameters. 
    method:
        Optimization method to use with the minimize command. Default is 'Nelder-Mead'

    Returns:
    ------------------------
    result = contains alphas, betas and distributional parameters. Seems like a lot of 
    things to estimate.
    """
    if k is None:
        k = len(ao.itable(Y))
    cri_ordered = lambda x: loglike_ged(x, X, Y, cdf, k, sign = -1)

    results = minimize(cri_ordered, guess, method = method, options = options)

    return results