from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.optimize import fmin_bfgs, minimize

#cdfs of the SGT trees. Includes SGED, Snormal, GED, Slaplace, Laplace, Normal cdfs
#assuming the mean = 0 on all distributions for identification purposes. 
 

def normal_cdf(paravec, y):
    # s = paravec[0]
    # cdf = 1/2 * (1 + erf((y - 0) / (s * 2**(1/2))))
    # cdf = norm.cdf(y, 0, s)

    sig = paravec[0]
    lamb = 0
    #our two scale parameters and sigma accounting for heteroskedasticity
    m = 0
    phi = sig * (gamma(1/2)/ (gamma(3/2) + lamb**2 * (3 * gamma(3/2) - 4 / gamma(1/2)))) ** (1/2)

    z = abs(y-m) ** 2 / (phi ** 2 * (1 + lamb * np.sign(y-m)) ** 2 )
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf 

def ged_cdf(paravec, y):
    sig = paravec[0]
    p = paravec[1][0]    
    m = 0    
    phi = sig * (gamma(1/p) / gamma(3/p))**(1/2)
  
    z = abs(y-m) ** p / (phi ** p)
    cdf = (1) / 2 + ((1) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def snormal_cdf(paravec, y):
    sig = paravec[0]
    lamb = paravec[1][0]
    #our two scale parameters and sigma accounting for heteroskedasticity
    m = 0 
    phi = sig * (gamma(1/2)/ (gamma(3/2) + lamb**2 * (3 * gamma(3/2) - 4 / gamma(1/2)))) ** (1/2)
    
    z = abs(y-m) ** 2 / (phi ** 2 * (1 + lamb * np.sign(y-m)) ** 2 )
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf


def laplace_cdf(paravec, y):
    "this is the closed form laplace, which should speed up the code"
    sig = paravec[0]
    m, lamb, p = 0, 0, 1
    phi = sig/2**(1/2)
    cdf = 1/2 + 1/2 * np.sign(y - m) * (1 - np.exp(-abs(y - m)/phi))
    return cdf 

def slaplace_cdf(paravec, y):
    sig = paravec[0]
    lamb = paravec[1][0]
    deltas = paravec[1:]    
    
    m = 0
    phi = sig / (gamma(3) + lamb ** 2 * \
         (3 * gamma(3) - 4 * gamma(2)**2))**(1/2)  

    z = abs(y-m) / (phi * (1 + lamb * np.sign(y-m)))
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1, z)
    return cdf

def sged_cdf(paravec, y):
    sig = paravec[0]
    lamb, p = paravec[1][0:]
    #our two scale parameters  
    m = 0     
    phi = sig * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) 
    
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf

def st_cdf(paravec, y):
    sig = paravec[0]
    # lamb, lq = paravec[1][0:]
    # q = np.exp(lq)

    lamb, q = paravec[1][0:]
    m, p = 0, 2

    phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
        *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)

    z = abs(y-m) ** p / (abs(y-m)**p + q * phi**p * (1 + lamb * np.sign(y-m))**p)
    cdf = (1-lamb)/ 2 + ((1 + lamb * np.sign(y-m)) / 2) * np.sign(y-m) * betainc(1/p, q, z)
    return cdf 

def phi_sgt(sig, lamb, q, p):
    
    phi = (sig / q**(1/p)) * (beta(1/p, q) / (beta(3/p, q-2/p) + lamb ** 2\
        *(3 * beta(3/p, q-2/p) - 4 * beta(2/p, q-1/p)**2 / beta(1/p, q)))) **(1/2)
    return phi