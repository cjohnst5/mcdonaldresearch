from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.optimize import fmin_bfgs, minimize

def sigma_calc(het_form, obs, deltas = None):
    """
    Returns either a column of ones or the heteroskedasticity for each observation, 
    given the provided x-variables.  

    Parameters: 
    ------------------------------------------------------------------
    het_form: 
        numpy array or vector of independent variables that are assumed to 
        affect variance or None if homoskedasticity is assumed. 
    obs: 
        number of observations. Scalar
    deltas: 
        Numpy vector or python list. effects of x on heteroskedasticity
    """

    if het_form == None:
        sig = np.ones(obs)
    else: 
        
        sig = np.exp(np.dot(het_form, deltas))
    return sig 