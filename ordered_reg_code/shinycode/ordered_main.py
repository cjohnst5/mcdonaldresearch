from __future__ import division
import scipy as sp
import numpy as np
import sgtcdfs as sgtc
import sgtpdfs as sgtp
# import opt_main as om
import opt_main2 as om
from std_errs1 import *
# from loglike import loglike
from improved_loglike import llv
from sigma_calc import sigma_calc
import scipy.linalg as la
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.optimize import fmin_bfgs, minimize


def int_guess_calc(Y, X, cdf_case, het_form = None):
    """
    Comes up with initial guesses for alphas, betas, thetas and deltas (if there is hetero) to put into our 
    optimization function. 
    Alphas are the cutoffs for our categorical variable, betas are the slope coefficients, thetas
    are the distributional parameters if there is heteroskedasticity, deltas are the heteroskedastic 
    x's effects on variance. 

    Parameters:
    ----------------------------------
    Y: numpy vector of categorical variable (must start at 0)
    X: numpy array or vector
    cdf_case: 
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST. This is still a work in progress 
    het_form:
        Either None or the X array which contains the X variables suspected of causing hetero 
    Returns:
    ----------------------------------
    start_values:
        our initial guess for the alphas, betas, thetas and deltas(if heteroskedasticity is assumed)
    ll_homo:
        If heteroskedasticity is incorporated, this is the loglikelihood value
        from estimating the model using the specified distribution, assuming homoskedasticity. 
    """
    

    startbetas = la.lstsq(X, Y)[0]
    alphas = []
    #getting alpha estimates using predicted y's from OLS. 
    unique_cat = set(Y)
    numcat = len(unique_cat)
    y_pred = np.dot(X, startbetas)
    max_min = y_pred.max() - y_pred.min() 
    
    cat = range(numcat)
    for i in xrange(1, numcat):
       alphas.append(max_min * (i/numcat) + y_pred.min())
    startalphas = np.asarray(alphas)
     
    #using some OLS and divying to start us off
    start_ab = np.zeros(len(startalphas) + len(startbetas))
    start_ab[0:len(startalphas)] = startalphas
    start_ab[len(startalphas):] = startbetas

    numab = len(start_ab)
    if het_form != None:
        numdelts = het_form.shape[1]
        start_delts = np.zeros(numdelts)     


    if cdf_case == 1:
        if het_form == None:
            start_values = start_ab
        elif het_form != None:
            results = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 2:
        start_values = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab).x
        if het_form != None:
            results = om.opt_main2(2, sgtc.laplace_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 3: 
        start_ab = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [0]))
        if het_form != None:
            results = om.opt_main2(3, sgtc.snormal_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun 
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 4:        
        start_ab = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [2]))
        if het_form != None:
            results = om.opt_main2(4, sgtc.ged_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x,-results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 5:
        start_ab = om.opt_main2(1, sgtc.laplace_cdf, None, Y, X, start_ab).x
        start_values = np.concatenate((start_ab, [0]))
        if het_form != None:
            results = om.opt_main2(5, sgtc.slaplace_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    elif cdf_case == 6:
        # Normal Route................
        # start_ab = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab).x
        # start_values = np.concatenate((start_ab, [0,2]))
        # Laplace Route...............
        start_ab = om.opt_main2(1, sgtc.laplace_cdf, None, Y, X, start_ab).x
        start_values_slaplace = np.concatenate((start_ab, [0]))
        start_values = om.opt_main2(5, sgtc.slaplace_cdf, None, Y, X, start_values_slaplace).x
        start_values = np.concatenate((start_values, [1]))
        if het_form != None:
            results = om.opt_main2(6, sgtc.sged_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun 
            start_values = np.concatenate((start_ab, start_delts))
    
    elif cdf_case == 7: 
        start_ab = om.opt_main2(1, sgtc.normal_cdf, None, Y, X, start_ab).x        
        # start_values = np.concatenate((start_ab, [0, np.log(100)]))
        start_values = np.concatenate((start_ab, [0, 100]))
        if het_form != None:
            results = om.opt_main2(7, sgtc.st_cdf, None, Y, X, start_values)
            start_ab, ll_func = results.x, -results.fun
            start_values = np.concatenate((start_ab, start_delts))

    if het_form == None:
        ll_func = []
    
    return start_values, ll_func


def restricted_calc(Y, X, start_values, method = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000})):
    """
    Calculating the restricted log-likelihood. Basically, we estimate where the cutoffs should be on
    the categorical variable and then compute the loglikelihood of that model (no x's, just bins for y)

    Returns the loglikelihood of the model sans x's. 
    """
    k = len(set(Y))
    n = X.shape[1]
    start_betas = np.zeros(n)
    ols_betas = la.lstsq(X, Y)[0]
    alphas = []

    #getting alpha estimates using predicted y's from OLS.     
    y_pred = np.dot(X, ols_betas)
    max_min = y_pred.max() - y_pred.min()
    cat = range(k)
    for i in xrange(1, k):
       alphas.append(max_min * (i/k) + y_pred.min())
    start_alphas = np.asarray(alphas)

    loglike_obj = lambda x: llv(np.concatenate((x, start_betas)), [0], X, Y, sgtc.normal_cdf, k, None)
    est_param = minimize(loglike_obj, start_alphas, method = method, options = options)
    print "est parama function eval: ", est_param.fun
    return -est_param.fun


def choose_pdfcdf(cdf_case):
    """
    Returns the appropriate cdf and pdf functions given the chosen distribution

    """
    if cdf_case == 1:
        cdf = sgtc.normal_cdf
        pdf = sgtp.normal_pdf
    elif cdf_case == 2:
        cdf = sgtc.laplace_cdf
        pdf = sgtp.laplace_pdf
    elif cdf_case == 3:
        cdf = sgtc.snormal_cdf
        pdf = sgtp.snormal_pdf
    elif cdf_case == 4:
        cdf = sgtc.ged_cdf
        pdf = sgtp.ged_pdf
    elif cdf_case == 5:
        cdf = sgtc.slaplace_cdf
        pdf = sgtp.slaplace_pdf
    elif cdf_case == 6:
        cdf = sgtc.sged_cdf
        pdf = sgtp.sged_pdf
    elif cdf_case == 7:
        cdf = sgtc.st_cdf
        pdf = sgtp.st_pdf
    return pdf, cdf


def calling_opt(cdf_case, Y, X, het_form, method = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000})):
    
    """
    Calls the opt_main2 procedure to obtain betas, alphas, thetas and deltas
    Also returns the start values, loglikelihood values, the pdf and cdf that we pick
    up on the way.  
    """
    start_values, ll_homo = int_guess_calc(Y, X, cdf_case, het_form)
    pdf, cdf = choose_pdfcdf(cdf_case)
    est_param = om.opt_main2(cdf_case, cdf, het_form, Y, X, start_values, method, options)
    return start_values, ll_homo, pdf, cdf, est_param 


def output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000}), method_std = 'optgradient'):
    
    start_values, ll_homo, pdf, cdf, est_param = calling_opt(cdf_case, Y, X, het_form, method_opt, options)
    
    """
    Takes the results of calling opt and organizes them in an easy to read fashion
    Also calls the restricted loglikelihood function. 
    """
    if est_param.success == 1:
        print est_param, "Parameters in their raw form"
        ll_restricted = restricted_calc(Y, X, start_values, method_opt, options)
        ll_unrestricted = -1*est_param.fun
        lr_test = 2*(ll_unrestricted - ll_restricted)
        parameters = est_param.x         
        numalphas = len(set(Y)) - 1
        numbetas = X.shape[1]
        

        alphas = parameters[0:numalphas]
        betas = parameters[numalphas:(numalphas + numbetas)]
        

        if het_form != None:
            numhetx = het_form.shape[1]
            thetas = parameters[(numalphas + numbetas):-numhetx]
            deltas = parameters[-numhetx:]
            print deltas, deltas
            print het_form.shape, len(Y), "Hetform shape and len(y)"
            sigma = sigma_calc(het_form, len(Y), deltas)
            # print sigma.shape, "Sigma shape"
        elif het_form == None:
            thetas = parameters[(numalphas + numbetas):]
            sigma = sigma_calc(het_form, len(Y))
            deltas = None
        errors = std_errs(Y, X, alphas, betas, thetas, pdf, cdf, sigma, cdf_case, deltas,  method_std)
        alphas_steds = errors[0:numalphas]
        betas_steds = errors[numalphas:]

        if cdf_case == 1: print "Probit"
        if cdf_case == 2: print "LAD"
        if cdf_case == 3: print "Snormal"
        if cdf_case == 4: print "GED"
        if cdf_case == 5: print "Slaplace"
        if cdf_case == 6: print "SGED"
        if cdf_case == 7: print "ST"
        

        print "Estimated Betas and Standard errors"        
        print ["Betas", list(betas)] 
        print ["Std Errs", list(betas_steds)], '\n'
        

        print "Estimated Cutoffs"
        print ["Alphas", list(alphas)]
        print ["Std Errs", list(alphas_steds)], '\n'
        

        if cdf_case >2:
            print "Distributional Parameters"
            print thetas, '\n'

        print "Restricted Log-Likelihood:"
        print  ll_restricted
        print "Unrestricted Log-Likelihood"
        print ll_unrestricted
        print "LR Test: Restricted vs. Unrestricted"
        print lr_test, '\n'

        if het_form !=None:
            print "Heteroskedasticity Parameters"
            print deltas, '\n'

            print "H0 = Homoskedasticity, Log-likelihood"
            print ll_homo
            print "LR test: homo vs heteroskedasticity"
            print 2*(ll_unrestricted - ll_homo)
        return alphas, betas, thetas, betas_steds, alphas_steds, ll_restricted, ll_unrestricted, lr_test, est_param
    elif est_param.success == 0:
        print "Oh dear. Try something new, like changing the number of iterations or the estimation method"

    else:
        print "Something else is wrong"






# def running_the_show(cdf_case, Y, X, het_form, method = 'Nelder-Mead',options = ({'maxiter': 20000, 'maxfev' : 20000})):
#     restricted_ll = 
#     stuff = calling_opt(cdf_case, Y, X, het_form, method, options)


    


# keep if !missing(satisfaction) | !missing(trust) | !missing(freedom) | !missing(religious) | !missing(male) | !missing(income) | !missing(yearsold) | !missing(yearsold2) | !missing(married) | ///
# !missing(div_sep_wid) | !missing(fulltime) | !missing(partime) | !missing(retired) | !missing(housewife_student) | !missing(unemployed) | !missing(primary) | !missing(inc_sec_tech) | ///
# !missing(sec_tech) | !missing(inc_sec_uni) | !missing(sec_uni) | !missing(lower_ter) | !missing(upper_ter) | !missing(wave2) | !missing(wave3) 