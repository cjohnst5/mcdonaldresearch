# This differs from optmain because it switches to a Powell method if we max out on function
# iterations. Kerk Phillips suggested this as a faster way to optimize.

from __future__ import division
import scipy as sp
import numpy as np
from sgtcdfs import *
import sigma_calc as sc 
# from loglike import *
from improved_loglike import *
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.optimize import fmin_bfgs, minimize

def opt_main2(cdf_case, cdf, het_form, Y, X, start_values, method = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000})):
    """
    Main optimization function. 

    Parameters
    -----------------------------------
    cdf_case: 
        1: Normal
        2: Laplace
        3: Snormal
        4: GED
        5: Slaplace
        6: SGED
        7: ST, but this is still a work in progress
    het_form:
        If homoskedasticity is assumed, then het_form should be None. 
        If heteroskedasticity is assumed: 
        Numpy array or vector which holds the x's that are suspected to cause
        heteroskedasticity.  
    Returns
    ----------------------------------
    The unknown alphas and betas and the distributional parameters, along with heteroskedasticity
    effects if heteroskedasticity is specified. 

    """ 
    #calculating my sigma:    
    k = len(set(Y))
    numabs = k - 1 + X.shape[1]
    if het_form != None:
        numdelts = het_form.shape[1]  

    if cdf_case <= 2: #normal and laplace don't have distributional parameters
                        #so we have to set up their criteria a little bit differently. 
        if het_form == None:             
            loglike_obj = lambda x: llv(x, [0], X, Y, cdf, k, het_form, deltas = None)
        else:
            loglike_obj = lambda x: llv(x[0:numabs], [0], X, Y, cdf, k, het_form, x[-numdelts:])
    else: #we can treat the rest of these distributions the same way 
        if het_form == None:                        
            loglike_obj = lambda x: llv(x[0:numabs], x[numabs:], X, Y, cdf, k, het_form, deltas = None)
        else:            
            loglike_obj = lambda x: llv(x[0:numabs], x[numabs:-numdelts], X, Y, cdf, k, het_form, x[-numdelts:])
    
    #It looks like if we were to speed up optimization, we would do it here. 

    options = ({'maxfev' : 100000, 'maxiter' : 100000, 'xtol' : 1e-8}) #Changin tolerance and iterations to see if that helps.
    est_param = minimize(loglike_obj, start_values, method = 'Nelder-Mead', options = options)
   

    if est_param.success == 0:
        options = ({'maxfev' : 100000})
        start_values = est_param.x
        est_param = minimize(loglike_obj, est_param, method = 'Powell', options = options)

    return est_param








