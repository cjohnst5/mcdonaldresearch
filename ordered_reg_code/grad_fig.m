


%trying to figure out how to get the gradient so I can calculate the standard errors of the betas
a = importdata('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\simul_normal_het.csv', ',');
a = importdata('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\HOROWTZ5.csv', ',');

Y = a(:, 1);
X = a(:, 2:end);
[n,k] = size(X);
% betas = [-.600766, 1.225191, .032368, .005346, .957014]; %probit
betas = [-.698258, 1.209430, .024493, .004452, .837430]; %GED 
onevec = ones([n,1]);

X = horzcat(onevec,X);
[n,k] = size(X);
xb = X * betas';
PDFeval = normpdf(xb);
CDFeval = normcdf(xb);
s =  ones([n,1]);
rho = (1./s.^2).*(xb);
Y = logical(Y);
Ygrad = zeros(n,1);
Ygrad(Y) = PDFeval(Y)./CDFeval(Y);
Ygrad(~Y) = -PDFeval(~Y)./(1-CDFeval(~Y));
Ygrad(isnan(Ygrad)) = 0;
Ygrad(isinf(Ygrad)) = 0;
gradrep = repmat(Ygrad,1,k).*X;
grad = gradrep'*gradrep;

Yvec = zeros(n,1);
Yvec(Y) = -rho(Y).*PDFeval(Y)./CDFeval(Y)-(PDFeval(Y)./CDFeval(Y)).^2;
Yvec(~Y) = rho(~Y).*PDFeval(~Y)./(1-CDFeval(~Y))-(PDFeval(~Y)./(1-CDFeval(~Y))).^2;
Yvec(isnan(Yvec)) = 0;
Yvec(isinf(Yvec)) = 0;
hess = X'*(X.*repmat(Yvec,1,k));



% V = inv(grad); %outer product of gradient matrix
V = inv(-hess);


rstderrors = sqrt(diag(V));