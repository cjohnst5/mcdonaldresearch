from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm
from scipy.optimize import fmin_bfgs, minimize

#these loglike and estm_ordered function are very similar to the functions found in carla_ordered
#The difference is that the loglike is now coded so the cdf used in it intakes independent
#variables to calculate the variance/sigma. 

def normal_cdf(paravec, y):
    m, s = paravec
    cdf = 1/2 * (1 + erf((y - m) / (s * 2**(1/2))))
    return cdf 

#ok. so this give me back the normal cdf. Now to put heteroskedasticity in it. 

def normal_cdf_het(paravec, x, y):
    """
    allows for heteroskedasticy (sigma is a function of the independent variables)

    Parameters:
    -----------------------
    paravec: 
        contains the estimated deltas for each x's affect on the standard deviation, sigma
    x:
        Numpy array. Independent variables.
    y:
        Numpy vector of random variable with the normal variable. 
    """
    m = 0   
    sigma = np.exp(np.dot(x, paravec))
    # sigma = np.sqrt(1+ np.dot(x**2, paravec))
    
    cdf = 1/2 * (1 + erf((y - m) / (sigma * 2 ** (1/2))))
    return cdf
    #so it looks like stata still assumes m = 0
    #this gives me the same results as STATA! Yay. 

def ged_cdf_het(paravec, x, y):
    p = paravec[0]    
    deltas = paravec[1:]

    # p = 2
    # deltas = paravec

    #our two scale parameters and a sigma accounting for hetero
    m = 0
    sig = np.exp(np.dot(x, deltas))
    phi = sig * (gamma(1/p) / gamma(3/p))**(1/2)
  
    z = abs(y-m) ** p / (phi ** p)
    cdf = (1) / 2 + ((1) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def snormal_cdf_het(paravec, x, y):
    lamb = paravec[0]
    deltas = paravec[1:]


    #our two scale parameters and sigma accounting for heteroskedasticity
    m = 0 
    sig = np.exp(np.dot(x, deltas))

    phi = sig * (gamma(1/2)/ (gamma(3/2) + lamb**2 * (3 * gamma(3/2) - 4 / gamma(1/2)))) ** (1/2)
    
    z = abs(y-m) ** 2 / (phi ** 2 * (1 + lamb * np.sign(y-m)) ** 2 )
    cdf = cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf

def laplace_cdf_het(paravec, x, y):
    #our phi is calculated whe sigma^2 = 1. 
    m, lamb, p = 0, 0, 1 
    deltas = paravec

    #allowing for heteroskedasticity
    sig = np.exp(np.dot(x, deltas))

    #our scale variable
    phi = sig /2**(1/2) 
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf


def slaplace_cdf_het(paravec, x, y):
    lamb = paravec[0]
    deltas = paravec[1:]    
    
    m = 0
    sigma = np.exp(np.dot(x, deltas))
    phi = sigma / (gamma(3) + lamb ** 2 * \
         (3 * gamma(3) - 4 * gamma(2)**2))**(1/2)  

    z = abs(y-m) / (phi * (1 + lamb * np.sign(y-m)))
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1, z)
    return cdf

def sged_cdf_het(paravec, x, y):
    lamb, p = paravec[0:2] 
    deltas = paravec[2:]

    #our two scale parameters  
    m = 0     
    sigma = np.exp(np.dot(x, deltas)) 
    phi = sigma * (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) #we get this from calculating variance
                                                                    #of the SGED and solving for phi. 
    
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf


def loglike(paravec_ab_cdf, X, Y, cdf, k, sign = 1):
    
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    given cumulative distribution function 

    Parameters:
    .................................
    paravec_ab_cdf:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers and start at 0)
    cdf:
        Python function: cdf(paravec, data)
        Specified cdf we assume for our data. (Eg. normal, logistic, skewed t...)
    paravec_cdf:
        parameters for the specified cdf. These are usually found using MLE 
        fitting of the pdf to the data. See sgttree/mle folder for MLE functions
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1

    Returns:
    ------------------------------------------
    L:
        The probability of the random variable Y occurring 
    """
    L = 0
    m = len(Y)
    try:
        n = X.shape[1]
    except:
        n = 1
    alpha = paravec_ab_cdf[0:k-1]
    betas = paravec_ab_cdf[k-1:n+k-1]
    paravec_cdf = paravec_ab_cdf[n+k-1:]
    
    count = 0   
    for j in xrange(k):
        for i in xrange(m):            
            if Y[i] == j: 
                count += 1
                # print '\n x', X[i]
                xb = np.dot(X[i], betas)    
                # print  'xb', xb                            
                if j == 0:
                    z = alpha[j] - xb
                    prob = cdf(paravec_cdf, X[i], z)
                    # print z, "alpha - xb"
                    # print prob, "prob when j = 0"
                elif j == k-1:
                    z = alpha[j-1] - xb
                    prob = 1 - cdf(paravec_cdf, X[i], z)
                    # print z, "alpha- xb"
                    # print prob, "prob when j=k-1"
                else: 
                    z1 = alpha[j] - xb
                    z0 = alpha[j-1] - xb
                    cdf1 = cdf(paravec_cdf, X[i] ,z1)
                    cdf0 = cdf(paravec_cdf, X[i], z0)                     
                    prob = cdf1 - cdf0
                    # print z1, z0, "z1, z0"
                    # print cdf1, "J"
                    # print cdf0, "J-1"
                    # print prob, "prob when 0<j<k-1"
                L += sign * np.log(prob)
    return L


def estm_ordered(X, Y, cdf, guess, k = None, method = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000})
):
    
    """
    Using MLE, estimates the alpha, betas and distributional parameters for 
    and ordered regression model. 
    
    Parameters:
    ------------------------
    Look at loglike_ordered docstring for parameters (X, Y, cdf, paravec_cdf, k)
   
    cdf:
        specified cdf (eg. normal, laplace, snormal, sged)
    guess:
        Initial guess for all the parameters. 
    method:
        Optimization method to use with the minimize command. Default is 'Nelder-Mead'
    options:
        Allows more iterations and function evaluations in the minimize command
    Returns:
    ------------------------
    result = contains alphas, betas and distributional parameters. Seems like a lot of 
    things to estimate.
    """
    if k is None:
        k = len(ao.itable(Y))

    cri_ordered = lambda x: loglike(x, X, Y, cdf, k, sign = -1)

    results = minimize(cri_ordered, guess, method = method, options = options)

    return results


