*This data comes from http://www3.norc.org/GSS+Website/Download/STATA+v8.0+Format/.
*It's the merged GSS 2012 data, it has more observations than just the 2012 data

use "C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\GSS_runs\gss2012merged_r1.dta", clear

keep happy income realinc educ sex region 
drop if missing(happy)
drop if missing(income)
drop if missing(realinc)
drop if missing(educ)
drop if missing(sex)
drop if missing(region)

replace sex = 0 if sex ==2
gen lincome = log(realinc)
gen lincome2 = lincome*lincome

oprobit happy income sex educ /*ooh. our income probably should be logged*/
oprobit happy lincome educ sex

*Fixing our happiness variable
replace happy = happy -1
replace happy = -1 if happy == 2
replace happy = 2 if happy == 0
replace happy = 0 if happy == -1

oprobit happy educ sex lincome
oprobit happy educ sex lincome lincome2 

export delimited using "C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\GSS_runs\gss_12merge.csv", nolabel replace
