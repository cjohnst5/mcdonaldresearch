#Just trying out my ordered probit with happiness and income data. 
#This data comes from the GSS 2012 merged survey. I worked with the data in Stata
#and then exported it to a csv file. 


from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 
from matplotlib import pyplot as plt
from scipy.stats import norm, laplace, t 
from scipy.optimize import fmin_bfgs, minimize


import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\shinycode')
from opt_main import *
from ordered_main import *
from std_errs1 import *
from sigma_calc import *
from sgtcdfs import *
from sgtpdfs import *
from loglike import *

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code')
from simul_func import *

data = np.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_reg_code\GSS_runs\gss_12merge.csv', \
        delimiter = ',', skiprows = 1)

#if you refer to the csv file, gss_12merge.csv, you'll see the variable names. 



#-----------------------------------------------------------------------
#Let's try the oprobit with happy (dependent), lincome, educ and gender
#-----------------------------------------------------------------------
X = data[:, [0,1,6,7]]
Y = data[:, 4]

cdf_case = 4
het_form = None  
o_probit_test = output_function(cdf_case, Y, X, het_form, method_opt = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000}), method_std = 'optgradient')