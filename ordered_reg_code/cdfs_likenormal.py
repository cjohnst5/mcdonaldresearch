#cdfs_likenormal
from __future__ import division
import scipy as sp
import numpy as np
from scipy.optimize import fmin_bfgs, minimize
from scipy.stats import norm
import altered_cdfs as alt
import adorio_ologit as ao

import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *

#this page is just a check to see if I get the same results as oprobit when I specify the
#ged to have m = 0, s= 2**.5, and p = 2
#it will probably be obsolute once I get the program up and running. 

def carla_ged_cdf(y):
    
    m = 0
    s = 2**.5
    p=2  
    
    z = abs(y-m) ** p / (s ** p * (1) ** p)
    cdf = (1) / 2 + ((1) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def ged_cdf_scale(y):
    p = 2
    m = 0
    phi = (gamma(1/p) / gamma(3/p))**(1/2)

    z = abs(y-m) ** p / (phi ** p * (1) ** p)
    cdf = (1) / 2 + ((1) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def snormal_cdf_scale(y):    
    lamb = 0
    m = 0 
    phi = (gamma(1/2)/ (gamma(3/2) + lamb**2 * (3 * gamma(3/2) - 4 / gamma(1/2))))**.5
    
    z = abs(y-m) ** 2 / (phi ** 2 * (1 + lamb * np.sign(y-m)) ** 2 )
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf

def slaplace_cdf_scale(y):
    lamb = 0
    #our two scale parameters:
    m = 0
    phi = 1     
    z = abs(y-m) / (phi * (1 + lamb * np.sign(y-m)))
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1, z)
    return cdf

def sged_cdf_scale(y):
    lamb, p = 0, 1

    #our two scale parameters  
    m = 0                                                           #we are not estimating the location or scale parameter. 
    phi = (gamma(1/p) / (gamma(3/p) + lamb ** 2 * \
         (3 * gamma(3/p) - 4 * gamma(2/p)**2 / gamma(1/p))))**(1/2) #we get this from calculating variance
                                                                    #of the SGED and solving for phi. 
    
    z = abs(y-m) ** p / (phi ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf

def loglike_ordered(paravec_ab, X, Y, k, cdf, sign = 1):
    
    """
    Calculates the probability of a set of ordered outcomes assuming the 
    standard normal cumulative density function

    Parameters:
    .................................
    paravec_ab:
        Includes all parameters needed to be estimated. Can be described as the
        following:
        a_0, a_1, ... a_(j-1), b_0, b1, ... b_(k-1) where a_j are the cutoff values and 
        b_i are the slope parameters.
        An intercept/constant is not included. 
    X: 
        numpy array containing the x-variable observations
    Y:  
        numpy array containing the ordinal Y values(must be integers, must start at 0)
    
    k:
        Number of categories the Y variable spans
    sign:
        If we would like to minimize, then we must multiply by -1
    cdf:
        What cdf I would like to use. Function only of a random variable, y  

    Returns:
    ------------------------------------------
    L:
        The probability of the random variable Y occurring 
    """
    L = 0
    n = len(Y)
    alpha = paravec_ab[0:k-1]
    betas = paravec_ab[k-1:]
    
    count = 0
    for j in xrange(k):
        for i in xrange(n):            
            if Y[i] == j: 
                # print '\n Y', Y[i]
                xb = np.dot(X[i], betas)    
                # print  'xb', xb
                count += 1             
                if j == 0:
                    z = alpha[j] - xb
                    prob = cdf(z)
                    # print z, "alpha - xb"
                    # print prob, "prob when j = 0"
                elif j == k-1:
                    z = alpha[j-1] - xb
                    prob = 1 - cdf(z)
                    # print z, "alpha- xb"
                    # print prob, "prob when j=k-1"
                else: 
                    z1 = alpha[j] - xb
                    z0 = alpha[j-1] - xb
                    cdf1 = cdf(z1)
                    cdf0 = cdf(z0)                     
                    prob = cdf1 - cdf0
                    # print z1, z0, "z1, z0"
                    # print cdf1, "J"
                    # print cdf0, "J-1"
                    # print prob, "prob when 0<j<k-1"
                L += sign * np.log(prob)
    return L


def estm_ordered(X, Y, cdf, guess, k = None, method = 'Nelder-Mead', options = ({'maxiter': 20000, 'maxfev' : 20000})):
    
    """
    Using MLE, estimates the alpha, betas and distributional parameters for 
    and ordered regression model. 
    
    Parameters:
    ------------------------
    Look at loglike_ordered docstring for parameters (X, Y, k)
   
    cdf: 
        cdf of the distribution we would like to use. Since there are no parameters
        specified for the distribution, this will usually be a standard normal or 
        standard laplace. 
    guess:
        Initial guess for all the parameters. 
    method:
        Optimization method to use with the minimize command. Default is 'Nelder-Mead'
    options:
        Dictionary of options specifying more function evaluations and iterations. 

    Returns:
    ------------------------
    result = contains alphas, betas and distributional parameters. Seems like a lot of 
    things to estimate.
    """
    if k is None:
        k = len(ao.itable(Y))
    cri_ordered = lambda x: loglike_ordered(x, X, Y, k, cdf, sign = -1)

    results = minimize(cri_ordered, guess, method = method, options = options)

    return results




