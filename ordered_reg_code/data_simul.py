#data_simul.py
#Last updated July 17, 2013

#Using data with normally distributed errors to try and get my GED to work. 

from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import carla_ordered as cor
import altered_cdfs as alt
import cdfs_likenormal as lk 
import hetero_cdfs as hc 
from plotfunc import pdf_cdf_plot
from scipy.stats import norm, laplace
from time import time
from std_errs import std_errs
from simul_func import simul_data

import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *
from sgttree_pdfs import *

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\mle')
from ged_MLE import ged_MLE
from snormal_MLE import snormal_MLE
from sged_MLE import sged_MLE
from sgt_MLE import sgt_MLE

x1 = sp.random.normal(size = 500)
x2 = sp.random.normal(size = 500)
x = np.vstack((x1, x2)).T

err = sp.random.normal(size = 500)
y_star = .5 * x1 + 2 * x2 + err
# y_star = .5 * x1 + err
y_obs = sp.ones((y_star.size))



for i in xrange(len(y_star)):
    if y_star[i] <=-4:        
        y_obs[i] = 0

    elif -4 < y_star[i] <= -2:
        y_obs[i] = 1

    elif -2 < y_star[i] <= 0:
        y_obs[i] = 2

    elif 0 < y_star[i] <=2:
        y_obs[i] = 3  

    elif 2 < y_star[i] <= 4:
        y_obs[i] = 4

    elif y_star[i] > 4:
        y_obs[i] = 5

    
# y_obs = y_obs.astype(int)
# data = np.vstack((y_obs, x1, x2)).T
# np.savetxt('simul_normal.csv', data, delimiter = ",")
# options = ({'maxiter': 20000, 'maxfev' : 20000})
# k = 6

#why does my standard error work with 2 categories, but not more? 
k = 4
Y, X, alphas = simul_data(500, [.5,2], k)
guess = [-1, 0, 1, .5, 2]

est1_param = lk.estm_ordered(X, Y, norm.cdf, guess, k, method = 'Nelder-Mead')
quad1, quad2, quad4, hess_exp, errs1 = std_errs(Y, X, est1_param.x[0:k-1], est1_param.x[k-1:], [0], norm.pdf, norm.cdf, method = 'invneghess')
data = np.concatenate((Y.reshape(len(Y),1), X), axis =1)
np.savetxt('simul_normal.csv', data, delimiter = ",")

# #.......................................GED................................................

# #ordered probit regression
# guess3_ged = [-3, -2, 0, 2, 3, .5, 2]
# est1_param = lk.estm_ordered(x, y_obs, norm.cdf, guess3_ged, k, method = 'Nelder-Mead')
# quad1, quad2, quad4, hess_exp, errs1 = std_errs(y_obs, x, est1_param.x[0:5], est1_param.x[5:], [0], norm.pdf, norm.cdf)

# estimate betas and alphas when m = 0, s = 1, and p = 2 (normal distribution). 
# est1_ged = lk.estm_ordered(X, y_obs, lk.ged_cdf_scale,  guess3_ged, k, method = 'Nelder-Mead')
# # # #cool. All the above results are the same. 

# # #estimate betas, alphas, p when m = 0 and s = 1 and phi is function of p.
# guess2_ged = [0, .5, 2, 2]
# est2_ged = cor.estm_ged(x, y_obs, alt.ged_cdf_scale, guess2_ged, k, method = 'Nelder-Mead')

# # #f(xb) * b = partial effects
# phi = alt.phi ([0, est2_ged.x[-1]])
# pdf_atmeans = ged_pdf([0, phi, est2_ged.x[-1]], np.dot(x.mean(axis=0), est2_ged.x[1:-1]))
# x1b1_x2b2 = x.mean(axis = 0) * est2_ged.x[1:-1]

# margins = pdf_atmeans * est2_ged.x[1:-1]
#this gives me the same distribution parameters as GQRmain, but different betas. 

# # #trying to estimate GED parameters using only MLE and compare those to what we get
# # #when doing ordered regression stuff. 
# # # guess_mle_ged = [0, 1, 2]
# # # est3_ged, est3_ged_trans = ged_MLE(err, guess_mle_ged)

# # # #..................................SNORMAL.............................................................

# # # #trying out the snormal
# # # guess_snormal = [-7, 1, .5, .1, -.5, 1.5]
# # # est1_snormal = cor.estm_ged(x, y_obs, snormal_cdf, guess_snormal, k, method = 'Powell')
# # #I think, due to identification issues, it is impossible to estimate all the distribution parameters
# # #for snormal, so we will used the scaled version.


# # #checking to see if I get the same results as the normal distribution
# # # guess3_snormal = [-3, 3, 1]
# # # est1_snormal = lk.estm_ordered(x, y_obs, lk.snormal_cdf_scale, guess3_snormal, k, method = 'Nelder-Mead')
# # # # #this gives me the same results as when I use the standard normal distribution.

# # #estimating using the snormal distribution wtih m = 0 and and phi(p).
# guess_snormal2 = [0, .5, 2, 0]
# est2_snormal = cor.estm_ged(x, y_obs, alt.snormal_cdf_scale, guess_snormal2, k, method = 'Nelder-Mead')
# # # #this works really nicely

# # # # #using MLE to estimate  the parameters of the snormal
# # # # guess_mle_snormal = [0, 0, 1]
# # # # est3_snormal, est2_snormal_trans = snormal_MLE(err, guess_mle_snormal)

# # # #.............................Laplace ..............................................
# # # #using a laplace distribution instead of normal. All distribution parameters are 
# # # #specified, so no need to estimate those, just alphas and betas
# guess1_laplace = [0, .5, 2]
# est1_laplace = lk.estm_ordered(x, y_obs, laplace.cdf, guess1_laplace, k, method = 'Nelder-Mead')
# # # #yay. This is the same result as the GQRmain. 

# # # # #............................Skewed laplace..........................................
# # # #checking to see if this gives me the same results as MLE using laplace
# # # guess1_slaplace = [-3, 3, 1]
# # # est1_slaplace = lk.estm_ordered(x, y_obs, lk.slaplace_cdf_scale, guess1_slaplace, k, method = 'Nelder-Mead')

# # # # #m = 0, s = 1, estimating lambda
# guess2_slaplace = [0, .5, 2, 0]
# est2_slaplace = cor.estm_ged(x, y_obs, alt.slaplace_cdf_scale, guess2_slaplace, k, method = 'Nelder-Mead')
# # # # #this works pretty nicely as well. 

# # # #............................SGED...................................................

# # # # #trying ordered regression estimation with the SGED
# # # # guess_sged = [-7, 1, .5, 0, 0, 1, 2]
# # # # est1_sged = cor.estm_ged(x, y_obs, sged_cdf, guess_sged, k, method = 'Nelder-Mead', options = options)
# # # # #due to identifcation issues, I don't think this way works. We have to standardize. 

# # # # #checking to make sure I get the same as laplace MLE when specific.
# # # guess3_sged = [-3, 3, 1]
# # # est1_sged = lk.estm_ordered(x, y_obs, lk.sged_cdf_scale, guess3_sged, k, method = 'Nelder-Mead')
# # # est2_laplace = lk.estm_ordered(x, y_obs, alt.laplace_cdf_scale, [-3, 3, 1], k, method = 'Nelder-Mead')



# # # # #setting m = 0 and sigma^2 = 1 and estimating betas, alphas, lambda and p. 
# guess2_sged = [0, .5, 2, 0, 2]
# begin = time()
# est2_sged = cor.estm_ged(x, y_obs, alt.sged_cdf_scale, guess2_sged, k, method = 'Nelder-Mead', options = options)
# end = time() - begin

# # # #MLE results
# # # guess_mle_sged = [0, 0, 1, 2]
# # # est3_sged, est3_sged_trans = sged_MLE(err, guess_mle_sged)



# # # #.................................SGT................................................

# guess_sgt = [-7, -1, .5, 0, 0, 1, 2, 100]
# est1_sgt = cor.estm_ged(x, y_obs, sgt_cdf, guess_sgt, k, method = 'Nelder-Mead')

# # # guess_mle_sgt=[0, 0, 1, 2, 100]
# # # est2_sgt, est2_sgt_trans = sgt_MLE(err, guess_mle_sgt)


# # # .................................STD ERRS..............................................
#checking out my standard errors function. 

# guess = [-4, -2, 0, 2, 4, .5, 2]
# estimate = lk.estm_ordered(x, y_obs, norm.cdf, guess, 6, method = 'Nelder-Mead')
# alphas = np.asarray(estimate.x[0:5])
# betas = np.asarray(estimate.x[5:])
# alpha_mat, beta_vec, gradrep, grad_exp, errs = std_errs(y_obs.astype(int), x, alphas, betas, [0], norm.pdf, norm.cdf)

#it works!


# # #PLOTS..................................................................................................

# x_range = np.linspace(-4,4,500)

# # # #plotting my actual errors with MLE:
# # # fig = plt.figure()

# # # ged_pdfpoints1 = ged_pdf(est3_ged, x_range)
# # # plt.hist(err, bins = 50, normed = True)
# # # # plt.plot(x_range, ged_pdfpoints1)
# # # plt.title('Errors')



# #plotting the GED, only p estimated
# fig = plt.figure()
# pdf_cdf_plot('GED', ged_pdf, ged_cdf, est2_ged.x[-1], alt.phi)

# # #plotting the SNORMAL
# fig = plt.figure()
# pdf_cdf_plot('Snormal', snormal_pdf, snormal_cdf, est2_snormal.x[-1], alt.phi)

# # # #plotting the SLaplace
# fig = plt.figure()
# pdf_cdf_plot('Slaplace', slaplace_pdf, slaplace_cdf, est2_slaplace.x[-1], alt.phi)

# # # #plotting the SGED
# fig = plt.figure()
# pdf_cdf_plot('SGED', sged_pdf, sged_cdf, est2_sged.x[-2:], alt.phi)

# #plotting the SGT
# fig = plt.figure()
# sgt_pdfpoints1 = sgt_pdf(est1_sgt.x[-5:], x_range)
# sgt_pdfpoints2 = sgt_pdf(est2_sgt, x_range)
# plt.plot(x_range, sgt_pdfpoints1, linewidth = 2, label = 'Ordered Estimate')
# plt.plot(x_range, sgt_pdfpoints2, linewidth = 2, label = 'MLE')
# plt.title('SGT')
# plt.legend()



# plt.show()

