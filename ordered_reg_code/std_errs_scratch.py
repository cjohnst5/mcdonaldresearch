from __future__ import division
import numpy as np
import scipy as sp
import scipy.linalg as la
from scipy.stats import norm, laplace
from simul_func import simul_data
from std_errs import *

betas = [.5, 2]
Y, X, alphas = simul_data(500, betas, 5)
m = len(Y)
xb = np.dot(X, betas)
alpha_dd_mat = np.zeros((m, len(alphas)))
a01 = Y == 0
z0 = alphas[0] - xb

rho1 = getrhoGQR(xb, alphas[0], 1)
alpha_dd_mat[:,0][a01] = -rho1[a01] * norm.pdf(z0[a01]) / norm.cdf(z0[a01]) -\
                        (norm.pdf(z0[a01]) / norm.cdf(z0[a01])) **2

# ab_cross_mat[:,0][a01] = rho1[a01] * pdf(z0[a01]) / cdf(z0[a01]) + \
#                         (pdf(z0[a01]) / cdf(z0[a01])) **2
# beta_hess[:,0][a01] = -rho1[a01] * pdf(z0[a01]) / cdf(z0[a01]) - \
                                    # (pdf(z0[a01]) / cdf(z0[a01])) **2

                                    