from __future__ import division
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import carla_ordered as cor
import altered_cdfs as alt
import cdfs_likenormal as lk 
import hetero_cdfs as hc 
from plotfunc import pdf_cdf_plot
from scipy.stats import norm, laplace
from time import time
from simul_func import *
from std_errs import *

import sys
sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *
from sgttree_pdfs import *

sys.path.insert(0, 'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\mle')
from ged_MLE import ged_MLE
from snormal_MLE import snormal_MLE
from sged_MLE import sged_MLE
from sgt_MLE import sgt_MLE

   

k = 2
Y, X, alphas = simul_data(500, [.5], k)
data = np.concatenate((Y.reshape(len(Y),1), X), axis = 1)
np.savetxt('simul_normal_het.csv', data, delimiter = ",")
options = ({'maxiter': 20000, 'maxfev' : 20000})


# # #............................normal oprobit model.................
guess3_ged = [0, 1]
est1_param = lk.estm_ordered(X, Y, norm.cdf, guess3_ged, k, method = 'Nelder-Mead')
quad1, quad2, quad4, hess_exp, errs1 = std_errs(Y, X, [est1_param.x[0]], est1_param.x[1:], [0], norm.pdf, norm.cdf, method = 'invneghess')

# # # #.........................hetprobit model...........................
# guess1_normalhet = [0, .5, 2, 0, 0]
# est_normalh = hc.estm_ordered(x, y_obs, hc.normal_cdf_het, guess1_normalhet, k, method = 'Nelder-Mead', options = options)

# #......................hetged model...............................

# #checking to see if it gives me the same results as the normal.
# guess1_gedhet = [2, 0, .5, 2, 0, 0]
# begin_ged = time()
# est1_gedh = hc.estm_ordered(x, y_obs, hc.ged_cdf_het, guess1_gedhet, k, method = 'Nelder-Mead')
# end_ged = time() - begin_ged

# #this GED model works

#..................hetsnormal model............................
# guess1_snormalhet = [.13, .5, 2, 0, 0, 0]
# begin_snormal = time()
# est1_snormalh = hc.estm_ordered(x, y_obs, hc.snormal_cdf_het, guess1_snormalhet, k, method = 'Nelder-Mead')
# end_snormal = time() - begin_snormal



#................hetlaplace model............................
# guess1_laplacehet = [0, .5, 2, 0, 0]
# begin_laplace = time()
# est1_laplaceh = hc.estm_ordered(x, y_obs, hc.laplace_cdf_het, guess1_laplacehet, k, method = 'Nelder-Mead')
# end_laplace = time() - begin_laplace

#................hetslaplace model...........................
# guess1_slaplacehet = [0, .5, 2, 0, 0, 0]
# begin_slaplace = time()
# est1_slaplaceh = hc.estm_ordered(x, y_obs, hc.slaplace_cdf_het, guess1_slaplacehet, k, method = "Nelder-Mead")
# end_slaplace = time() - begin_slaplace
# #this works rather well. 

#.................hetsged model..................
# guess1_sgedhet = [0, .5, 2, 0, 2, 0, 0]
# begin_sged = time()
# est1_sgedh = hc.estm_ordered(x, y_obs, hc.sged_cdf_het, guess1_sgedhet, k, method = 'Nelder-Mead')
# end_sged = time() - begin_sged
#this seems to work too. 

#seeing if my loglikelihood function works. 
# a = hc.loglike(guess1_snormalhet, x, y_obs, hc.snormal_cdf_het, k)