#function that plots the pdf and cdf of distribution.

from __future__ import division
import matplotlib.pyplot as plt
import scipy as sp
import numpy as np


def pdf_cdf_plot(dist, pdf, cdf, paravec, scale_func, x_obs = None):
    """
    Function that plots the estimated pdf and cdf of distributions. This is written with 
    the ordered regression MLE in mind. 

    Parameters
    ----------------------------------------------------
    dist:
        String. Can either be GED, Snormal, Slaplace, or SGED.
    pdf:
        Python function to calculate the pdf of the specificed distribution
    cdf:
        Python function to calculate the cdf of the specified distribution
    paravec:
        Vector of distribution parameters
    scale_func:
        Function to calculate the scale variable, phi, in the GED, Snormal, Slaplace and SGED 
        distribution. Takes one argument: a numpy vector [lambda, p]    

    """

    x_range = np.linspace(-4, 4, 500)
    if dist == 'GED':
        p = paravec
        phi = scale_func([0, p])
        pdf_points = pdf([0, phi, paravec], x_range)
        cdf_points = cdf([0, phi, paravec], x_range)
    elif dist == 'Snormal':
        lamb = paravec
        phi = scale_func([lamb, 2])
        pdf_points = pdf([0, lamb, phi], x_range)
        cdf_points = cdf([0, lamb, phi], x_range)
    elif dist == 'Slaplace':
        lamb = paravec
        phi = scale_func([lamb, 1])
        pdf_points = pdf([0, lamb, phi], x_range)
        cdf_points = cdf([0, lamb, phi], x_range)   
    elif dist == 'SGED':
        lamb, p = paravec
        phi = scale_func(paravec)
        pdf_points = pdf([0, lamb, phi, p], x_range)
        cdf_points = cdf([0, lamb, phi, p], x_range)
    

    plt.subplot(2, 1, 1)
    plt.plot(x_range, pdf_points)
    plt.title((dist, 'pdf'))

    plt.subplot(2, 1, 2)
    plt.plot(x_range, cdf_points)
    plt.title((dist, 'cdf'))


