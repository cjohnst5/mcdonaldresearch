Last updated July 15, 2013
Carla Johnston
This gb2tree folder contains code for estimating the parameters of the GB2, Burr3, Burr12, Generalized Gamma, Gamma and Lognormal distributions for a given dataset. 

The two datasets I have tried this on are income_data.csv, which is income data from Professor McDonald and ceosal.csv, which is 1999 CEO salary data obtained from http://www.oswego.edu/~kane/econometrics/ceo.htm.  

The folder, MLE_code contains code for estimating the above distributions' parameters, using log-likelihood functions as the objective function. 

The folder, MLE_trans_code contains code for estimating the above distributions' parameters, using transformed log-likelihood functions. The natural logs of the parameters are passed in, instead of the parameters themselves. 
This way we constrain the optimization problem to only produce positive parameter estimates. 

The folder, graphs, only contains graphs of estimated distributions using both methods. The graph titles are more explicative than the file names.  