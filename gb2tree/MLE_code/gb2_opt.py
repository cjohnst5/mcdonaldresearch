from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl
reload(sl)

sal = sl.sal
tolerance = 1e-5
guess = [3.7, 700000, .466, 1040]
guess1 = [1, 500000, 1, 1000]
word = str(guess)
#word1 = str(guess1)
method = 'Powell'

#changing maximum number of evaluations and such
opts = ({'maxiter': 10000, 'maxfev' : 10000})

cri_gb2 = lambda x: gb2tf.criterion_gb2(x, sal)
gb2 = opt.minimize(cri_gb2, guess, method = method,  tol = tolerance, options = opts)
#gb21 = opt.minimize(cri_gb2, guess1, method = method, constraints = gb2tf.cons_gb2, tol = tolerance, options = opts)



#getting points
x_range = sp.linspace(sal.min(), sal.max(), 500)
gb2_points1 = gb2tf.gb2pdf(gb2.x[0], gb2.x[1], gb2.x[2], gb2.x[3], x_range)
gb2_points2 = gb2tf.gb2pdf(3.73692, 690338, .466953, 10390, x_range)
#gb2_points3 = gb2tf.gb2pdf(gb21.x[0], gb21.x[1], gb21.x[2], gb21.x[3], x_range)

#plotting things:
figure = plt.figure()
plt.hist(sal, bins = 100, normed = True)
plt.plot(x_range, gb2_points1, 'g', label = word)
plt.plot(x_range, gb2_points2, 'r--', label = 'actual GB2')
#plt.plot(x_range, gb2_points3, 'y--', label = word1)
plt.title(('GB2',method), fontsize = 'small')
plt.legend()
plt.show()