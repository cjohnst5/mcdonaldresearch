from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl
reload(sl)

sal = sl.sal
tolerance = 1e-5

cri_lognormal = lambda x: gb2tf.criterion_lognormal(x, sal)
guess = [10, 10]
method = 'Nelder-Mead'

lognormal = opt.minimize(cri_lognormal, guess, method = method, tol = tolerance)

