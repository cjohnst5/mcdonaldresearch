from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl 
reload(sl)


sal = sl.sal
tolerance = 1e-5

opts = ({'maxiter': 15000, 'maxfev' : 15150})

cri_gamma = lambda x: gb2tf.criterion_gamma(x, sal)
guess = [3, 100]
method = 'Nelder-Mead'

gam = opt.minimize(cri_gamma, guess, method = method, tol = tolerance)

#this works nicely. I'm not going to plot it since it converges easily. 