from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis




def log_gb2(a, b, p, q, data):
    """
    Returns the log-likelihood of a gb2 distribution. This is a very general form that can
    be modifed for the burr3 and burr12. 

    Parameters
    .....................
    a, b, p, q: the parameters that are included in the gb2 function. 
    data: column vector that gives us a our observations. 

    Returns
    .............
    log_likehood value. 
    """
    log_like = len(data) * sp.log(abs(a)) + (abs(a)*p-1) * sum(sp.log(data)) \
     - len(data)*abs(a)*p*sp.log(b) - len(data) * betaln(p, q) - (p+q) * sum(sp.log(1+(data/b)**abs(a)))
    return log_like




def criterion_gb2 (paravec1, data):
    """
    a function that allows us to estimate a, b, p, q from the GB2 distribution when using an optimizer or a minimizer. 
    """
    a = paravec1[0]
    b = paravec1[1]
    p = paravec1[2]
    q = paravec1[3]
    return -log_gb2(a, b, p, q, data)




def criterion_burr12(paravec2, data):
    """
    a function that allows us to estimate a, b, q for the burr12 when using an optimizer or a minimizer. 
    """
    a, b, q = paravec2
    p = 1
    return -log_gb2(a, b, p, q, data)




def criterion_burr3(paravec3, data):
    """
    a function that allows us to estimate a, b, p for the burr3  when using an optimizer or a minimizer. 
    """
    
    a = paravec3[0]  
    b = paravec3[1]
    p = paravec3[2]
    q = 1
    return -log_gb2(a, b, p, q, data)




def criterion_gengamma(paravec4, data):
    """
    a function that allows us to estimate a, b, p for the generalized gamma when using an optimizer or a minimizer. 
    """
    a, b, p = paravec4
    log_like = len(data) * sp.log(abs(a)) + (abs(a)*p-1) * sum(sp.log(data)) - (1/b**abs(a)) * sum(data**abs(a))\
    -len(data)*abs(a)*p*sp.log(b) - len(data) * gammaln(p)
    return -log_like




def criterion_gamma(paravec5, data):
    """
    a function that allows us to estimate  b, p for the gamma distribution when using an optimizer or a minimizer. 
    """
    b, p = paravec5
    log_like = (p-1)*sum(sp.log(data)) - (1/b)*sum(data) - len(data)*gammaln(p) - len(data)*p*sp.log(b)
    return -log_like




def criterion_lognormal(paravec, data):
    """
    a function that allows us to estimate a, b for the lognormal when using an optimizer or a minimizer. 
    """
    mu= paravec[0]
    sigma = paravec[1]
    log_like = (-1/(2*sigma**2)) * sum((sp.log(data)-mu)**2) - (len(data)/2) * sp.log(2*sp.pi) - len(data) * sp.log(sigma) - sum(sp.log(data))
    return -log_like


    

#writing constraints to our function. We want our parameters to be positive
#I'm only going to start with a and p, since those are the ones giving us trouble

#to use with the minimize command, COBYLA method
cons_gb2 = ({'type' : 'ineq', 
             'fun' : lambda x: np.array(x[0]),
             'jac'  : lambda x: np.array ([1, 0, 0, 0])},
            {'type' : 'ineq', 
             'fun' : lambda x: np.array(x[1]),
             'jac'  : lambda x: np.array([0, 1, 0, 0])},
            {'type' : 'ineq', 
             'fun' : lambda x: np.array(x[2]),
             'jac'  : lambda x: np.array ([0, 0, 1, 0])},
            {'type' : 'ineq', 
             'fun' : lambda x: np.array(x[3]),
             'jac'  : lambda x: np.array([0, 0, 0, 1])},
            )

cons_gb2_c = [lambda x: np.array(x[0]), 
              lambda x: np.array(x[1]), 
              lambda x: np.array(x[2]), 
              lambda x: np.array(x[3])]

cons_b12 = ({'type' : 'ineq', 
             'fun' : lambda x: np.array(x[0]),
             'jac'  : lambda x: np.array ([1, 0, 0])},
            {'type' : 'ineq', 
             'fun' : lambda x: np.array(x[1]),
             'jac'  : lambda x: np.array([0, 1, 0])},
            {'type' : 'ineq', 
             'fun' : lambda x: np.array(x[2]),
             'jac'  : lambda x: np.array ([0, 0, 1])})


#writing some pdf functions so I can plot them:
def gb2pdf(a, b, p, q, y = None):    
    pdf = abs(a) * y **(a*p - 1) / (b**(a*p) * beta(p, q) * (1 + (y/b)**a)**(p+q))
    return pdf 
def ggampdf(a, b, p, y):
    pdf = abs(a) * y**(a*p - 1) * np.exp(-(y/b)**a)/(b**(a*p) * gamma(p))
    return pdf
def lognormpdf(mu, sigma, y):
    pdf = np.exp(-(np.log(y) - mu)**2 / (2 * sigma**2)) / ((2 * np.pi)**(1/2) * sigma * y)
    return pdf



#So where I'm at right now: I can't get positve a values for the GB2 and Burr12
#COBYLA seems like a good path, but it maxes out iterations. Try a different 
#optimization approach??