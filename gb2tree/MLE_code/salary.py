from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis

#Load the datafile here and the rest of the distributions will get it from this file. 


sal = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\flexibledist\\income_data.csv", delimiter = ",")
sal = sal[sal>0] 
sal = sal[sal < 80000]