#ggamma_opt.py
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl
reload(sl)

sal = sl.sal
tolerance = 1e-5

#changing maximum number of evaluations and such
opts = ({'maxiter': 15000, 'maxfev' : 15150})
guess = [3, 5000, 1]
word = str(guess)
method = 'Nelder-Mead'

cri_gengamma = lambda x: gb2tf.criterion_gengamma(x, sal)
ggam = opt.minimize(cri_gengamma, guess, method = method, tol = tolerance, options = opts)


#ggam1 = opt.minimize(cri_gengamma, [.27, .44, 3.4], method = 'Powell', tol = tolerance, options = opts)
#the above minimization gets me convergence and reasonable parameters for ceosal



#getting some points for plotting
x_range = sp.linspace(sal.min(), sal.max(), 500)
ggam_points1 = gb2tf.ggampdf(ggam.x[0], ggam.x[1], ggam.x[2], x_range)
ggam_points2 = gb2tf.ggampdf(3.739, 58098.6, .46686, x_range)



#plotting the generalized gamma against true values:
fig = plt.figure()
plt.hist(sal, bins = 100, normed = True)
plt.plot(x_range, ggam_points1, 'r', label = guess)
#plt.plot(x_range, ggam_points2, 'y--', label = 'Actual ggam')

plt.title(('GGamm',method))
plt.legend()
plt.show()

#look at OneNote work log for more thoughts on this. I think we have found a winner and it is COBYLA
