#gb2all_plot.py

from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf
import salary as sl
import gb2_opt as g
import burr12_opt as b12
import burr3_opt as b3
import ggamma_opt as gg
import gamma_opt as gm
import lognormal_opt as ln 
reload(g)
reload(b12)
reload(b3)
reload(gg)
reload(gm)
reload(ln)



#plotting
x_range = sp.linspace(sl.sal.min(), sl.sal.max(), 500)
gb2_points = gb2tf.gb2pdf(g.gb2.x[0], g.gb2.x[1], g.gb2.x[2], g.gb2.x[3], x_range)
burr12_points = gb2tf.gb2pdf(b12.burr12.x[0], b12.burr12.x[1], 1, b12.burr12.x[2], x_range)
burr3_points = gb2tf.gb2pdf(b3.burr3.x[0], b3.burr3.x[1], b3.burr3.x[2], 1, x_range)
gamma_points = gb2tf.ggampdf(1, gm.gam.x[0], gm.gam.x[1], x_range)
ggamma_points = gb2tf.ggampdf(gg.ggam.x[0], gg.ggam.x[1], gg.ggam.x[2], x_range)
lognorm_points = gb2tf.lognormpdf(ln.lognormal.x[0], ln.lognormal.x[1], x_range)



figure = plt.figure()
plt.hist(sl.sal, bins = 100, normed = True)
plt.plot(x_range, burr12_points, color = 'r', linewidth = 2, label = 'Burr12')
plt.plot(x_range, burr3_points, 'g--', linewidth = 2, label = 'Burr3')
plt.plot(x_range, gamma_points, color = 'black', linewidth = 2, label = "Gamma")
plt.plot(x_range, gb2_points, 'y', linewidth = 2, label = 'GB2')
plt.plot(x_range, ggamma_points, 'm', linewidth = 2, label = 'GG')
plt.plot(x_range, lognorm_points, 'c', linewidth = 2, label = 'LN')

plt.legend()
plt.show()


