from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf



#plotting Professor's McDonald's estimated values for the distributions. 
sal = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\MLE_code\\income_data.csv", delimiter = ",")
sal = sal[sal>0] 
sal = sal[sal < 80000]
tolerance = 1e-5

x_range = sp.linspace(sal.min(), sal.max(), 500) 
gb2_points = gb2tf.gb2pdf(3.73692, 690338, .466953, 10390, x_range)
burr12_points = gb2tf.gb2pdf(2.22729, 1.31856e6, 1, 2066.93, x_range)
#burr3 is correct
burr3_points = gb2tf.gb2pdf(10.7399, 63172.7, .138184, 1, x_range)
gamma_points = gb2tf.ggampdf(1, 10519, 3.60516, x_range)
ggamma_points = gb2tf.ggampdf(3.739, 58098.6, .46686, x_range)
lognorm_points = gb2tf.lognormpdf(10.3983, .59647, x_range)



figure = plt.figure()
plt.hist(sal, bins = 100, normed = True)
plt.plot(x_range, burr12_points, color = 'r', linewidth = 2, label = 'Burr12')
plt.plot(x_range, burr3_points, 'g--', linewidth = 2, label = 'Burr3')
plt.plot(x_range, gamma_points, color = 'black', linewidth = 2, label = "Gamma")
plt.plot(x_range, gb2_points, 'y', linewidth = 2, label = 'GB2')
plt.plot(x_range, ggamma_points, 'm--', linewidth = 2, label = 'GG')
plt.plot(x_range, lognorm_points, 'c', linewidth = 2, label = 'LN')

plt.legend()
plt.show()

#this works with the gb2tree_func file. 