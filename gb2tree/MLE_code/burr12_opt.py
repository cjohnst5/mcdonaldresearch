from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl
reload(sl)

sal = sl.sal
tolerance = 1e-5


cri_burr12 = lambda x: gb2tf.criterion_burr12(x, sal)
#creating a dictionary of options to use with Nelder mead in the opt.minimize command
opts = ({'maxiter': 15000, 'maxfev' : 15150})

#initial guess that have led to convergence using Nelder-Mead: 
guess1 = [4.2, 1000, .3]
word1 = str(guess1)

#trying to get it to converge to Dr. McDonald's values.
guess = [2.22729, 1.31e6, 2066]

method = 'COBYLA'


burr12 = opt.minimize(cri_burr12, guess1, method = method, tol = tolerance, options = opts, constraints = gb2tf.cons_b12)
#burr12_base = opt.minimize(cri_burr12, guess1, method = 'Nelder-Mead', tol = tolerance)

#plotting the burr12
x_range = sp.linspace(sal.min(), sal.max(), 500)
burr12_points1 = gb2tf.gb2pdf(burr12.x[0], burr12.x[1], 1, burr12.x[2], x_range)

figure = plt.figure()
plt.hist(sal, bins = 100, normed = True)
plt.plot(x_range, burr12_points1, color = 'r', linewidth = 1, label = guess1)
#plt.plot(x_range, burr12_points2, 'g--', linewidth =1, label = 'Burr12 actual')


plt.title(('Burr12',method))
plt.legend()
plt.show()


#to do when I resume: Figure out if it is alright to be off by factors of ten
#try it with a different method. ..
#try the gb2 with more iterations and stuff. 

