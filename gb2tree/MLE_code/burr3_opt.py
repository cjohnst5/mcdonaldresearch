from __future__ import division
import sympy as sym
from sympy import pprint
import math
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy import linalg as la
import time
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func as gb2tf 
import salary as sl

sal = sl.sal
tolerance = 1e-5


cri_burr3 = lambda x: gb2tf.criterion_burr3(x, sal)
guess = [1, 1000, 1]
method = 'Nelder-Mead'

burr3 = opt.minimize(cri_burr3, guess, method = method, tol = tolerance)


