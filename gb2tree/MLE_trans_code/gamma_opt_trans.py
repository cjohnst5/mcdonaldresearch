#gam_opt_trans
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans


def gamma_MLE(data, guess = [3, 100], method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 1000, 'maxfev' : 1000})):
    """
    Uses the MLE transformed method to find b, p for the gamma distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to estimate the gamma distribution for
    guess:
        Initial guess to use in our minimization procedure.
        Given initial guess is one that I know works for income_data and ceosal datasets
    method:
        Minimization method to use
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    gamma:
        Estimated parameters for the gamma distribution
    gamma_log:
        output from the minimization procedure, including the ln(parameters)
    """
    cri_gamma = lambda x: gb2trans.criterion_gamma_exp(x, data)

    gamma_log = opt.minimize(cri_gamma, np.log(guess), method = method, tol = tolerance, options = options)
    gamma = np.exp(gamma_log.x)
    return gamma, gamma_log





# cri_gamma = lambda x: gb2trans.criterion_gamma_exp(x, sal)
# guess = 
# method = 'Nelder-Mead'

# gam_log = opt.minimize(cri_gamma, np.log(guess), method = method, tol = tolerance)
# gam = gb2trans.transform(gam_log.x)



#this works for income_data and ceosal. Initial guesses were the same for both of them. 