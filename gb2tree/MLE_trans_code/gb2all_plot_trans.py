#gb2all_trans_plot.py
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans
import gb2_opt_trans as g
import burr12_opt_trans as b12
import burr3_opt_trans as b3
import ggamma_opt_trans as gg
import gamma_opt_trans as gm
import lognormal_opt_trans as ln 
reload(g)
reload(b12)
reload(b3)
reload(gg)
reload(gm)
reload(ln)


#file that plots all the distributions. 

sal = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\income_data.csv", delimiter = ",")
sal = sal[sal>0] 
sal = sal[sal < 80000]

#plotting
# x_range = sp.linspace(sal.min(), sal.max(), 500)
# gb2_points = gb2trans.gb2pdf(g.gb2[0], g.gb2[1], g.gb2[2], g.gb2[3], x_range)
# burr12_points = gb2trans.gb2pdf(b12.burr12[0], b12.burr12[1], 1, b12.burr12[2], x_range)
# burr3_points = gb2trans.gb2pdf(b3.burr3[0], b3.burr3[1], b3.burr3[2], 1, x_range)
# gamma_points = gb2trans.ggampdf(1, gm.gam[0], gm.gam[1], x_range)
# ggamma_points = gb2trans.ggampdf(gg.ggam[0], gg.ggam[1], gg.ggam[2], x_range)
# lognorm_points = gb2trans.lognormpdf(ln.lognormal[0], ln.lognormal[1], x_range)



figure = plt.figure()
plt.hist(g.sl.sal, bins = 100, normed = True)
plt.plot(x_range, burr12_points, color = 'r', linewidth = 2, label = 'Burr12')
plt.plot(x_range, burr3_points, 'g--', linewidth = 2, label = 'Burr3')
plt.plot(x_range, gamma_points, color = 'black', linewidth = 2, label = "Gamma")
plt.plot(x_range, gb2_points, 'y', linewidth = 2, label = 'GB2')
plt.plot(x_range, ggamma_points, 'm', linewidth = 2, label = 'GG')
plt.plot(x_range, lognorm_points, 'c', linewidth = 2, label = 'LN')

plt.legend()
plt.title('GB2 tree distributions')
plt.show()
