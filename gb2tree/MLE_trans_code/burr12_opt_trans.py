from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans 





def burr12_MLE(data, guess = [4.2, 1000, .3], method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 15000, 'maxfev' : 15000})):
    """
    Uses the MLE transformed method to find a, b, p for the Burr3 distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to estimate the Burr3 distribution for
    guess:
        Initial guess to use in our minimization procedure.
        Given initial guess is one that I know works for income_data and ceosal datasets
    method:
        Minimization method to use
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    burr12:
        Estimated parameters for the burr3 distribution
    burr12_log:
        output from the minimization procedure, including the ln(parameters)
    """
    cri_burr12 = lambda x: gb2trans.criterion_burr12_exp(x, data)

    burr12_log = opt.minimize(cri_burr12, np.log(guess), method = method, tol = tolerance, options = options)
    burr12 = np.exp(burr12_log.x)
    return burr12, burr12_log





#this works for income_data and ceosal. Yesss. 