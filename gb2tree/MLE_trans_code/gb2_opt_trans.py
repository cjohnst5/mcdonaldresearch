from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans


def gb2_MLE(data, guess = [1, 500000, 1, 1000], method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 15000, 'maxfev' : 15000})):
    """
    Uses the MLE transformed method to find a, b, p, q for the gb2 distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to estimate the gb2 distribution for
    guess:
        Initial guess to use in our minimization procedure.
        Given initial guess is one that I know works for income_data and ceosal datasets
    method:
        Minimization method to use
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    gb2:
        Estimated parameters for the gb2 distribution
    gb2_log:
        output from the minimization procedure, including the ln(parameters)
    """
    cri_gb2 = lambda x: gb2trans.criterion_gb2_exp(x, data)

    gb2_log = opt.minimize(cri_gb2, np.log(guess), method = method, tol = tolerance, options = options)
    gb2 = np.exp(gb2_log.x)
    return gb2, gb2_log




# this works with income_data and ceosal. 
# it doesn't like Nelder-Mead too much. 