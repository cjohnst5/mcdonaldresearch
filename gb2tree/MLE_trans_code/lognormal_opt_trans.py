from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans 

def lognormal_MLE(data, guess = [10, 10], method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 1000, 'maxfev' : 1000})):
    """
    Uses the MLE transformed method to find mu, sigma for the lognormal distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to estimate the lognormal distribution for
    guess:
        Initial guess to use in our minimization procedure.
        Given initial guess is one that I know works for income_data and ceosal datasets
    method:
        Minimization method to use
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    lognormal:
        Estimated parameters for the lognormal distribution
    lognormal_log:
        output from the minimization procedure, including the ln(parameters)
    """
    cri_lognormal = lambda x: gb2trans.criterion_lognormal_exp(x, data)

    lognormal_log = opt.minimize(cri_lognormal, np.log(guess), method = method, tol = tolerance, options = options)
    lognormal = np.exp(lognormal_log.x)
    return lognormal, lognormal_log





