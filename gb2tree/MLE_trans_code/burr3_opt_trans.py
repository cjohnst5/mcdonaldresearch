from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2tree_func_trans as gb2trans 





def burr3_MLE(data, guess = [1, 1000, 1], method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 1000, 'maxfev' : 1000})):
    """
    Uses the MLE transformed method to find a, b, q for the Burr3 distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to estimate the Burr3 distribution for
    guess:
        Initial guess to use in our minimization procedure.
        Given initial guess is one that I know works for income_data and ceosal datasets
    method:
        Minimization method to use
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    burr3:
        Estimated parameters for the burr3 distribution
    burr3_log:
        output from the minimization procedure, including the ln(parameters)
    """
    cri_burr3 = lambda x: gb2trans.criterion_burr3_exp(x, data)

    burr3_log = opt.minimize(cri_burr3, np.log(guess), method = method, tol = tolerance, options = options)
    burr3 = np.exp(burr3_log.x)
    return burr3, burr3_log



