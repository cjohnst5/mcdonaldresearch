from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis




def log_gb2_exp(a, b, p, q, data):
    """
    Transformed log-likelihood function for a gb2 distribution

    Parameters
    .....................
    a, b, p, q: natural log of the original gb2 parameters, a, b, p, q.  
    data: column vector that gives us a our observations. 

    Returns
    .............
    log_likehood value. 
    """
    log_like = len(data) * sp.log(np.exp(a)) + (np.exp(a)*np.exp(p)-1) * sum(sp.log(data)) \
     - len(data)*np.exp(a)*np.exp(p)*sp.log(np.exp(b)) - len(data) * betaln(np.exp(p), np.exp(q)) - (np.exp(p)+np.exp(q)) * sum(sp.log(1+(data/np.exp(b))**np.exp(a)))
    return log_like

def criterion_gb2_exp(paravec1, data):
    """
    a function that allows us to estimate a, b, p, q from the GB2 distribution when using an optimizer or a minimizer. 
    """
    a = paravec1[0]
    b = paravec1[1]
    p = paravec1[2]
    q = paravec1[3]
    return -log_gb2_exp(a, b, p, q, data)

def criterion_burr12_exp(paravec2, data):
    """
    a function that allows us to estimate a, b, q for the burr12 when using an optimizer or a minimizer. 
    """
    a, b, q = paravec2
    p = 0
    return -log_gb2_exp(a, b, p, q, data)

def criterion_burr3_exp(paravec3, data):
    """
    a function that allows us to estimate a, b, p for the burr3  when using an optimizer or a minimizer. 
    """
    
    a = paravec3[0]  
    b = paravec3[1]
    p = paravec3[2]
    q = 0
    return -log_gb2_exp(a, b, p, q, data)

def criterion_ggamma_exp(paravec4, data):
    """
    Transformed log-likelihood for the generalized gamma.
    Parameters
    ------------------------
    paravec4:
        numpy array = [ln(a), ln(b), ln(p)]
        We take the log of the parameters and then exponentiate 
        terms with these paratmers in the log-likelihood
        to avoid using a constrained minimizer to solve for it. 
    returns: 
        Weird objective function. We are mostly concerned with the function
        to use in a minimizer.  
    """
    a, b, p = paravec4
    log_like = len(data) * sp.log(np.exp(a)) + (np.exp(a)*np.exp(p)-1) * sum(sp.log(data)) - (1/np.exp(b)**np.exp(a)) * sum(data**np.exp(a))\
    -len(data)*np.exp(a)*np.exp(p)*sp.log(np.exp(b)) - len(data) * gammaln(np.exp(p))
    return -log_like


def criterion_gamma_exp(paravec5, data):
    """
    transformed log-likelihood function for the gamma. 

    Parameters
    --------------------------------
    parave5: 
        numpy array: [log(b), log(p)]
        contains log(b) and log(p). Because of these transformations, 
        we exponentiate any term that contains b or p in the log-likehood function.
        we do this in order to constrain our parameter to positive terms.  
    """
    b, p = paravec5
    log_like = (np.exp(p)-1)*sum(np.log(data)) - (1/np.exp(b))*sum(data) - len(data)*gammaln(np.exp(p)) - len(data)*np.exp(p)*np.log(np.exp(b))
    return -log_like
    
def criterion_lognormal_exp(paravec, data):
    """
    transformed log-likelihood for the lognormal function

    Parameters: 
    ...............................
    paravec: 
        numpy array = [log(mu), log(sigma)]
        contains log(b) and log(p). Because of these transformations, 
        we exponentiate any term that contains b or p in the log-likehood function.
        we do this in order to constrain our parameter to positive terms. 
    returns: 
        log-likelihood value. 
    """
    mu= paravec[0]
    sigma = paravec[1]
    log_like = (-1/(2*np.exp(sigma)**2)) * sum((sp.log(data)-np.exp(mu))**2) - (len(data)/2) * sp.log(2*sp.pi) - len(data) * sp.log(np.exp(sigma)) - sum(sp.log(data))
    return -log_like


def transform(log_paravec):
    paravec = np.exp(log_paravec)
    return paravec


def gb2pdf(paravec, y = None): 
    a, b, p, q = paravec   
    pdf = abs(a) * y **(a*p - 1) / (b**(a*p) * beta(p, q) * (1 + (y/b)**a)**(p+q))
    return pdf 

def burr12pdf(paravec, y):
    a, b, q = paravec
    pdf = abs(a) * y **(a - 1) / (b**(a) * beta(1, q) * (1 + (y/b)**a)**(1+q))
    return pdf 

def burr3pdf(paravec,y):
    a, b, p = paravec
    pdf = abs(a) * y **(a*p - 1) / (b**(a*p) * beta(p, 1) * (1 + (y/b)**a)**(p+1))
    return pdf 

def ggammapdf(paravec, y):
    a, b, p = paravec
    pdf = abs(a) * y**(a*p - 1) * np.exp(-(y/b)**a)/(b**(a*p) * gamma(p))
    return pdf

def gammapdf(paravec, y):
    b, p = paravec
    pdf = y**(p - 1) * np.exp(-(y/b))/(b**(p) * gamma(p))
    return pdf

def lognormalpdf(paravec, y):
    mu, sigma = paravec
    pdf = np.exp(-(np.log(y) - mu)**2 / (2 * sigma**2)) / ((2 * np.pi)**(1/2) * sigma * y)
    return pdf
