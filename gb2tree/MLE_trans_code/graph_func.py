from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt


def single_graph(data, pdf, paravec, name=None, method=None, guess=None):
    """
    Plots the estimated pdf for the given distribution

    parameters 
    ------------------------------------
    data: 
        numpy vector whose distribution was estimated
    pdf
        pdf function we need the distribution of
    paravec
        parameters for the distribution function
    title:
        String
        title of the graph/usually the name of the chosen distribution
    """

    x_range = sp.linspace(data.min(), data.max(), 500)
    points = pdf(paravec, x_range)
    title = str()    
    plt.hist(data, bins = 100, normed = True)
    plt.plot(x_range, points, linewidth =2, label = guess)
    plt.title((name, method))
    plt.legend()


    