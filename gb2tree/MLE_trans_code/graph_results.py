#graph_results
#I just want to see pdf's for the various distributions, and ths page is where I plot them.
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import gb2_opt_trans as g
import burr3_opt_trans as b3
import burr12_opt_trans as b12
import lognormal_opt_trans as gg
import gamma_opt_trans as gm
import lognormal_opt_trans as ln
import graph_func as gf
import gb2tree_func_trans as gb2trans


sal = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\flexibledist\\income_data.csv", delimiter = ",")
sal = sal[sal>0] 
sal = sal[sal < 80000]
lognormal, lognormal_log = ln.lognormal_MLE(sal, method = 'Nelder-Mead')

gf = gf.single_graph(sal, gb2trans.lognormalpdf, lognormal, name = 'lognormal', method = 'Nelder')
plt.show()
