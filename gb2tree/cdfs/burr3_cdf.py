import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis



def burr3_cdf(paravec, x):
    a, b, p = paravec
    cdf = 1/(1 + (x/b)**-a)**p
    return cdf 
