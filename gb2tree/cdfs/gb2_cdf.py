from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis


def gb2_cdf(paravec, x):
    """
    Returns the cumulative distribution function evaluated at x
    for a gb2 distribution.

    Parameters
    ------------------------------
    paravec contains:
    a, b, p, q: 
        Distributional parameters for the GB2 distribution. These
        can be estimated for a given dataset using gb2_opt_trans
        program found in C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\flexibledist\\MLE_trans_code

        Special cases of the gb2 cdf are the burr12 cdf (p=1) and burr3 (q=1).
        burr12 parameters (a, b, q) can be estimated using burr12_opt_trans in the above folder
        burr3 parameters (a, b, p) can be estimated using burr3_opt_trans in the above folder
    x: 
        vector or a single number containing where we would like to evaluate our cdf at. 
   

    Returns
    ------------------------------
    prob:
        The probability that a random variable will be less than or equal to x.        
    """
    a, b, p, q = paravec
    z = (x/b)**a / (1 + (x/b)**a)
    cdf = betainc(p, q, z)
    return cdf


