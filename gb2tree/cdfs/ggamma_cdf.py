from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis

def ggamma_cdf(paravec, x):
    """
    Returns the cumulative distribution function evaluated at x
    for a generalized gamma distribution.

    Parameters
    ------------------------
   paravec contains:
   a, b, p: 
        Distributional parameters for the generalized gamma distribution. These
        can be estimated for a given dataset using ggamma_opt_trans
        program found in C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\flexibledist\\MLE_trans_code

        Special cases of the ggamma cdf are the gamma cdf (a=1) and the lognormal?? but that has 
        its own cdf program.
        gamma distributional parameters can be estimated using gamma_opt_trans in the above folder
    x: 
        vector or a single number containing where we would like to evaluate our cdf at. 

    

    Returns
    ------------------------------
    prob:
        The probability that a random variable will be less than or equal to x, given the 
        specified distribution.        
    """

    a, b, p = paravec
    z = (x/b)**a
    cdf = gammainc(p, z)
    return cdf





