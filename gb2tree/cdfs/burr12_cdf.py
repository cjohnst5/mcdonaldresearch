import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis

def burr12_cdf(paravec, x):
    """
    Closed form of the burr 12 cdf    
    """
    a, b, q = paravec
    cdf = 1 - (1/(1+(x/b)**a)**q)
    return cdf