#using estimated parameters to see if my cdf's are looking good
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis
import gb2_cdf as g_cdf
import burr3_cdf as b3_cdf
import burr12_cdf as b12_cdf
import ggamma_cdf as gg_cdf
import gamma_cdf as gm_cdf
import lognormal_cdf as ln_cdf
from scipy.stats import pareto

import sys
sys.path.insert(0,'C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\MLE_trans_code')
import gb2tree_func_trans as gb2trans
import gb2_opt_trans as g
import burr12_opt_trans as b12
import burr3_opt_trans as b3
import ggamma_opt_trans as gg
import gamma_opt_trans as gm
import lognormal_opt_trans as ln 

sys.path.insert(0,'C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\gb2tree_class')
import gb2library as lib




def plot_cdf(data, cdf, name):
    x_range = np.linspace(data.min(), data.max(), 500)
    cdf_points = cdf(x_range)
    plt.plot(x_range, cdf_points)
    plt.title(name)
    return cdf_points, x_range


sal = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\income_data.csv", delimiter = ",")
sal = sal[sal>0] 
sal = sal[sal < 80000]

lognormalparavec, lognormalpara_log = ln.lognormal_MLE(sal)
burr3_paravec, burr3_paravec_log = b3.burr3_MLE(sal)
burr12_paravec, burr12_paravec_log = b12.burr12_MLE(sal)
ggamma_paravec, ggamma_paravec_log = gg.ggamma_MLE(sal)
gamma_paravec, gamma_paravec_log = gm.gamma_MLE(sal)
gb2_paravec, gb2_paravec_log = g.gb2_MLE(sal)
x_range = np.linspace(sal.min(), sal.max(), 500)




#testing the library. 
    # # distributionlist = [lib.Gb2(a1, b1, p1, q1), lib.B2(b1, p1, q1), lib.Br12(a3, b3, q3), lib.Br3(a2, b2, p2), 
    # #                     lib.Gg(a4, b4, p4), lib.Ga(b5, p5), lib.W(a4, b4), lib.Lom(b3, 10), lib.InvLom(b3, q3),
    # #                     lib.Fisk(a2, b2), lib.Exp(b5), lib.Chi2(p5), lib.LogLog(b2)]

    # # #checking the methods for my distribution list:
    # # n = len(distributionlist)
    # # mean_vec = np.zeros(n)
    # # std_vec = np.zeros(n)
    # # skew_vec = []
    # # kurt_vec = []
    # # pdf_all = []
    # # cdf_all = []
    # # for i in xrange(n):
    # #     mean_vec[i] = distributionlist[i].mean()
    # # for i in xrange(n):
    # #     std_vec[i] = distributionlist[i].std()
    # # for i in xrange(n):
    # #     # skew_vec[i] = distributionlist[i].skew()
    # #     skew_vec.append(distributionlist[i].skew())
    # # for i in xrange(n):
    # #     # kurt_vec[i] = distributionlist[i].kurt()
    # #     kurt_vec.append(distributionlist[i].kurt())

    # # "Plotting my pdfs"
    # # for i in xrange(n):
    # #     plot_cdf(sal, distributionlist[i].pdf, str(distributionlist[i]))
    # #     plt.show()
    # # "Plotting my cdfs"
    # # for i in xrange(n):
    # #     plot_cdf(sal, distributionlist[i].cdf, str(distributionlist[i]))
    # #     plt.show()


mu, sig = lognormalparavec
# a, b, p = burr3_paravec
##testing out my gb2. 
distLn = Ln(mu, sig)
m = distLn.mean()
se = distLn.std()
sk = distLn.skew()
k = distLn.kurt()

#pdf and cdf stuff looks good
x_range = np.linspace(0, 5, 200)
plt.plot(x_range, distLn.pdf(x_range))
plt.title('Ln Pdf')
plt.show()
plt.plot(x_range, distLn.cdf(x_range) )
plt.title('Ln Cdf')
plt.show()

plot_cdf(sal, distLn.pdf, 'Ln pdf')
plt.show()
plot_cdf(sal, distLn.cdf, 'Ln cdf')
plt.show()

#loglikelihood, big test...and it works!
cri_Ln = lambda x: distLn.loglike(sal, x, sign = -1)
guess = [37000, 3]
method = 'Powell'
tolerance = 1e-5
options = ({'maxiter': 50000, 'maxfev' : 50000})
Gb1_log = opt.minimize(cri_Ln, np.log(guess), method = method, tol = tolerance, options = options)
Gb1_result = np.exp(Gb1_log.x)

# #checking out my pareto. 