from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
from scipy.stats import skew, kurtosis, norm

import sys
sys.path.insert(0,'C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\gb2tree\\MLE_trans_code')
import lognormal_opt_trans as ln
import salary as sl
reload(sl)
reload(ln)


def lognormal_cdf(paravec, x):
    """
    Returns the cumulative distribution function evaluated at x
    for a lognormal distribution.

    Parameters
    ------------------------
    paravec contains:
    mu, sigma: 
        Distributional parameters for the generalized gamma distribution. These
        can be estimated for a given dataset using lognormal_opt_trans
        program found in C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\flexibledist\\MLE_trans_code

    x: 
        vector or a single number containing where we would like to evaluate our cdf at. 

   

    Returns
    ------------------------------
    prob:
        The probability that a random variable will be less than or equal to x, given the 
        specified distribution.        
    """

    mu, sigma = paravec
    z = (np.log(x) - mu)/sigma
    prob = norm.cdf(z)
    return prob

# x_range = np.linspace(sl.sal.min(), sl.sal.max(), 500)
# cdf_points = lognormal_cdf(ln.lognormal, x_range)

# figure = plt.figure()
# plt.plot(x_range, cdf_points)
# plt.title('Log normal')
# plt.show()
