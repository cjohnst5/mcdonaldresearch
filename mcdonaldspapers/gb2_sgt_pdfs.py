from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import beta, gamma, betaln, gammaln, betainc, gammainc, erf
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy import optimize as opt 





#importing data and parameters we need
data = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\carson_data_all.csv', delimiter = ',', skiprows=1)
parameters_gb2 = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\Fit_Carson_GB2.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))
parameters_sgt = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\Fit_Carson_SGT.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))

#adding a column of ones to the xs
obs = data.shape[0]
xs = np.concatenate((np.ones((obs,1)), data[:,3:]), axis = 1)
y = data[:,2]

#Getting the mean x's
mean_xs = xs.mean(axis=0)

#Getting the betas
num_betas = 13
betas_lnorm = parameters_gb2[0,0:13]
betas_weibull = parameters_gb2[1,0:13]
betas_gg = parameters_gb2[2,0:13]
betas_br12 = parameters_gb2[3,0:13]
betas_br3 = parameters_gb2[4,0:13]
betas_gb2 = parameters_gb2[5,0:13]

betas_normal = parameters_sgt[0,0:13]
betas_sged = parameters_sgt[1,0:13]
betas_sgt = parameters_sgt[2,0:13]

#getting our parameters
num_dist = 6
sig_lnorm = parameters_gb2[0,13]
sig_weibull = parameters_gb2[1,13]
sig_gg, p_gg = parameters_gb2[2,13:15]
sig_br12, q_br12 = parameters_gb2[3,13:15]
sig_br3, p_br3 = parameters_gb2[4,13:15]
sig_gb2, p_gb2, q_gb2 = parameters_gb2[5,13:16]

sig_norm = parameters_sgt[0,13]
sig_sged, p_sged, lamb_sged = parameters_sgt[1,13:16]
sig_sgt, p_sgt, q_sgt, lamb_sgt = parameters_sgt[2,13:17]


#getting our xbeta stuff
xbar_beta = np.dot(mean_xs, betas_gb2)
x_beta_mean = np.dot(xs, betas_gb2).mean()

y_range = sp.linspace(.1, 300, 100)


def gb2_pdf(y, delta, sigma, p, q):
    """
    This function takes y and xb and plots the gb2 distribution for the data's errors
    -------------------------Parameters-------------------------------------
    y = dependent variable
    delta = x variables dotted with betas
    sigma, p and q = parameters for the gb2
    --------------------------------------------------
    Returns pdf points for the specified data. 
    """
    bta = np.exp(gammaln(p) + gammaln(q) - gammaln(p+q))
    # bta = beta(p,q)
    pdf = np.exp((np.log(y) - delta)*(p/sigma -1))/ \
    (sigma*np.exp(delta)*bta*(1 + np.exp((np.log(y)-delta)/sigma))**(p+q))
    return pdf

def gg_pdf(y, delta, sigma, p):
    numerator_1 = np.exp(p*(np.log(y)-delta)/sigma)
    numerator_2a = -np.exp((np.log(y)-delta)/sigma)
    numerator_2 = np.exp(numerator_2a)
    pdf =  numerator_1*numerator_2/(sigma*y*gamma(p))
    return pdf

def sged_pdf(y, mu, sigma, p, lamb):
    pdf = p*np.exp(-(abs(y-mu)/((1+lamb*np.sign(y-mu))*sigma))**p)/ (2*sigma*gamma(1/p))
    return pdf

def sgt_pdf(y, mu, sigma, p, q, lamb):
    bta = np.exp(gammaln(1/p) + gammaln(q) - gammaln(1/p+q))
    big_stuff = (1 + abs(y-mu)**p/(q*sigma**p*(1+lamb*np.sign(y-mu))**p))**(q+1/p)
    pdf = p/(2*sigma*q**(1/p)*bta*big_stuff)
    return pdf 

#Plotting the GB2
gb2_points = gb2_pdf(y_range, x_beta_mean, sig_gb2, p_gb2, q_gb2)
plt.plot(y_range, gb2_points)
plt.title('GB2')
plt.show()

#plotting the br3 
# xbar_beta = np.dot(mean_xs, betas_br3)
# br3_points = gb2_pdf(y_range, delta, sig_br3, p_br3, 1)
# plt.plot(y_range, br3_points)
# plt.show()

#Plotting the gg
xbar_beta = np.dot(mean_xs, betas_gg)
gg_points = gg_pdf(y_range, xbar_beta, sig_gg, p_gg)
plt.plot(y_range, gg_points)
plt.title('gg')
plt.show()

#Plotting the Gamma

#Plotting the Weibull
xbar_beta = np.dot(mean_xs, betas_weibull)
weibull_points = gg_pdf(y_range, xbar_beta, sig_weibull, 1)
plt.plot(y_range, weibull_points)
plt.title('Weibull')
plt.show()

#overlaying all the distributions
plt.plot(y_range, gb2_points, 'b', label='GB2')
plt.plot(y_range, gg_points, 'r', label='GG2')
plt.plot(y_range, weibull_points, 'b', label='weibull')
plt.legend()
plt.title('GB2 tree distributions')
plt.show()


#Since the GB2 looks so weird, we are going to move onto the normal
xbar_beta = np.dot(mean_xs, betas_normal)
normal_points = sged_pdf(y_range, xbar_beta, sig_norm, 2, 0)
plt.plot(y_range, normal_points)
plt.title('Normal')
plt.show()

#now trying the SGED: 
xbar_beta = np.dot(mean_xs, betas_sged)
sged_points = sged_pdf(y_range, xbar_beta, sig_sged, p_sged, lamb_sged)
plt.plot(y_range, sged_points)
plt.title('SGED')
plt.show()

#Now trying the SGT
xbar_beta = np.dot(mean_xs, betas_sgt)
sgt_points = sgt_pdf(y_range, xbar_beta, sig_sgt, p_sgt, q_sgt, lamb_sgt)
plt.plot(y_range, sgt_points)
plt.title('SGT')
plt.show()

#Plotting the SGT pdfs on top of each other
plt.plot(y_range, normal_points, color = 'r', label = 'Normal')
plt.plot(y_range, sged_points, color = 'black', label = 'SGED')
plt.plot(y_range, sgt_points, color ='g', label = 'SGT')
plt.legend()
plt.title('SGT and GB2 distributions')
plt.show()


#Plotting everything on top of each other.
plt.plot(y_range, gb2_points, color = 'b', label = 'P and q')
plt.plot(y_range, normal_points, color = 'r', label = 'Normal')
plt.plot(y_range, sged_points, color = 'black', label = 'SGED')
plt.plot(y_range, sgt_points, color ='g', label = 'SGT')
plt.legend()
plt.title('SGT distributions')
plt.show()

#Seperately, the GB2 tree distributions and SGT tree distributions look plausible, 
#but overlaid together, they look nothing alike. 