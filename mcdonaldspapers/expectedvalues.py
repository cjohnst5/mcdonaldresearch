##Finding expected values for McDonald
##May 6, 2014

import numpy as np
import scipy as sp
from scipy.special import betaln, gammaln, beta, gamma, gammainc, betainc, erf 

#-------------------------------------------------------Collecting our data, betas, and distributional parameters---------------------

#importing data and para_gb2 we need
#Directory at work
# data = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\carsondata.csv', delimiter = ',', skiprows=1)
# para_gb2 = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\Fit_Carson_GB2.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))
# para_sgt = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\mcdonaldspapers\Fit_Carson_SGT.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))

#Directory on Laptop
data = sp.loadtxt('C:\Users\Carla\Documents\McDonald\mcdonaldresearch\mcdonaldspapers\carsondata.csv', delimiter = ',', skiprows=1)
para_gb2 = sp.loadtxt('C:\Users\Carla\Documents\McDonald\mcdonaldresearch\mcdonaldspapers\Fit_Carson_GB2.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))
para_sgt = sp.loadtxt('C:\Users\Carla\Documents\McDonald\mcdonaldresearch\mcdonaldspapers\Fit_Carson_SGT.csv', delimiter = ',', skiprows=1, usecols=np.arange(3,26))


#adding a column of ones to the xs
obs = data.shape[0]
data = np.concatenate((np.ones((obs,1)), data), axis = 1)

#Getting the mean x's
mean_xs = data.mean(axis=0)

#Getting the betas
num_betas = 13
betas_lnorm = para_gb2[0,0:13]
betas_weibull = para_gb2[1,0:13]
betas_gg = para_gb2[2,0:13]
betas_br12 = para_gb2[3,0:13]
betas_br3 = para_gb2[4,0:13]
betas_gb2 = para_gb2[5,0:13]

betas_norm = para_sgt[0,0:13]
betas_sged = para_sgt[1,0:13]
betas_sgt = para_sgt[2,0:13]


#getting our distributional parameters
num_dist = 6
sig_lnorm = para_gb2[0,13]
sig_weibull = para_gb2[1,13]
sig_gg, p_gg = para_gb2[2,13:15]
sig_br12, q_br12 = para_gb2[3,13:15]
sig_br3, p_br3 = para_gb2[4,13:15]
sig_gb2, p_gb2, q_gb2 = para_gb2[5,13:16]

sig_norm = para_sgt[0,13]
sig_sged, p_sged, lamb_sged = para_sgt[1,13:16]
sig_sgt, p_sgt, q_sgt, lamb_sgt = para_sgt[2,13:17]



#-----------------------------------------------------Functions--------------------------------------------------

# defining our expected values for the gb2 (along with the br12 and br3)
def ev_gb2(betas, xs, dist_param):
    p, q, sig = dist_param
    xb = np.dot(xs, betas)
    lng = gammaln(p+sig) + gammaln(q-sig) - gammaln(p) - gammaln(q)
    exp_val = np.exp(xb)*np.exp(lng)
    # exp_val = np.exp(xb)*gamma(p+sig)*gamma(q-sig)/(gamma(p)*gamma(q))
    return exp_val

#defining our expected values for gg (and the weibull)
def ev_gg(betas, xs, dist_param):
    sig, p = dist_param
    xb =  np.dot(xs, betas)
    lng = gammaln(p + sig) - gammaln(p)
    exp_val = np.exp(xb) * np.exp(lng)    
    return exp_val

#defining our expected value for the log normal
def ev_lnorm(betas, xs, sig):
    xb = np.dot(xs, betas)
    exp_val = np.exp(xb + (1/sig)**2/2)
    return exp_val

def ev_sgt(betas, xs, dist_param):
    sig, p, q, lamb = dist_param
    xb = np.dot(xs, betas)
    lnbeta_num = gammaln(2/p) + gammaln(q-1/p) - gammaln(2/p+(q-1/p))
    lnbeta_denom = gammaln(1/p) + gammaln(q) - gammaln(1/p+q)
    exp_val = xb + 2*lamb*sig*np.exp(1/p*np.log(q) + lnbeta_num - lnbeta_denom)
    return exp_val

def ev_sged(betas, xs, dist_param):
    sig, p, lamb = dist_param
    xb = np.dot(xs, betas)
    exp_val = xb + 2*lamb*sig*np.exp(gammaln(2/p)-gammaln(1/p))
    return exp_val

#----------------------------------------------------------------------------------------------------------------
#------------------------------------------Xbar betas for all the distributions: --------------------------------
xbar_beta_gb2 = np.dot(mean_xs, betas_gb2)
xbar_beta_br3 = np.dot(mean_xs, betas_br3)
xbar_beta_br12 = np.dot(mean_xs, betas_br12)
xbar_beta_gg = np.dot(mean_xs, betas_gg)
xbar_beta_weibull = np.dot(mean_xs, betas_weibull)
xbar_beta_lnorm = np.dot(mean_xs, betas_lnorm)

xbar_beta_norm = np.dot(mean_xs, betas_norm)
xbar_beta_sged = np.dot(mean_xs, betas_sged)
xbar_beta_sgt = np.dot(mean_xs, betas_sgt)

xbar_beta_array = [xbar_beta_gb2, xbar_beta_br3, xbar_beta_br12, xbar_beta_weibull, xbar_beta_lnorm,\
                            xbar_beta_sgt, xbar_beta_sged, xbar_beta_norm]
print "Xbar Betas"
print xbar_beta_array

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------Expected value of the mean x's------------------------------------------
param_gb2 = [sig_gb2, p_gb2, q_gb2]
exp_val_gb2 = ev_gb2(betas_gb2, mean_xs, param_gb2)

param_br3 = [sig_br3, p_br3, 1]
exp_val_br3 = ev_gb2(betas_br3, mean_xs, param_br3)

param_br12 = [sig_br12, 1, q_br12]
exp_val_br12 = ev_gb2(betas_br12, mean_xs, param_br12)

param_gg = [sig_gg, p_gg]
exp_val_gg = ev_gg(betas_gg, mean_xs, param_gg)

param_weibull = [sig_weibull, 1]
exp_val_weibull = ev_gg(betas_weibull, mean_xs, param_weibull)

exp_val_lnorm = ev_lnorm(betas_lnorm, mean_xs, sig_lnorm)



param_sgt = [sig_sgt, p_sgt, q_sgt, lamb_sgt]
exp_val_sgt = ev_sgt(betas_sgt, mean_xs, param_sgt)

param_sged = [sig_sged, p_sged, lamb_sged]
exp_val_sged = ev_sged(betas_sged, mean_xs, param_sged)

param_norm = [sig_sged, 2, 0]
exp_val_norm = ev_sged(betas_norm, mean_xs, param_norm)


exp_val_array = [exp_val_gb2, exp_val_br3, exp_val_br12, exp_val_weibull, exp_val_lnorm, exp_val_sgt, exp_val_sged, exp_val_norm]

print "Expected values of the mean x's for lognormal"
print exp_val_array
# print exp_val_lnorm

#---------------------------------------------------------------------------------------------------------------------
#------------------------------------------Average of the expected values---------------------------------------------
# result_gb2 = ev_gb2(betas_gb2, data, param_gb2)

# result_br3 = ev_gb2(betas_br3, data, param_br3)


# result_br12 = ev_gb2(betas_br12, data, param_br12)


# result_gg = ev_gg(betas_gg, data, param_gg)


# result_weibull = ev_gg(betas_weibull, data, param_weibull)

# result_lnorm = ev_lnorm(betas_lnorm, data, sig_lnorm)


# average_vector = [result_gb2.mean(), result_br3.mean(), result_br12.mean(), result_gg.mean(), result_weibull.mean(), result_lnorm.mean()]
# print "Average expected values across observations", result_lnorm.mean()
print " "



#.......................May 12, 2014, getting some means for Professor McDonald.........

practice_xb = np.dot(data, betas_lnorm).mean()
mean1_xb = [np.exp(np.dot(data, betas_lnorm)).mean(), np.exp(np.dot(data, betas_weibull)).mean(), np.exp(np.dot(data, betas_gg)).mean(), np.exp(np.dot(data, betas_br3)).mean(), \
                    np.exp(np.dot(data, betas_br12)).mean(), np.exp(np.dot(data, betas_gb2)).mean()]

mean2_xb = [np.exp(np.dot(mean_xs, betas_lnorm)).mean(), np.exp(np.dot(mean_xs, betas_weibull)).mean(), np.exp(np.dot(mean_xs, betas_gg)).mean(), np.exp(np.dot(mean_xs, betas_br3)).mean(), \
                    np.exp(np.dot(mean_xs, betas_br12)).mean(), np.exp(np.dot(mean_xs, betas_gb2)).mean()]

#Getting our random samples.............................................................
sample_num = obs/10
sample_xb = []
betas_array = [betas_lnorm, betas_weibull, betas_gg, betas_br3, betas_br12, betas_gb2]
for i in xrange(num_dist):     
    reshape_xb = np.exp(np.dot(data, betas_array[i])).reshape(obs)    
    np.random.shuffle(reshape_xb)
    sample_xb.append(reshape_xb[0:sample_num].mean())


#Putting our lists in one array
info_xb = np.array([mean1_xb, mean2_xb, sample_xb])

np.savetxt("info1_xb.csv", info_xb, delimiter=",")


#........................Looking at the medians...............

def weibull_median(b, percentile, sig):
    median = b*(-np.log(1-percentile))**(sig)
    return median
def br3_median(b, percentile, sig, p):
    median = b*(percentile**(-1/p)-1)**(-sig)
    return median 
def br12_median(b, percentile, sig, q):
    median = b*(1/(1-percentile)**(1/q) - 1)**(sig)
    return median

practice_xb = np.dot(data, betas_weibull).mean()

mean1_w = weibull_median(mean2_xb[1], .5, sig_weibull)
mean2_w = weibull_median(mean1_xb[1], .5, sig_weibull)
mean1_br3 = br3_median(mean2_xb[3], .5, sig_br3, p_br3)
mean2_br3 = br3_median(mean1_xb[3], .5, sig_br3, p_br3)
mean1_br12 = br12_median(mean2_xb[4], .5, sig_br12, q_br12)
mean2_br12 = br12_median(mean1_xb[4], .5, sig_br12, q_br12)

median_array = [0, mean1_br3, mean1_br12, mean1_w, 0, 0, 0, 0]
median_array_xbar = [0, mean2_br3, mean2_br12, mean2_w, 0, 0, 0, 0]

print "Median WTP for exp(xbar beta) x's. Weibull", mean1_w
print "Median of WTP for mean exp(x beta). Weibull", mean2_w
print "Median of WTP for exp(xbar beta) x's. Br3", mean1_br3
print "Median of WTP for mean exp(x beta). Br3", mean2_br3
print "Median of WTP for exp(xbar beta) x's. Br12", mean1_br12
print "Median of WTP for mean exp(x beta). Br12", mean2_br12
    

#putting all our info in one matrix so we can save it to excel: 
all_results =  np.array([xbar_beta_array, exp_val_array, median_array, median_array_xbar])
np.savetxt('computer_work_on_cv.csv', all_results, delimiter = ',')