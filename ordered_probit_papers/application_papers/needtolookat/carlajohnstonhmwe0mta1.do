*/************************************************/
*/*   Please scroll down to the INFIX step for    */
*/* instructions related to providing full path  */
*/* name for the input data file.                */
*/************************************************/
*/ then scoll to the end of the file and provide the */
*/ path for where to store the data when finished */

#delimit ;
clear ;

label define FM4X
-1  "Not in Universe"
1  "Married - Spouse Present"
2  "Married-Spouse Absent"
3  "Widowed"
4  "Divorced"
5  "Separated"
6  "Never Married"
;
label define FM5X
1  "Male"
2  "Female"
;
label define FM7X
-1  "Not in Universe"
1  "Employed-At Work"
2  "Employed-Absent"
3  "Unemployed-On Layoff"
4  "Unemployed-Looking"
5  "Retired-Not In Labor Force"
6  "Disabled-Not In Labor Force"
7  "Other-Not In Labor Force"
;
label define FM8X
-1  "Not in Universe"
;
label define FM9X
-1  "Not in Universe"
1  "Graduation from High School"
2  "GED or  other Equivalent"
;
label define FM10X
-1  "Not in Universe"
1  "Less than 1st grade"
2  "1st, 2nd, 3rd, or 4th Grade"
3  "5th or 6th Grade"
4  "7th or 8th Grade"
5  "9th Grade"
6  "10th Grade"
7  "11th Grade"
8  "12th Grade, NO DIPLOMA"
;
label define FM11X
-1  "Not in Universe"
1  "Less than 1 year (includes 0 years completed)?"
2  "The first, or Freshman year?"
3  "The second, or Sophomore year?"
4  "The third, or Junior year?"
5  "Four or more years?"
;
label define FM12X
-1  "Not in Universe"
1  "Yes"
2  "No"
;
label define FM13X
-1  "Not in Universe"
1  "1 year program"
2  "2 year program"
3  "3 year program (or longer)"
;
label define FM14X
1  "White Only"
2  "Black Only"
3  "American Indian, Alaskan Native Only"
4  "Asian Only"
5  "Hawaiian/Pacific Islander Only"
6  "White-Black"
7  "White-AI"
8  "White-Asian"
9  "White-Hawaiian"
10  "Black-AI"
11  "Black-Asian"
12  "Black-HP"
13  "AI-Asian"
14  "Asian-HP"
15  "W-B-AI"
16  "W-B-A"
17  "W-AI-A"
18  "W-A-HP"
19  "W-B-AI-A"
20  "2 or 3 Races"
21  "4 or 5 Races"
;
/************************************************/
/*    You will need to change the "using"   */
/* statement at the end of the "infix" command  */
/* statement to include the full directory path */
/* of the directory where you saved the ASCII   */
/* data file.  For example:                     */
/* infix...                                     */
/*  using "C:\My Documents\bearhardlo5u2ic1.asc";*/
/********************************************************/

infix
double HRHHID 1 - 16
HRHHID2 17 - 32
OCCURNUM 33 - 41
YYYYMM 42 - 50
PEMARITL 51 - 61
PESEX 62 - 72
PRTAGE 73 - 83
PEMLR 84 - 94
PTERNH1O 95 - 103
PEDIPGED 104 - 114
PEHGCOMP 115 - 125
PECYC 126 - 136
PEGR6COR 137 - 147
PEMS123 148 - 158
PTDTRACE 159 - 169

using "C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\ordered_probit_papers\application_papers\needtolookat"
;

label variable HRHHID "HRHHID";
label variable HRHHID2 "HRHHID2";
label variable OCCURNUM " ";
label variable YYYYMM " ";
label variable PEMARITL "Demographics-marital status";
label variable PESEX "Demographics-sex";
label variable PRTAGE "Demographics - age topcoded at 85, 90 or 80 (see full description)";
label variable PEMLR "Labor Force-employment status";
label variable PTERNH1O "Earnings-hourly pay rate,amount";
label variable PEDIPGED "Demographics-high school,graduation/GED";
label variable PEHGCOMP "Demographics-highest grade completed before GED";
label variable PECYC "Demographics-years of college credit completed";
label variable PEGR6COR "Demographics-completed 6 or more graduate courses,y/n";
label variable PEMS123 "Demographics-Master''s program 1, 2, or 3 years";
label variable PTDTRACE "Demographics- race of respondent";
label values PEMARITL FM4X;
label values PESEX FM5X;
label values PEMLR FM7X;
label values PTERNH1O FM8X;
label values PEDIPGED FM9X;
label values PEHGCOMP FM10X;
label values PECYC FM11X;
label values PEGR6COR FM12X;
label values PEMS123 FM13X;
label values PTDTRACE FM14X;

save "c:\documents and setting\bearhardlo5u2ic1",
replace;

describe;

