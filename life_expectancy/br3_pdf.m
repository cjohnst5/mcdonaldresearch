function [ pdf ] = br3_pdf( paravec, x )
%%Calculates the pdf values of a br3 pdf, given parameters and random
%variable values
%   ----------------------Inputs------------------
% paravec: Vector that contains [a, b, p]

paravec_gb2 = [paravec, 1];
pdf = gb2_pdf(paravec_gb2, x);


end

