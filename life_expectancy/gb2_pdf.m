function [ pdf ] = gb2_pdf( paravec, x )
%Returns pdf points given distributional parameters and a range of the
%random variable. 
%   ----------------------Inputs----------------------------
% paravec: parameter vector that contains [a, b, p,q], in that order
% x: points that we would like to calculate the pdf of. 

%   ----------------------Returns------------------------
% pdf: the pdf evaluated at that point. 

a = paravec(1);
b = paravec(2);
p = paravec(3);
q = paravec(4);

lnbta = gammaln(p) + gammaln(q) - gammaln(p+q);
pdf = exp(log(abs(a)) + (a*p - 1)*log(x)- (a*p)*log(b)...
             - lnbta - (p+q)*log((1 + (x/b).^a)));
        

end

