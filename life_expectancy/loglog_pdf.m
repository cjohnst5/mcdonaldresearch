function [ pdf ] = loglog_pdf( paravec, x )
%Calculates the pdf values of a loglog pdf, given parameters and random
%variable values
%-------------------Inputs------------------
% paravec: Could be a scalar or a vector of one element. Contains the 
% parameter b. 
% x: points that we would like to calculate the pdf of. 



paravec_gb2 = [1, paravec, 1, 1];
pdf = gb2_pdf(paravec_gb2, x);
end

