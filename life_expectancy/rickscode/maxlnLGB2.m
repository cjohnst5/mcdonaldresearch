function minfunc = maxlnLGB2(parvec,clms)
%-------------------------------------------------------------------------%
% Calculate the log likelihood function for given parameters of the
% generalized beta 2 (GB2) distribution and minimize its negative (maximize
% it)
%
% f(x;a,b,p,q)=(a/((b^(a*p))*Beta(p,q)*((1+(x/b)^a)^(p+q)))*x^(a*p-1)
%-------------------------------------------------------------------------%
% parvec     = 1 x 4 vector, values of a, b, p, and q from generalized beta
%              2 (GB2) distribution
% clms     = clmsize x 1 vector, monthly expenditure values
% aa         = scalar, value of a in expenditure process
% bb         = scalar, value of b in expenditure process
% pp         = scalar, value of p in expenditure process
% qq         = scalar, value of q in expenditure process
% gprobs     = clmsize x 1 vector, GB2 distributed probabilities of
%              expenditures
% lngprobs   = clmsize x 1 vector, log of gprobs
% lnL        = scalar, log-likelihood function value
% neglnL     = scalar, negative log-likelihood function value
% minfunc    = scalar, criterion to be minimized (max log-likelihood)
%-------------------------------------------------------------------------%
aa = parvec(1) ;
bb = parvec(2) ;
pp = parvec(3) ;
qq = parvec(4) ;

gprobs = (aa./((bb^(aa*pp))*beta(pp,qq)*(ones(size(clms)) + ...
         (clms./bb).^aa).^(pp+qq))).*(clms.^(aa*pp-1)) ;
gprobs(gprobs<10^(-15)) = 10^(-15) ;
lngprobs = log(gprobs) ;
lnL = sum(lngprobs) ;
neglnL = -lnL ;

minfunc = neglnL ;