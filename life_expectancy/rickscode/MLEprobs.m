%-------------------------------------------------------------------------%
% Solves the five exercises in the MLE section of the textbook
%
% This function calls the following text file and functions:
%    * clms.txt:    tab-delimited data file. Total monthly health
%                   expenditures for 10,619 individuals
%    * maxlnLGA.m:  m-file that uses MLE to fit the gamma (GA) distribution
%                   to the observations on health care expenditures
%    * maxlnLGG.m:  m-file that uses MLE to fit the generalized gamma (GG)
%                   distribution to the observations on health care
%                   expenditures
%    * maxlnLGB2.m: m-file that uses MLE to fit the generalized beta 2
%                   (GB2) distribution to the observations on health care
%                   expenditures
%-------------------------------------------------------------------------%
clear ;
% cd('/Users/rwe2/Documents/BYU Economics/Teaching/BYU-MCL Econ 413R/DistFit/Matlab/MLEprobs') ;
starttime = clock ; % start timer

%-------------------------------------------------------------------------%
% Load in data
%-------------------------------------------------------------------------%
% clms = obs x 1 vector, monthly total health expend's by individual i
% obs  = scalar, number of observations
%-------------------------------------------------------------------------%
load('clms.txt') ;
obs = max(size(clms)) ;

%-------------------------------------------------------------------------%
% Calculate mean, median, max, min, and standard deviation
%-------------------------------------------------------------------------%
% mean = scalar, average monthly health expenditures
% med = scalar, median monthly health expenditures
% max = scalar, maximum monthly health expenditures
% min = scalar, minimum monthly health expenditures
% stdev = scalar, standard deviation of monthly health expenditures
%-------------------------------------------------------------------------%
mean = mean(clms) ;
med = median(clms) ;
max = max(clms) ;
min = min(clms) ;
stdev = std(clms) ;

%-------------------------------------------------------------------------%
% Plot two histograms of empirical moments of the U.S. income distribution
%-------------------------------------------------------------------------%
% bins1  = scalar, number of bins for histogram in Figure 1
% freq1  = 1 x bins1 vector, count of observations in each histogram bin
% cents1 = 1 x bins1 vector, center values for each bin in histogram
% bins   = scalar, number of bins for all remaining histograms
% freq2  = 1 x bins vector, count of observations in each histogram bin
% cents2 = 1 x bins vector, center values for each bin in histogram
%-------------------------------------------------------------------------%
bins1 = 1000 ;
[freq1,cents1] = hist(clms,1000) ;
figure(1)
f1 = bar(cents1,freq1./obs,1,'r') ;
title('Histogram of Monthly Health Expenditure','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)

bins = 100 ;
maxplotsup = 800 ;
[freq2,cents2] = hist(clms(clms<=maxplotsup),bins) ;
figure(2)
f2 = bar(cents2,freq2./obs,1,'r') ;
title('Histogram of Monthly Health Expenditures (<=$800)','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)

%-------------------------------------------------------------------------%
% Estimate gamma (GA) distribution parameters using expenditure values less
% than $700 and plot estimated distribution of expenditures histogram
%
% f(x;alp,bet) = (1/((bet^alp)*Gamma(alp)))*(x^(alp-1))*exp(-(x/bet))
%-------------------------------------------------------------------------%
% betGAinit  = scalar, initial guess for beta
% alpGAinit  = scalar, initial guess for alpha
% GAinit     = 1 x 2 vector, initial guesses for alpha and beta
% maxsupGA   = scalar, maximum value in expenditures support to use in
%              estimation
% minvecGA   = 1 x 2 vector of minimum values for alpha and beta parameters
% optionsfmc = structural array options for fmincon
% parGA      = 1 x 2 vector, estimated values for alpha and beta from gamma
%              (GA) distribution
% negLGA     = scalar, negative of the maximized log likelihood function
% maxLGA     = scalar, maximized log likelihood function value
% alphaGA    = scalar, estimated value for alpha in gamma (GA) dist.
% betaGA     = scalar, estimated value for beta in gamma (GA) dist.
% gamhistGA  = 1 x bins vector, histogram percentage values from estimated
%              gamma (GA) distribution pdf
% bind       = integer, index of histogram bin number
%-------------------------------------------------------------------------%
betGAinit = var(clms)/mean ;
alpGAinit = mean/betGAinit ;
GAinit = [alpGAinit betGAinit] ;
maxsupGA = 300000 ;
minvecGA = [10^(-10) 10^(-10)] ;
optionsfmc = optimset('Display','off','MaxFunEvals',100000,'MaxIter',...
                      10000,'TolFun',1e-15,'Algorithm','interior-point') ;
[parGA,negLGA] = fmincon(@maxlnLGA,GAinit,[],[],[],[],minvecGA,[],[],...
                       optionsfmc,clms,maxsupGA) ;

maxLGA = -negLGA ;
alphaGA = parGA(1) ;
betaGA = parGA(2) ;
display([alphaGA betaGA]) ;
display(maxLGA) ;
gamhistGA = zeros(1,bins) ;
gamhistGA(1) = gamcdf(0.5*(cents2(1)+cents2(2)),alphaGA,betaGA) ;
for bind = 2:bins-1
    gamhistGA(bind) = ...
        gamcdf(0.5*(cents2(bind)+cents2(bind+1)),alphaGA,betaGA) - ...
        gamcdf(0.5*(cents2(bind-1)+cents2(bind)),alphaGA,betaGA) ;
end
gamhistGA(bins) = gamcdf(cents2(bins)+cents2(bins) - ...
                  0.5*(cents2(bins-1)+cents2(bins)),alphaGA,betaGA) - ...
                  gamcdf(0.5*(cents2(bins-1)+cents2(bins)),alphaGA,betaGA);

figure(3)
f3 = bar(cents2,freq2./obs,1,'r') ;
title('Histogram of Monthly Health Expenditures (<=$800)','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)
hold on ;
plot([0 cents2],[0 gamhistGA],'b:') ;
legend('Hist','GA') ;
hold off ;

%-------------------------------------------------------------------------%
% Use gamma (GA) estimates as initial values to estimate generalized gamma
% (GG) distribution parameters using expenditure values less than $700 and
% plot estimated distribution of expenditures histogram
%
% f(x;alp,bet,m)=(m/((bet^alp)*Gamma(alp/m)))*x^(alp-1)*exp(-(x/bet)^m)
%-------------------------------------------------------------------------%
% mGGinit    = scalar, initial guess for m
% GGinit     = 1 x 3 vector, initial guesses for alpha, beta, and m
% maxsupGG   = scalar, maximum value in expenditures support to use in
%              estimation
% minvecGG   = 1 x 3 vector of minimum values for alpha, beta, and m
%              parameters
% parGG      = 1 x 3 vector, estimated values for alpha, beta, and m from
%              generalized gamma (GG) distribution
% negLGG     = scalar, negative of the maximized log likelihood function
% maxLGG     = scalar, maximized log likelihood function value
% alphaGG    = scalar, estimated value for alpha in GG distribution
% betaGG     = scalar, estimated value for beta in GG distribution
% gamhistGG  = 1 x bins vector, histogram percentage values from estimated
%              GG distribution pdf
%-------------------------------------------------------------------------%
mGGinit = 1 ;
GGinit = [alphaGA betaGA mGGinit] ;
maxsupGG = 300000 ;
minvecGG = [10^(-10) 10^(-10) 10^(-10)] ;
[parGG,negLGG] = fmincon(@maxlnLGG,GGinit,[],[],[],[],minvecGG,[],[],...
                         optionsfmc,clms,maxsupGG) ;

maxLGG = -negLGG ;
alphaGG = parGG(1) ;
betaGG = parGG(2) ;
mGG = parGG(3) ;
display([alphaGG betaGG mGG]) ;
display(maxLGG) ;

GGfunc = (@(xx)(mGG/((betaGG^alphaGG)*gamma(alphaGG/mGG)))*...
         (xx.^(alphaGG-1)).*exp(-(xx./betaGG).^mGG)) ;

gamhistGG = zeros(1,bins) ;
cut2 = 0.5*(cents2(1)+cents2(2)) ;
gamhistGG(1) = quadgk(GGfunc,0,cut2) ;
for bind = 2:bins-1
    cut2 = 0.5*(cents2(bind)+cents2(bind+1)) ;
    cut1 = 0.5*(cents2(bind-1)+cents2(bind)) ;
    gamhistGG(bind) = quadgk(GGfunc,cut1,cut2) ;
end
cut2 = cents2(bins)+cents2(bins) - 0.5*(cents2(bins-1)+cents2(bins)) ;
cut1 = 0.5*(cents2(bins-1)+cents2(bins)) ;
gamhistGG(bins) = quadgk(GGfunc,cut1,cut2) ;
figure(4)
f4 = bar(cents2,freq2./obs,1,'r') ;
title('Histogram of Monthly Health Expenditures (<=$800)','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)
hold on ;
plot([0 cents2],[0 gamhistGG],'m--') ;
legend('Hist','GG') ;
hold off ;

%-------------------------------------------------------------------------%
% Use generalized gamma (GG) estimates as initial values to estimate
% generalized beta 2 (GB2) distribution parameters using all expenditure
% observations and plot estimated distribution of expenditures histogram
%
% f(x;a,b,p,q)=(a/((b^(a*p))*Beta(p,q)*((1+(x/b)^a)^(p+q)))*x^(a*p-1)
%-------------------------------------------------------------------------%
% qGB2init   = scalar, initial guess for q parameter of GB2 distribution
% aGB2init   = scalar, initial guess for a parameter of GB2 distribution
% bGB2init   = scalar, initial guess for b parameter of GB2 distribution
% pGB2init   = scalar, initial guess for p parameter of GB2 distribution
% GB2init    = 1 x 4 vector, initial guesses for parameter choice vector
% minvecGB2  = 1 x 4 vector, minimum values of parameter choice vector
% parGB2     = 1 x 4 vector, estimated values for a, b, p, and q from
%              generalized beta 2 (GB2) distribution
% negLGB2    = scalar, negative of the maximized log likelihood function
% maxLGB2    = scalar, maximized log likelihood function value
% aGB2       = scalar, estimated value for a in GB2 distribution
% bGB2       = scalar, estimated value for b in GB2 distribution
% pGB2       = scalar, estimated value for p in GB2 distribution
% qGB2       = scalar, estimated value for q in GB2 distribution
% gamhistGB2 = 1 x bins vector, histogram percentage values from estimated
%              generalized beta 2 (GB2) distribution pdf
% GB2func    = anonymous function, for value xx returns the value of the
%              GB2 pdf at xx: GB2(xx;aGB2,bGB2,pGB2,qGB2)
%-------------------------------------------------------------------------%
qGB2init = 10000 ;
aGB2init = mGG ;
bGB2init = (qGB2init^(1/mGG))*betaGG ;
pGB2init = alphaGG/mGG ;
GB2init = [aGB2init bGB2init pGB2init qGB2init] ;
minvecGB2 = [10^(-10) 10^(-10) 10^(-10) 10^(-10)] ;
[parGB2,negLGB2] = fmincon(@maxlnLGB2,GB2init,[],[],[],[],minvecGB2,...
                           [],[],optionsfmc,clms) ;

maxLGB2 = -negLGB2 ;
aGB2 = parGB2(1) ;
bGB2 = parGB2(2) ;
pGB2 = parGB2(3) ;
qGB2 = parGB2(4) ;

display([aGB2 bGB2 pGB2 qGB2]) ;
display(maxLGB2) ;
gamhistGB2 = zeros(1,bins) ;

GB2func = (@(xx)(aGB2./((bGB2^(aGB2*pGB2))*beta(pGB2,qGB2).*...
          (ones(size(xx))+(xx./bGB2).^aGB2).^(pGB2+qGB2))).*...
          (xx.^(aGB2*pGB2-1))) ;

cut2 = 0.5*(cents2(1)+cents2(2)) ;
gamhistGB2(1) = quadgk(GB2func,0,cut2) ;
for bind = 2:bins-1
    cut2 = 0.5*(cents2(bind)+cents2(bind+1)) ;
    cut1 = 0.5*(cents2(bind-1)+cents2(bind)) ;
    gamhistGB2(bind) = quadgk(GB2func,cut1,cut2) ;
end
cut2 = cents2(bins)+cents2(bins) - 0.5*(cents2(bins-1)+cents2(bins)) ;
cut1 = 0.5*(cents2(bins-1)+cents2(bins)) ;
gamhistGB2(bins) = quadgk(GB2func,cut1,cut2) ;
figure(5)
f5 = bar(cents2,freq2./obs,1,'r') ;
title('Histogram of Monthly Health Expenditures (<=$800)','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)
hold on ;
plot([0 cents2],[0 gamhistGB2],'k-','LineWidth',3) ;
legend('Hist','GB2') ;
hold off ;

%-------------------------------------------------------------------------%
% Plot all four histograms together: Data, GA, GG, and GB2
%-------------------------------------------------------------------------%
figure(6)
f6 = bar(cents2,freq2./obs,1,'r') ;
title('Histogram of Monthly Health Expenditures (<=$800)','FontSize',14)
xlabel('total monthly health expenditure ($s)','FontSize',12)
ylabel('Fraction of individuals in bin','FontSize',12)
hold on ;
plot([0 cents2],[0 gamhistGA],'b:') ;
hold on ;
plot([0 cents2],[0 gamhistGG],'m--') ;
hold on ;
plot([0 cents2],[0 gamhistGB2],'k-','LineWidth',3) ;
legend('Hist','GA','GG','GB2') ;
hold off ;

%-------------------------------------------------------------------------%
runtime = etime(clock,starttime) ; % end timer
save MLEprobs.mat ;
