function minfunc = maxlnLGA(parvec,clms,maxsup)
%-------------------------------------------------------------------------%
% Calculate the log likelihood function for given parameters of gamma (GA)
% distribution and minimize its negative (maximize it)
%
% f(x;alp,bet) = (1/((bet^alp)*Gamma(alp)))*(x^(alp-1))*exp(-(x/bet))
%-------------------------------------------------------------------------%
% parvec   = 1 x 2 vector, values of alpha and beta from gamma (GA) dist.
% clms     = clmsize x 1 vector, monthly expenditure values
% maxsup   = scalar, maximum value in support of clms to be used in
%            estimation
% alpha    = scalar, value of alpha in expenditure process
% beta     = scalar, value of beta in expenditure process
% clms_trc = clmsize2 x 1 vector, truncated set of lower support of
%            expenditure values
% gprobs   = epssize2 x 1 vector, gamma distributed probabilities of each
%            expenditure value
% lngprobs = epssize2 x 1 vector, log of gprobs
% lnL      = scalar, log-likelihood function value
% neglnL   = scalar, negative log-likelihood function value
% minfunc  = scalar, criterion to be minimized (max log-likelihood)
%-------------------------------------------------------------------------%
alpha = parvec(1) ;
beta = parvec(2) ;

clms_trc = clms(clms<=maxsup) ;
gprobs = gampdf(clms_trc,alpha,beta) ;
lngprobs = log(gprobs) ;
lnL = sum(lngprobs) ;
neglnL = -lnL ;

minfunc = neglnL ;