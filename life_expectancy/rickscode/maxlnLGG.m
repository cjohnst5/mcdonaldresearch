function minfunc = maxlnLGG(parvec,clms,maxsup)
%-------------------------------------------------------------------------%
% Calculate the log likelihood function for given parameters of the
% generalized gamma (GG) distribution and minimize its negative (maximize
% it)
%
% f(x;alp,bet,m)=(1/((bet^alp)*Gamma(alp/m)))*x^(alp-1)*exp(-(x/bet)^m)
%-------------------------------------------------------------------------%
% parvec     = 1 x 3 vector, values of alpha, beta, and m from generalized
%              gamma (GG) distribution
% clms       = clmsize x 1 vector, expenditure values
% maxsup     = scalar, maximum value in support of clms to be used in
%              estimation
% alpha      = scalar, value of alpha in expenditure process
% beta       = scalar, value of beta in expenditure process
% mm         = scalar, value of m in expenditure process
% clms_trc   = clmsize2 x 1 vector, truncated set of lower observations of
%              expenditure values
% gprobs     = epssize2 x 1 vector, GG distributed probabilities of
%              expenditures
% lngprobs   = epssize2 x 1 vector, log of gprobs
% lnL        = scalar, log-likelihood function value
% neglnL     = scalar, negative log-likelihood function value
% minfunc    = scalar, criterion to be minimized (max log-likelihood)
%-------------------------------------------------------------------------%
alpha = parvec(1) ;
beta = parvec(2) ;
mm = parvec(3) ;

clms_trc = clms(clms<=maxsup) ;
gprobs = (mm/((beta^alpha)*gamma(alpha/mm)))*(clms_trc.^(alpha-1)).*...
    exp(-(clms_trc./beta).^mm) ;
lngprobs = log(gprobs) ;
lnL = sum(lngprobs) ;
neglnL = -lnL ;

minfunc = neglnL ;