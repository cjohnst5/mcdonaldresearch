function [ LLV ] = loglike_br3( paravec, data )
%   Calculates the log likelihood of a bf3 random variable. Calls the 
%   loglike_gb2 function to do so. 
paravec_gb2 = [paravec, 0]; %we must feed in ln(1)=0 when q = 1
LLV = loglike_gb2(paravec_gb2, data);




end

