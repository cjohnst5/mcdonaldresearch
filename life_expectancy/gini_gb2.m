function [ g ] = gini_gb2( paravec )
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here

a = paravec(1);
p = paravec(2);
q = paravec(3);
row1 = [1, p+q, 2*p+(1/a)];
row2 = [p+1, 2*(p+q)];
row3 = [p+(1/a)+1, 2*(p+q)];
g = (1/p)*hypergeom(row1, row2, 1) - (1/(p+1/a))*hypergeom(row1,row3,1);

end

