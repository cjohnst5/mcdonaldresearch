function [ g ] = gini_br12( paravec )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

a = paravec(1);
q = paravec(2);
g = 1 - exp(gammaln(q) + gammaln(2*q -(1/a)) - gammaln(q-(1/a)) - gammaln(2*q));

end

