function [ g ] = gini_br3( paravec )
%Gini Coefficient for the Burr 3
%   Detailed explanation goes here
a = paravec(1);
p = paravec(2);
g = exp(gammaln(p) + gammaln(2*p + (1/a)) - gammaln(2*p)*gammaln(p+(1/a))) - 1;

end

