function [ LLV ] = loglike_loglog( paravec, data )
%Returns the loglikelihood value for a loglogistic random variable
%   Inputs:
%   paravec: the [log(b)], where b is the distributional parameter
%   data: the data/random variable we are calculating the log logistic
%   value of
%   ..............Returns........................
%   The loglikelihood value (float)

paravec_gb2 = [1, paravec, 0, 0];
LLV = loglike_gb2(paravec_gb2, data);


end

