function [ ev ] = exp_val_gb2( paravec )
%Calculates the expected value of a GB2 random variable
%   -----------------------Inputs------------------------------------------
% paravec: Vector of parameter values: [a, b, p, q]

% Returns the expected value
a = paravec(1);
b = paravec(2);
p = paravec(3);
q = paravec(4);

lnbeta_num = gammaln(p+1/a) + gammaln(q-1/a) - gammaln(p+q);
lnbeta_denom = gammaln(p) + gammaln(q) - gammaln(p+q);
ev = b*exp(lnbeta_num)/exp(lnbeta_denom);


end

