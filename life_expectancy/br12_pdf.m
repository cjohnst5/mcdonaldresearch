function [ pdf ] = br12_pdf( paravec, x )
%Calculates the pdf values of a br12 pdf, given parameters and random
%   -----------------Inputs---------------------------
% paravec: A vector that contains the distrubutional parameter, [a, b, q]

paravec_gb2 = [paravec(1:2), 1, paravec(3)];
pdf = gb2_pdf(paravec_gb2, x);


end

