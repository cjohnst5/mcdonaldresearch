function [ ev ] = exp_val_br3( paravec )
%Calculates the expected value of a GB2 random variable
%   -----------------------Inputs------------------------------------------
% paravec: Vector of parameter values: [a, b, p]

% Returns the expected value

paravec_gb2 = [paravec(1:3), 1]; 
ev = exp_val_gb2(paravec_gb2);
end