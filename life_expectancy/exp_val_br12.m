function [ ev ] = exp_val_br12( paravec )
%Calculates the expected value of a GB2 random variable
%   -----------------------Inputs------------------------------------------
% paravec: Vector of parameter values: [a, b, q]

% Returns the expected value

paravec_gb2 = [paravec(1:2), 1, paravec(3)]; 
ev = exp_val_gb2(paravec_gb2);
end

