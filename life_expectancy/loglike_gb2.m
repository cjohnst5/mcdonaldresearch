function[LLV] = loglike_gb2(lnparavec, data)

%Getting our variables all sorted out
% x = size(data);
numobs = sum(data(:,1));

paravec = exp(lnparavec(2:end));
a = lnparavec(1); %since a can be negative, we did not have to exp(ln) it
b = paravec(1);
p = paravec(2);
q = paravec(3);

%getting the beta term.
lnbta = gammaln(p) + gammaln(q) - gammaln(p+q);
%getting the other terms
term1 = (log(abs(a)) - a*p*log(b) - lnbta)*numobs;
term2 = (a*p-1)*sum(data(:,1).*log(data(:,2)));
term3 = (p+q)*(sum(data(:,1).*log(1 + (data(:,2)/b).^a)));

LLV = (-1)*(term1 + term2 - term3);

end