% May 29 2014
%--------------------------------------------------------------------------
% This script estimates distributional parameters, gini coefficients and 
% expected value for the loglog, br3, br12 and gb2 distributions using 
% four sets of life expectancy data:


%    utah_men_life_expectancy.xslx
%    utah_women_life_expectancy.xslx
%    texas_men_life_expectancy.xlsx
%    texas_women_life_expectancy.xlsx
%    Some other data set

% -------------------------------------------------------------------------
%% Utah Men
display('Utah Women')
data = xlsread('life_expectancy_averages.xlsx.');
numobs = sum(data(:,1));
density = data(:,1)./numobs;


% Loglog, br3, br12, gb2
guess_loglog = [log(sum(density.*data(:,2)))];
[x_loglog, fval_loglog, exitflag, output] = fminsearch(@(x)loglike_loglog(x, data), ...
    guess_loglog, optimset('MaxFunEvals',1e6,'MaxIter',1e6));

guess_br3 = [1, x_loglog, 0];
[x_br3, fval_br3, exitflag_br3, output_br3] = fminsearch(@(x)loglike_br3(x, data),...
    guess_br3, optimset('MaxFunEvals',1e6,'MaxIter',1e6));

guess_br12 = [1, x_loglog, 0];
[x_br12, fval_br12, exitflag_br12, output_br12] = fminsearch(@(x)loglike_br12(x, data), guess_br12, optimset('MaxFunEvals',1e6,'MaxIter',1e6)); 

guess_gb2 = [x_br3(1), x_br3(2), x_br3(3), x_br12(3)];
[x_gb2, fval_gb2, exitflag_gb2, output_gb2] = fminsearch(@(x)loglike_gb2(x, data), guess_gb2, optimset('MaxFunEvals',1e8,'MaxIter',1e8)); 

% Exponentiating the parameters that we logged: 
loglog_para1 = exp(x_loglog);
br3_para1 = [x_br3(1), exp(x_br3(2:end))];
br12_para1 = [x_br12(1), exp(x_br12(2:3))];
gb2_para1 = [x_gb2(1), exp(x_gb2(2:end))];

% Gini coefficients
coef_br3 = gini_br3([br3_para1(1), br3_para1(3)]); %We only need a and p, or the 
                                                 %1st and third parameter
coef_br12 = gini_br12([br12_para1(1), br12_para1(3)]);
coef_gb2 = gini_gb2([gb2_para1(1), gb2_para1(3:4)]);

% Expected Values: 
ev_br3 = exp_val_br3(br3_para1);
ev_br12 = exp_val_br3(br12_para1);
ev_gb2 = exp_val_br3(gb2_para1);

% filename = 'results';
% param_results = [1,exp(x_loglog), 1, 1; x_br3(1), exp(x_br3(2:end)), 1; x_br12, exp(x_br12(2)), 1, exp(x_br12(3))];
% xlswrite(filename, param_results)


%Plotting the results we have so far: 
x_points = linspace(50,150, 500);

loglog_points = loglog_pdf(loglog_para1, x_points);
figure(1);
plot(x_points, loglog_points);
title('Loglog- Utah men');

br3_points = br3_pdf(br3_para1, x_points);
figure(2);
plot(x_points, br3_points);
title('br3- Utah men');

br12_points = br12_pdf(br12_para1, x_points);
figure(3);
plot(x_points, br12_points);
title('Br12 - utah men')

gb2_points = gb2_pdf(gb2_para1, x_points);
figure(4);
plot(x_points, gb2_points);
title('GB2- Utah men');
display('Done with Utah men')
%% Utah Women
display('Utah women')
x_range = 'C2:D30';
data_utah_women = xlsread('utah_women_life_expectancy.xlsx.', x_range);
numobs = sum(data_utah_women(:,1));
density = data_utah_women(:,1)./numobs;

% Loglog, br3, br12, gb2
guess_loglog = [log(sum(density.*data_utah_women(:,2)))];
[x_loglog, fval_loglog, exitflag, output] = fminsearch(@(x)loglike_loglog(x, data_utah_women), ...
    guess_loglog, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Loglog is done')

guess_br3 = [1, x_loglog, 0];
[x_br3, fval_br3, exitflag_br3, output_br3] = fminsearch(@(x)loglike_br3(x, data_utah_women),...
    guess_br3, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Br3 is done')

guess_br12 = [1, x_loglog, 0];
[x_br12, fval_br12, exitflag_br12, output_br12] = fminsearch(@(x)loglike_br12(x, data_utah_women),...
    guess_br12, optimset('MaxFunEvals',1e6,'MaxIter',1e6)); 
display('Br12 is done')

guess_gb2 = [x_br3(1), x_br3(2), x_br3(3), x_br12(3)];
[x_gb2, fval_gb2, exitflag_gb2, output_gb2] = fminsearch(@(x)loglike_gb2(x, data_utah_women), ...
    guess_gb2, optimset('MaxFunEvals',1e8,'MaxIter',1e8)); 
display('Gb2 is done')

% Exponentiating the parameters that we logged: 
loglog_para4 = exp(x_loglog);
br3_para4 = [x_br3(1), exp(x_br3(2:end))];
br12_para4 = [x_br12(1), exp(x_br12(2:3))];
gb2_para4 = [x_gb2(1), exp(x_gb2(2:end))];

% Gini coefficients
coef_br3 = gini_br3([br3_para4(1), br3_para4(3)]); %We only need a and p, or the 
                                                 %1st and third parameter
coef_br12 = gini_br12([br12_para4(1), br12_para4(3)]);
coef_gb2 = gini_gb2([gb2_para4(1), gb2_para4(3:4)]);
display('Ginis are done')

% Expected Values
ev_br3 = exp_val_br3([br3_para4]);
ev_br12 = exp_val_br3([br12_para4]);
ev_gb2 = exp_val_br3([gb2_para4]);

display('Done with it all')
%% Texas Men Data
display('Texas Men')
x_range = 'C2:D255';
data_texas_men = xlsread('texas_men_life_expectancy.xlsx.', x_range);
numobs = sum(data_texas_men(:,1));
density = data_texas_men(:,1)./numobs;

% Loglog, br3, br12, gb2
guess_loglog = [log(sum(density.*data_texas_men(:,2)))];
[x_loglog, fval_loglog, exitflag, output] = fminsearch(@(x)loglike_loglog(x, data_texas_men), ...
    guess_loglog, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Done loglog')

guess_br3 = [1, x_loglog, 0];
[x_br3, fval_br3, exitflag_br3, output_br3] = fminsearch(@(x)loglike_br3(x, data_texas_men),...
    guess_br3, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Done br3')

guess_br12 = [1, x_loglog, 0];
[x_br12, fval_br12, exitflag_br12, output_br12] = fminsearch(@(x)loglike_br12(x, data_texas_men),...
    guess_br12, optimset('MaxFunEvals',1e6,'MaxIter',1e6)); 
display('Done br12')

guess_gb2 = [x_br3(1), x_br3(2), x_br3(3), x_br12(3)];
[x_gb2, fval_gb2, exitflag_gb2, output_gb2] = fminsearch(@(x)loglike_gb2(x, data_texas_men), ...
    guess_gb2, optimset('MaxFunEvals',1e8,'MaxIter',1e8)); 
display('Done gb2')

% Exponentiating the parameters that we logged: 
loglog_para2 = exp(x_loglog);
br3_para2 = [x_br3(1), exp(x_br3(2:end))];
br12_para2 = [x_br12(1), exp(x_br12(2:3))];
gb2_para2 = [x_gb2(1), exp(x_gb2(2:end))];

% Gini coefficients
coef_br3 = gini_br3([br3_para2(1), br3_para2(3)]); %We only need a and p, or the 
                                                 %1st and third parameter
coef_br12 = gini_br12([br12_para2(1), br12_para2(3)]);
% coef_gb2 = gini_gb2([gb2_para2(1), gb2_para2(3:4)]);
display('Done Gini')

% Plotting
x_points = linspace(50,150, 500);

loglog_points = loglog_pdf(loglog_para2, x_points);
figure(5);
plot(x_points, loglog_points);
title('Loglog-Texas men');

br3_points = br3_pdf(br3_para2, x_points);
figure(6);
plot(x_points, br3_points);
title('br3-Texas men');

br12_points = br12_pdf(br12_para2, x_points);
figure(7);
plot(x_points, br12_points);
title('Br12-Texas men')

gb2_points = gb2_pdf(gb2_para2, x_points);
figure(8);
plot(x_points, gb2_points);
title('GB2- Texas men');

% Expected Values
ev_br3 = exp_val_br3(br3_para2);
ev_br12 = exp_val_br3(br12_para2);
ev_gb2 = exp_val_br3(gb2_para2);
display('Done with Texas Men')
% -------------------------------------------------------------------------
%% Texas Women
x_range = 'C2:D255';
data_texas_women = xlsread('texas_women_life_expectancy.xlsx.', x_range);
numobs = sum(data_texas_women(:,1));
density = data_texas_women(:,1)./numobs;

% Loglog, br3, br12, gb2
guess_loglog = [log(sum(density.*data_texas_women(:,2)))];
[x_loglog, fval_loglog, exitflag, output] = fminsearch(@(x)loglike_loglog(x, data_texas_women), ...
    guess_loglog, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Loglog is done')

guess_br3 = [1, x_loglog, 0];
[x_br3, fval_br3, exitflag_br3, output_br3] = fminsearch(@(x)loglike_br3(x, data_texas_women),...
    guess_br3, optimset('MaxFunEvals',1e6,'MaxIter',1e6));
display('Br3 is done')

guess_br12 = [1, x_loglog, 0];
[x_br12, fval_br12, exitflag_br12, output_br12] = fminsearch(@(x)loglike_br12(x, data_texas_women),...
    guess_br12, optimset('MaxFunEvals',1e6,'MaxIter',1e6)); 
display('Br12 is done')

guess_gb2 = [x_br3(1), x_br3(2), x_br3(3), x_br12(3)];
[x_gb2, fval_gb2, exitflag_gb2, output_gb2] = fminsearch(@(x)loglike_gb2(x, data_texas_women), ...
    guess_gb2, optimset('MaxFunEvals',1e8,'MaxIter',1e8)); 
display('Gb2 is done')

% Exponentiating the parameters that we logged: 
loglog_para3 = exp(x_loglog);
br3_para3 = [x_br3(1), exp(x_br3(2:end))];
br12_para3 = [x_br12(1), exp(x_br12(2:3))];
gb2_para3 = [x_gb2(1), exp(x_gb2(2:end))];

% Gini coefficients
coef_br3 = gini_br3([br3_para3(1), br3_para3(3)]); %We only need a and p, or the 
                                                 %1st and third parameter
coef_br12 = gini_br12([br12_para3(1), br12_para3(3)]);
% coef_gb2 = gini_gb2([gb2_para3(1), gb2_para3(3:4)]);
display('Ginis are done')

% Expected Values
ev_br3 = exp_val_br3([br3_para3]);
ev_br12 = exp_val_br3([br12_para3]);
ev_gb2 = exp_val_br3([gb2_para3]);

display('Done with it all')
