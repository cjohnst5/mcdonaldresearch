Last updated July 15, 2013

This folder, sgttree, contains folders and code that we can use to estimate distributional parameters and plot cdfs and pdfs. The folders are the following:

cdfs_pfs: Cdfs and pdfs for the SGT, SGED, GED, GT, ST, T, Slaplace, Snormal distributions. I didn't code the normal or laplace because python already has those coded. 

graph_func: function to plot the pdf of a distribution, given parameters

mle: contains log-likelihood functions and functions that perform MLE in order to estimate distributional parameters of the SGT, SGED, GT, ST, GED, Slaplace, Snormal, normal, laplace and t distributions. 