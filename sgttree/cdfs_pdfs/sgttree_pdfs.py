#sgttree_pdfs
#Last modified July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 

def sgt_pdf(paravec, y):
    m, lamb, s, p, q = paravec
    pdf = p / (2 * s * q ** (1/p) * beta(1/p, q) * \
        ((1 + abs(y-m) ** p  / ((1 + lamb * np.sign(y-m)) ** p * q * s ** p))) ** (q + 1/p))
    return pdf

def sged_pdf(paravec, y):
    m, lamb, s, p = paravec
    pdf = p * np.exp(- abs(y-m) ** p / (( 1 + lamb * np.sign(y-m)) * s) ** p) \
          / (2 * s * gamma(1/p))
    return pdf 

#special cases of the SGT.......................................................
def gt_pdf(paravec, y):
    m, s, p, q = paravec
    pdf = p / (2 * s * q ** (1/p) * beta(1/p, q) * \
        ((1 + abs(y-m) ** p  / (q * s ** p))) ** (q + 1/p))
    return pdf

def st_pdf(paravec, y):
    m, lamb, s, q = paravec
    pdf = 2 / (2 * s * q ** (1/2) * beta(1/2, q) * \
        ((1 + abs(y-m) ** 2  / ((1 + lamb * np.sign(y-m)) ** 2 * q * s ** 2))) ** (q + 1/2))
    return pdf

def t_pdf(paravec, y):
    m, s, q = paravec
    pdf = 1 / (s * q ** (1/2) * beta(1/2, q) * \
        ((1 + abs(y-m) ** 2  / (q * s ** 2))) ** (q + 1/2))
    return pdf

#special cases of the SGED.................................
def slaplace_pdf(paravec, y):    
    m, lamb, s = paravec
    pdf = np.exp(- abs(y-m) / (( 1 + lamb * np.sign(y-m)) * s)) \
          / (2 * s * gamma(1))
    return pdf

def ged_pdf(paravec, y):
    m, s, p = paravec
    pdf = p * np.exp(-abs(y-m) ** p / s ** p ) \
          / (2 * s * gamma(1/p))
    return pdf

def snormal_pdf(paravec, y):
    m, lamb, s = paravec
    pdf = np.exp(- abs(y-m) ** 2 / (( 1 + lamb * np.sign(y-m)) * s) ** 2) \
          / (s * gamma(1/2))
    return pdf

