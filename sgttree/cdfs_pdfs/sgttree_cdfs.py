#sgttree_cdfs.py
#instead of putting each cdf in its own script, I'm going to compile them here
from __future__ import division
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 




#Main two cdfs: ...................................................
def sgt_cdf(paravec, y):
    m, lamb, s, p, q = paravec
    z = abs(y-m) ** p / (abs(y-m) ** p + q * s ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1-lamb)/2 + ((1 + lamb * np.sign(y - m))/2) * np.sign(y-m) * betainc(1/p, q, z)
    return cdf

def sged_cdf(paravec, y):
    m, lamb, s, p = paravec
    z = abs(y-m) ** p / (s ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z)
    return cdf



#Special cases of the sgt_cdf ......................................
def gt_cdf(paravec, y):
    m, s, p, q = paravec
    z = abs(y-m) ** p / (abs(y-m) ** p + q * s ** p) 
    cdf = 1/2 + (1/2) * np.sign(y-m) * betainc(1/p, q, z)
    return cdf

def st_cdf(paravec, y):    
    m, lamb, s, q = paravec
    z = abs(y-m) ** 2 / (abs(y-m) ** 2 + q * s ** 2 * (1 + lamb * np.sign(y-m)) ** 2)
    cdf = (1-lamb)/2 + ((1 + lamb * np.sign(y - m))/2) * np.sign(y-m) * betainc(1/2, q, z)
    return cdf

def t_cdf(paravec, y):
    m, s, q = paravec
    z = abs(y-m) ** 2 / (abs(y-m) ** 2 + q * s ** 2)
    cdf = 1/2 + (1/2) * np.sign(y-m) * betainc(1/2, q, z)
    return cdf
#I'm ignoring the cauchy stuff.



#Special cases of the sged_cdf
def slaplace_cdf(paravec, y):
    m, lamb, s = paravec
    z = abs(y-m) / (s * (1 + lamb * np.sign(y-m)))
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1, z)
    return cdf

def ged_cdf(paravec, y):
    m, s, p = paravec
    # z = abs(y-m) ** p / s ** p 
    # cdf = 1/2 + (1/2) * np.sign(y-m) * gammainc(1/p, z)
    lamb = 0
    z = abs(y-m) ** p / (s ** p * (1 + lamb * np.sign(y-m)) ** p)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/p, z) 
    return cdf

def snormal_cdf(paravec, y):
    m, lamb, s = paravec
    z = abs(y-m) ** 2 / (s ** 2 * (1 + lamb * np.sign(y-m)) ** 2)
    cdf = (1 - lamb) / 2 + ((1 + lamb * np.sign(y-m)) /2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf

def normal_cdf(paravec, y):
    m, s = paravec
    z = abs(y-m) ** 2 / (s ** 2)
    cdf = 1/2 + (1/2) * np.sign(y-m) * gammainc(1/2, z)
    return cdf

#laplace and normal cdfs are already part of python's packages. 





