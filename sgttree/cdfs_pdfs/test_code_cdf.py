
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import betaln, gammaln, beta, gamma, betainc, gammainc 
from scipy import optimize as opt 
import matplotlib.pyplot as plt
from scipy.stats import norm, laplace

import sys
sys.path.insert(0,'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_cdfs import *

from scipy.stats import skew, kurtosis, norm, laplace

sys.path.insert(0,'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\mle')
import test_code_MLE as tc 
from sgt_MLE import sgt_MLE


#seeing if my cdf's look ok. 
data = sp.loadtxt('C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\\bwght.csv', delimiter = ",")
def plot_cdf(data, cdf, paravec, name, lab = None):
    x_range = np.linspace(data.min(), data.max(), 500)
    cdf_points = cdf(paravec, x_range)
    plt.plot(x_range, cdf_points, label= lab)
    plt.title(name)


# sgt_guess = [data.mean(), skew(data), np.std(data), 5, 100]
# sgt, sgt_trans = sgt_MLE(data, sgt_guess)
# plot_cdf(data, sgt_cdf, sgt, 'SGT CDF')

# plot_cdf(data, gt_cdf, tc.gt, 'GT CDF')

# plot_cdf(data, st_cdf, tc.st, 'ST CDF')
# plot_cdf(data, t_cdf, tc.t, 'T CDF')
# plot_cdf(data, sged_cdf, tc.sged, 'SGED cdf')

# plot_cdf(data, slaplace_cdf, tc.slap, 'Slaplace cdf')
# plot_cdf(data, ged_cdf, tc.ged, 'GED cdf')
# plot_cdf(data, snormal_cdf, tc.snormal, 'Snormal cdf')
plot_cdf(np.array([-30,30]), normal_cdf, [0,1], 'Normal cdf', lab ='mycdf')

x_range = np.linspace(-30, 30, 500)
norm_points = norm.cdf(x_range)
lap_points = laplace.cdf(x_range, *tc.lap)
plt.plot(x_range, norm_points, label ='Python normal cdf')
plt.legend()
plt.show()

#looks like all the cdfs are in order 