#laplace_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 

def laplace_MLE(data, method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 1000, 'maxfev' : 1000})):
    
    """
    Uses MLE to find m (mean) and s (sigma) for the laplace distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a laplace distribution    
        
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    laplace:
        Estimated parameters for the laplace distribution
    laplace_trans:
        output from the minimization procedure, including the ln(parameters)
    """
    m = data.mean()
    ls = np.log(np.std(data))

    cri_laplace = lambda x: ll.laplace_loglike_crit(x, data)

    laplace_trans = opt.minimize(cri_laplace, [m, ls], method = method, tol = tolerance, options = options)
    laplace = [laplace_trans.x[0], np.exp(laplace_trans.x[1])]

    return laplace, laplace_trans