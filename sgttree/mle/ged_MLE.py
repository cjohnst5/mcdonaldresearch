#ged_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from scipy.stats import skew, kurtosis


def ged_MLE(data, guess, method = 'Powell', tolerance = 1e-5, options = ({'maxiter': 15000, 'maxfev' : 15000})):
    
    """
    Uses MLE to find m (mean) and s (sigma) for the ged distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a ged distribution    
        
    guess:
        Initial guess for the original parameters. Transformations will occur 
        within this function
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    ged:
        Estimated parameters for the ged distribution
    ged_trans:
        output from the minimization procedure, including the transformed parameters
    """
    m, s, p = guess
    guess_trans = [m, np.log(s), np.log(p)]
    
    cri_ged = lambda x: ll.ged_loglike_crit(x, data)

    ged_trans = opt.minimize(cri_ged, guess_trans, method = method, tol = tolerance, options = options)
    
    #getting our original lambda back
    ged = [ged_trans.x[0], np.exp(ged_trans.x[1]), np.exp(ged_trans.x[2])]

    return ged, ged_trans