#gt_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from scipy.stats import skew, kurtosis


def gt_MLE(data, guess, method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 15000, 'maxfev' : 15000})):
    
    """
    Uses MLE to find m (mean), s (sigma), p, and q for the gt distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a gt distribution    
    guess:
        Initial guess for the original parameters. The proper transformations to 
        the parameters in order to bound them will occur within the t_MLE function
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    gt:
        Estimated parameters for the gt distribution
    gt_trans:
        output from the minimization procedure, including the transformed parameters
    """
    
    m, s, p, q = guess
    guess_trans = [m, np.log(s), np.log(p), np.log(q)]

    cri_gt = lambda x: ll.gt_loglike_crit(x, data)

    gt_trans = opt.minimize(cri_gt, guess_trans, method = method, tol = tolerance, options = options)
    
    #getting our original lambda back
    gt = [gt_trans.x[0], np.exp(gt_trans.x[1]), np.exp(gt_trans.x[2]), np.exp(gt_trans.x[3])]

    return gt, gt_trans