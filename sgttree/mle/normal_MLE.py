#normal_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 

def normal_MLE(data, method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 1000, 'maxfev' : 1000})):
    
    """
    Uses MLE to find m (mean) and s (sigma) for the normal distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a normal distribution    
        
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    norm:
        Estimated parameters for the normal distribution
    norm_log:
        output from the minimization procedure, including the transformed
    """
    m = data.mean()
    ls = np.log(np.std(data))

    cri_normal = lambda x: ll.normal_loglike_crit(x, data)

    norm_log = opt.minimize(cri_normal, [m, ls], method = method, tol = tolerance, options = options)
    norm = [norm_log.x[0], np.exp(norm_log.x[1])]

    return norm, norm_log