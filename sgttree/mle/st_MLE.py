#st_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from scipy.stats import skew, kurtosis


def st_MLE(data, guess, method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 15000, 'maxfev' : 15000})):
    
    """
    Uses MLE to find m (mean), lambda, and s (sigma) and q for the st distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a st distribution    
    guess:
        Initial guess for the original parameters. The proper transformations to 
        the parameters in order to bound them will occur within the t_MLE function
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    st:
        Estimated parameters for the st distribution
    st_trans:
        output from the minimization procedure, including the transformed parameters
    """
    
    m, lamb, s, q = guess
    lamb_trans = np.log((1 + lamb)/ (1 - lamb))
    guess_trans = [m, lamb_trans, np.log(s), np.log(q)]

    cri_st = lambda x: ll.st_loglike_crit(x, data)

    st_trans = opt.minimize(cri_st, guess_trans, method = method, tol = tolerance, options = options)
    
    #getting our original lambda back
    lamb_end = (np.exp(st_trans.x[1])-1) / (np.exp(st_trans.x[1]) + 1)
    st = [st_trans.x[0], lamb, np.exp(st_trans.x[2]), np.exp(st_trans.x[3])]

    return st, st_trans