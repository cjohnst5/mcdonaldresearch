#snormal_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from scipy.stats import skew, kurtosis


def snormal_MLE(data, guess, method = 'Powell', tolerance = 1e-5, options = ({'maxiter': 15000, 'maxfev' : 15000})):
    
    """
    Uses MLE to find m (mean) and s (sigma) for the snormal distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a snormal distribution    
        
    guess:
        Initial guess for the original parameters. Transformations will occur 
        within this function
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    snormal:
        Estimated parameters for the snormal distribution
    snormal_trans:
        output from the minimization procedure, including the transformed parameters
    """
    m, lamb, s = guess    
    
    lamb_trans = np.log((1 + lamb)/ (1 - lamb))
    guess_trans = [m, lamb_trans, np.log(s)]
    
    cri_snormal = lambda x: ll.snormal_loglike_crit(x, data)

    snormal_trans = opt.minimize(cri_snormal, guess_trans, method = method, tol = tolerance, options = options)
    
    #getting our original lambda back
    lamb_end = (np.exp(snormal_trans.x[1])-1) / (np.exp(snormal_trans.x[1]) + 1)

    snormal = [snormal_trans.x[0], lamb_end, np.exp(snormal_trans.x[2])]

    return snormal, snormal_trans