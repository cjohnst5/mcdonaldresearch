#loglikelihoo_funcs
#I'm using the ln, exp trick on these functions to bound my parameters to values >0. 
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 

#our two main log-likelihood functions:
def sgt_loglike_main(m, lamb_trans, ls, lp, lq, y):

    """
    Log-likelihood function of the SGT. 

    Parameters
    ---------------------------------
    m:
        mode (location parameter)
    lamb_trans:
        log((1+lamb)/(1-lamb)). When written this way, -1<lamb<1
    ls, lp, lq:
        The natural logs of the parameters s, p, and q. When
        written this way, 0<= s, p, q.
    y:
        Random variable/dataset we would like fit an SGT distribution to.

    """
    s = np.exp(ls)
    p = np.exp(lp)
    q = np.exp(lq)
    lamb = (np.exp(lamb_trans) - 1)/ (np.exp(lamb_trans) + 1)    
    n = len(y)

    loglike = n*np.log(p) - n*np.log(2) - n*np.log(s) - (n/p)*np.log(q) - n*betaln((1/p), q) - \
    (q+ (1/p))*sum(np.log(1 + abs(y-m) ** p / ((1 + lamb * np.sign (y-m)) ** p * q * s **p)))

    return loglike

def sged_loglike_main(m, lamb_trans, ls, lp, y):
    """
    Log-likelihood function of the SGED. 

    Parameters
    ---------------------------------
    m:
        mode (location parameter)
    lamb_trans:
        log((1+lamb)/(1-lamb)). When written this way, -1<lamb<1
    ls, lp:
        The natural logs of the parameters s and p. When
        written this way, 0<= s, p, q.
    y:
        Random variable/dataset we would like fit an SGT distribution to.

    """
    s = np.exp(ls)
    p = np.exp(lp)    
    lamb = (np.exp(lamb_trans) - 1)/ (np.exp(lamb_trans) + 1)    
    n = len(y)

    loglike = n * np.log(p) - sum(abs(y-m)**p/(((1 + lamb * np.sign(y - m)) * s ) ** p)) \
            - n * np.log(2) - n * np.log(s) - n * gammaln(1/p)
    return loglike

#.................Distributions that are special cases of the SGT.............

def sgt_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as an sgt distribution
    m, lamb_trans, ls, lp, lq = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)

def gt_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as a gt distribution
    lamb_trans = 0
    m, ls, lp, lq = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)

def st_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as an st distribution
    lp = np.log(2)
    m, lamb_trans, ls, lq = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)

def scauchy_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as an scauchy distribution
    lp = np.log(2)
    lq = np.log(.5)
    m, lamb_trans, ls = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)

def cauchy_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as a cauchy distribution
    lp = np.log(2)
    lq = np.log(.5)
    lamb_trans = 0
    m, ls = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)

def t_loglike_crit(paravec, y):
    #returns the loglikehood value of having data fit as a t distribution
    lamb_trans = 0
    lp=np.log(2)
    m, ls, lq = paravec
    return -sgt_loglike_main(m, lamb_trans, ls, lp, lq, y)


#.............Distributions that are special cases of the SGED.............

def sged_loglike_crit(paravec, y):
    m, lamb_trans, ls, lp = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)

def slaplace_loglike_crit(paravec, y):
    lp = np.log(1)
    m, lamb_trans, ls = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)

def ged_loglike_crit(paravec, y):
    lamb_trans = 0
    m, ls, lp = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)

def snormal_loglike_crit(paravec, y):
    lp = np.log(2)
    m, lamb_trans, ls = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)

def laplace_loglike_crit(paravec, y):
    lp = np.log(1)
    lamb_trans = 0
    m, ls = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)

def normal_loglike_crit(paravec, y):
    lamb_trans = 0
    lp = np.log(2)
    m, ls = paravec
    return -sged_loglike_main(m, lamb_trans, ls, lp, y)








