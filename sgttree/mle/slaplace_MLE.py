#slaplace_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from scipy.stats import skew, kurtosis


def slaplace_MLE(data, method = 'Powell', tolerance = 1e-5,  options = ({'maxiter': 15000, 'maxfev' : 15000})):
    
    """
    Uses MLE to find m (mean), lambda, and s (sigma) for the slaplace distribution

    Parameters
    --------------------------
    data:
        numpy vector containing the data we would like to fit with a slaplace distribution    
        
    method:
        Minimization method to use with the opt.minimize command
    Tolerance:
    Options:
        Dictionary that allows one to specify higher iterations and function evaluations
        if the minimization procedure doesn't converge in the default no. of iterations and evaluations.

    Returns
    --------------------------
    slaplace:
        Estimated parameters for the slaplace distribution
    slaplace_trans:
        output from the minimization procedure, including the transformed parameters
    """
    m = data.mean()
    ls = np.log(np.std(data))
    lamb_guess = skew(data)
    lamb_trans = np.log((1 + lamb_guess)/ (1 - lamb_guess))

    cri_slaplace = lambda x: ll.slaplace_loglike_crit(x, data)

    slaplace_trans = opt.minimize(cri_slaplace, [m, lamb_trans, ls], method = method, tol = tolerance, options = options)
    
    #getting our original lambda back
    lamb = (np.exp(slaplace_trans.x[1])-1) / (np.exp(slaplace_trans.x[1]) + 1)
    slaplace = [slaplace_trans.x[0], lamb, np.exp(slaplace_trans.x[2])]

    return slaplace, slaplace_trans