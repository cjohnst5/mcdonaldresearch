#normal_MLE.py
#Last updated July 12, 2013
from __future__ import division
import scipy as sp
import numpy as np
from scipy.special import betaln, gammaln, beta, gamma 
from scipy import optimize as opt 
import loglike_funcs as ll 
from normal_MLE import normal_MLE
from laplace_MLE import laplace_MLE
from slaplace_MLE import slaplace_MLE
from ged_MLE import ged_MLE
from snormal_MLE import snormal_MLE
from t_MLE import t_MLE
from sged_MLE import sged_MLE
from gt_MLE import gt_MLE
from st_MLE import st_MLE
from sgt_MLE import sgt_MLE
from scipy.stats import skew, kurtosis, norm, laplace
import matplotlib.pyplot as plt

import sys
sys.path.insert(0,'C:\Users\cjohnst5\Documents\orderedprobit_repo\mcdonaldresearch\sgttree\cdfs_pdfs')
from sgttree_pdfs import *
sys.path.insert(0,'C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\sgttree\\graph_func')
from graph_func import single_graph



data = sp.loadtxt("C:\\Users\\cjohnst5\\Documents\\orderedprobit_repo\\mcdonaldresearch\\sgttree\\bwght.csv", delimiter = ",")
data = data[data<250]
normal, normal_trans = normal_MLE(data)

lap, lap_trans = laplace_MLE(data)

slap, slap_trans = slaplace_MLE(data, method = 'Nelder-Mead')

ged_guess = [data.mean(), np.std(data), 5]
ged, ged_trans = ged_MLE(data, ged_guess)

snormal_guess = [data.mean(), skew(data), np.std(data)]
snormal, snormal_trans = snormal_MLE(data, snormal_guess)

t_guess = [data.mean(), np.std(data), 100]
t, t_trans = t_MLE(data, t_guess)

sged_guess = [data.mean(), skew(data), np.std(data), 5]
sged_opts = ({'maxiter': 20000, 'maxfev' : 20000})
sged, sged_trans = sged_MLE(data, sged_guess, method = 'Nelder-Mead', options = sged_opts)

gt_guess = [data.mean(), np.std(data), 5, 100]
gt, gt_trans = gt_MLE(data, gt_guess, method = 'Nelder-Mead')

st_guess = [data.mean(), skew(data), np.std(data), 100]
st, st_trans = st_MLE(data, st_guess)

sgt_guess = [data.mean(), skew(data), np.std(data), 5, 100]
sgt, sgt_trans = sgt_MLE(data, sgt_guess)


#plotting the figure
# x_range = sp.linspace(data.min(), data.max(), 500)
# points_norm = norm.pdf(x_range, normal[0], normal[1])
# points_lap = laplace.pdf(x_range, lap[0], lap[1])
# plt.hist(data, bins = 100, normed = True)
# plt.plot(x_range, points_norm, linewidth = 2, label ='normal')
# plt.plot(x_range, points_lap, linewidth = 2, label = 'normal')

# fig = plt.figure()
# single_graph(data, slaplace_pdf, slap, name = 'Skewed Laplace')
# fig = plt.figure()
# single_graph(data, snormal_pdf, snormal, name = 'Skewed Normal')
# fig = plt.figure()
single_graph(data, ged_pdf, ged, name = 'GED')
# fig = plt.figure()
# single_graph(data, t_pdf, t, name = "T-distribution")
# fig = plt.figure()
# single_graph(data, sged_pdf, sged, name = 'SGED')
# fig = plt.figure()
# single_graph(data, gt_pdf, gt, name = 'GT1')
# fig = plt.figure()
# single_graph(data, st_pdf, st, name = 'ST')
# fig = plt.figure()
# single_graph(data, sgt_pdf, sgt, name = 'SGT')


# plt.show()

#yay. works for the normal and the laplace. Let's see what else we can do.
#skewed laplace converges and looks good.  
#looks pretty good for the GED.  
#looks good for the t. 
#SGED converged too! Ya. :)
#GT converges. 
#wow, they all converge. That was pretty easy. 
